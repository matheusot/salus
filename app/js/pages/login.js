function formLoginValidate() {
    var user = $('#inputUsuario').val();
    var senha = $('#inputSenha').val();
    var erro = [];
    erro = validaCampo(erro, user, 'Email', 4, 50, 'email');
    erro = validaCampo(erro, senha, 'Senha', 6, 24, 'texto');
    if (jQuery.isEmptyObject(erro)) {
        return true;
    } else {
        $('#modalErrosContent').html(erro);
        $('#modalErros').modal('show');
        return false;
    }
}

$(window).on('load', function () {
    console.log("Arquivo login.js carregado!");
    $(document).keypress(function(e) {
        if(e.which == 13) {
            $('#loginBtnSubmit').click();
        }
    });
    $('#loginBtnSubmit').on('click', function () {  
        if (formLoginValidate()) {
            var salt = "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj";
            $.ajax({
                type: "POST",
                url: "http://app.salus.ml/scripts/login.php",
                crossDomain: true,
                data: $("#formLogin").serialize() + '&hash=' + salt,
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.status == "success") {
                        if (data.tipo == 1) {
                            var array = {
                                'cod' : data.cod,
                                'cpf' : data.cpf,
                                'rg' : data.rg,
                                'nasc' : data.nasc,
                                'sexo' : data.sexo,
                                'tel' : data.tel,
                                'nome' : data.nome,
                                'email' : data.email,
                                'tipo' : data.tipo,
                                'status' : data.stats,
                                'uuid' : data.uuid,
                                'planoSaude' : data.planoSaude,
                                'numPlanoSaude' : data.numPlanoSaude,
                                'numSus' : data.numSus,
                                'tipoSanguineo' : data.tipoSanguineo,
                                'enfermidades' : data.enfermidades,
                                'medicacoes' : data.medicacoes,
                                'resp' : data.resp
                            }
                        } else if (data.tipo == 2) {
                            var array = {
                                'cod' : data.cod,
                                'cpf' : data.cpf,
                                'rg' : data.rg,
                                'nasc' : data.nasc,
                                'sexo' : data.sexo,
                                'tel' : data.tel,
                                'nome' : data.nome,
                                'email' : data.email,
                                'tipo' : data.tipo,
                                'status' : data.stats,
                                'uuid' : data.uuid,
                                'rua' : data.rua,
                                'numero' : data.numero,
                                'bairro' : data.bairro,
                                'cidade' : data.cidade,
                                'estado' : data.estado,
                                'cep' : data.cep,
                                'dep' : data.dep
                            }
                        }
                        localStorage.setItem('salus_user_info', JSON.stringify(array));
                        localStorage.getItem('salus_primeiro_login') ? localStorage.setItem('salus_primeiro_login', false) : localStorage.setItem('salus_primeiro_login', true);
                        document.location.href = 'inicial.html';
                    } else {
                        $('#modalErrosContent').html(data.msg);
                        $('#modalErros').modal('show');
                    }
                }
            });
        }
    });
});