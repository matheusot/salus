var graficos = ['batimentos', 'oxigenacao', 'temperatura'];
var count = [0, 0, 0];
var mediaInfo = [0, 0, 0];

function addGrafico(id, labels, nome, dados, bg, border) {
    var ctx = document.getElementById(id);
    if (id == 'batimentos') {
        var i = 0;
    } else if (id == 'oxigenacao') {
        var i = 1;
    } else if (id == 'temperatura') {
        var i = 2;
    }
    graficos[i] = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: nome,
                data: dados,
                backgroundColor: [
                    bg
                ],
                borderColor: [
                    border
                ],
                borderWidth: 1

            }]
        },
        options: {
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: false,
                    },
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            animation: {
                duration: 1000
            }
        }
    });
}

function addInformacao(id, info) {
    var d = new Date();
    if (d.getHours() < 10) {
        var h = '0' + d.getHours();
    } else {
        var h = d.getHours();
    }
    if (d.getMinutes() < 10) {
        var m = '0' + d.getMinutes();
    } else {
        var m = d.getMinutes();
    }
    var hora = h + ':' + m;
    graficos[id].data.labels.push(hora);
    // mostra apenas os 10 últimos registros
    // if (count[id] > 10) {
    //     graficos[id].data.labels.shift();
    // }
    graficos[id].data.datasets.forEach((dataset) => {
        dataset.data.push(info);
        // mostra apenas os 10 últimos registros
        // if (count[id] > 10) {
        //     dataset.data.shift();
        // }
        // count[id]++;
    });
    graficos[id].update(2000);
    if (id == 0) {
        var nome = 'batimentos';
    } else if (id == 1) {
        var nome = 'oxigenacao';
    } else if (id == 2) {
        var nome = 'temperatura';
    }
    $('#tabela-' + nome + ' tr:first').after('<tr><td class="table-itens-bpm">' + hora + '</td><td class="table-itens-bpm">' + info + '</td></tr>');
}

function addInformacoes() {
    addInformacao(0, Math.floor(Math.random()*100) + 30);
    addInformacao(1, Math.floor(Math.random()*100) + 30);
    addInformacao(2, Math.floor(Math.random()*100) + 30);
}

function addGraficos() {
    addGrafico("batimentos", ["00:00"], "Batimentos por minuto", [120], "rgba(0,99,97,0.2)", "rgba(65,165,165,1)");
    addGrafico("oxigenacao", ["00:00"], "Oxigenação no Sangue", [120], "rgba(0,99,97,0.2)", "rgba(65,165,165,1)");
    addGrafico("temperatura", ["00:00"], "Temperatura Corporal", [37], "rgba(0,99,97,0.2)", "rgba(65,165,165,1)");
}

function attEnfermidades() {
    var user_data = JSON.parse(localStorage.getItem('salus_user_info'));
    var html = '';
    if (user_data.tipo == 1) {
        if (user_data.enfermidades.length > 0) {
            for (var i = 0; i < user_data.enfermidades.length; i++) {
                html += '<tr><td class="table-itens">' + user_data.enfermidades[i] + '</td></tr>';
            }
        } else {
            html += '<tr><td class="table-itens">Você não possui enfermidades cadastradas!</td></tr>';
        }
    } else if (user_data.tipo == 2) {
        if (user_data.dep.enfermidadesDep.length > 0) {
            for (var i = 0; i < user_data.dep.enfermidadesDep.length; i++) {
                html += '<tr><td class="table-itens">' + user_data.dep.enfermidadesDep[i] + '</td></tr>';
            }
        } else {
            html += '<tr><td class="table-itens">O seu dependente não possui enfermidades cadastradas!</td></tr>';
        }
    }
    
    $('#tabelaEnfermidades').html(html);
}

function attMedicacoes() {
    var user_data = JSON.parse(localStorage.getItem('salus_user_info'));
    var html = '';
    if (user_data.tipo == 1) {
        if (user_data.medicacoes.length > 0) {
            for (var i = 0; i < user_data.medicacoes.length; i++) {
                html += '<tr><td class="table-itens">' + user_data.medicacoes[i] + '</td></tr>';
            }
        } else {
            html += '<tr><td class="table-itens">Você não possui medicações cadastradas!</td></tr>';
        }
    } else if (user_data.tipo == 2) {
        if (user_data.dep.medicacoesDep.length > 0) {
            for (var i = 0; i < user_data.dep.medicacoesDep.length; i++) {
                html += '<tr><td class="table-itens">' + user_data.dep.medicacoesDep[i] + '</td></tr>';
            }
        } else {
            html += '<tr><td class="table-itens">O seu dependente não possui medicações cadastradas!</td></tr>';
        }
    }
    $('#tabelaMedicacoes').html(html);
}

function attTipoSanguineo() {
    var user_data = JSON.parse(localStorage.getItem('salus_user_info'));
    if (user_data.tipo == 1) {
        if (user_data.tipoSanguineo.length > 2 || $(document).width() < 325) {
            $('.btn-blood').css('font-size', '2.7em').css('padding', '18px 0px');
        }
        $('#tipoSanguineo').html(user_data.tipoSanguineo.toUpperCase());
    } else if (user_data.tipo == 2) {
        if (user_data.dep.tipoSanguineoDep.length > 2 || $(document).width() < 325) {
            $('.btn-blood').css('font-size', '2.7em').css('padding', '18px 0px');
        }
        $('#tipoSanguineo').html(user_data.dep.tipoSanguineoDep.toUpperCase());
    }
    
    
}

$(window).on('load', function () {
    console.log("Arquivo inicial.js carregado!");
    var user_data = JSON.parse(localStorage.getItem('salus_user_info'));
    if (user_data.tipo == 2) {
        $('.btn-help').hide();
    }
    setTimeout( function () {
        $('#menu-oxigenacao').click();
        setTimeout( function () {
            $('#menu-temperatura').click();
            setTimeout( function () {
                $('#menu-batimentos').click();
            }, 50);
        }, 50);
    }, 50);
    checkFirstLogin();
    addGraficos();
    setInterval( function() {
        addInformacoes();
    }, 1000);
    attMedicacoes();
    attEnfermidades();
    attTipoSanguineo()
});