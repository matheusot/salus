let countMedicamento = 1;
let countEnfermidade = 1;
let user_data = JSON.parse(localStorage.getItem('salus_user_info'));

function formPerfilValidate() {
  var user = $('#inputUsuario').val();
  var senha = $('#inputSenha').val();
  var erro = [];
  erro = validaCampo(erro, user, 'Email', 4, 50, 'email');
  erro = validaCampo(erro, senha, 'Senha', 6, 24, 'texto');
  if (jQuery.isEmptyObject(erro)) {
      return true;
  } else {
      $('#modalErrosContent').html(erro);
      $('#modalErros').modal('show');
      return false;
  }
}

function preencheGeral() {
  $('#inputNome').val(user_data['nome']);
  $('#inputEmail').val(user_data['email']);
  $('#inputSexo').val(user_data['sexo']);
  user_data['rg'] != '' ? $('#inputRG').val(user_data['rg']) : 0;
  user_data['cpf'] != '' ? $('#inputCPF').val(user_data['cpf']) : 0;
  user_data['tel'] != '0' ? $('#inputTel').val(user_data['tel'].substr(3, 20)) : 0;
  user_data['nasc'] != '0000-00-00' ? $('#inputNasc').val(user_data['nasc'].substr(8, 2) + user_data['nasc'].substr(5, 2) + user_data['nasc'].substr(0, 4)) : 0;
}

function preencheDependente() {
  user_data['planoSaude'] != null && user_data['planoSaude'] != '' ? $('#inputPlanoSaude').val(user_data['planoSaude']) : 0;
  user_data['numPlanoSaude'] != null && user_data['numPlanoSaude'] != '' ? $('#inputNumeroPlano').val(user_data['numPlanoSaude']) : 0;
  user_data['numSus'] != null && user_data['numSus'] != '' ? $('#inputNumeroSUS').val(user_data['numSus']) : 0;
  user_data['tipoSanguineo'] != null && user_data['tipoSanguineo'] != '' ? $('#inputBlood').val(user_data['tipoSanguineo']) : 0;
  setTimeout(function() {
    var arrEnfermidades = user_data['enfermidades'];
    var arrMedicamentos = user_data['medicacoes'];
    for (var i = 1; i < arrEnfermidades.length; i++) {
      $("#addEnfermidade").click();
    }
    for (var i = 0; i < arrEnfermidades.length; i++) {
      $("#enfermidade-" + (i+1)).val(arrEnfermidades[i]);
    }
    for (var i = 1; i < arrMedicamentos.length; i++) {
      $("#addMedicamento").click();
    }
    for (var i = 0; i < arrMedicamentos.length; i++) {
      $("#medicamento-" + (i+1)).val(arrMedicamentos[i]);
    }
  }, 100);

  if (user_data.resp.length != 0) {
    var inforesp = '';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user-o fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputNomeResponsavel" value="" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputEmailResponsavel" value="" disabled></div></div>';
    inforesp += '<div class="form-group" id="formGroupSexo"><div class="input-group"><span class="input-group-addon"><i class="fa fa-neuter fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputSexoResponsavel" value="" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-id-card-o fa-fw" aria-hidden="true"></i></span>    <input type="text" class="form-control" id="inputRGResponsavel" value="" placeholder="RG" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-address-card-o fa-fw" aria-hidden="true"></i></span>    <input type="text" class="form-control" id="inputCPFResponsavel" value="" placeholder="CPF" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-phone fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputTelResponsavel" value="" placeholder="Telefone" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar-o fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputNascResponsavel" value="" placeholder="Data de Nascimento" disabled></div></div>';
    inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputEnderecoResponsavel" value="" placeholder="Endereço" disabled></div></div>';
    inforesp += '<div class="form-group"><a class="btn btn-carousel btn-carousel-danger btn-block" id="btnDesvincular">Desvincular</a></div>'
    $('#info-resp').html(inforesp);
  
    $('#inputNomeResponsavel').val(user_data.resp['nomeResp']);
    $('#inputEmailResponsavel').val(user_data.resp['emailResp']);
    user_data.resp['sexoResp'] == 'M' ? $('#inputSexoResponsavel').val("Masculino") : $('#inputSexoResponsavel').val("Feminino");
    user_data.resp['rgResp'] != '' ? $('#inputRGResponsavel').val(user_data.resp['rgResp']) : 0;
    user_data.resp['cpfResp'] != '' ? $('#inputCPFResponsavel').val(user_data.resp['cpfResp']) : 0;
    user_data.resp['telResp'] != '0' ? $('#inputTelResponsavel').val(user_data.resp['telResp'].substr(3, 20)) : 0;
    user_data.resp['nascResp'] != '0000-00-00' ? $('#inputNascResponsavel').val(user_data.resp['nascResp'].substr(8, 2) + user_data.resp['nascResp'].substr(5, 2) + user_data.resp['nascResp'].substr(0, 4)) : 0;
    user_data.resp['numeroResp'] != '0' ? $('#inputEnderecoResponsavel').val(user_data.resp['ruaResp'] + ', nº ' + user_data.resp['numeroResp'] + ', ' + user_data.resp['bairroResp'] + ', ' + user_data.resp['cidadeResp'] + ', ' + user_data.resp['estadoResp'] , ' - ', user_data.resp['cepResp']) : 0;
  
    $('#inputRGResponsavel').mask('00.000.000-0');
    $('#inputCPFResponsavel').mask('000.000.000-00');
    $('#inputTelResponsavel').mask('(00) 0000-00000');
    $('#inputNascResponsavel').mask('00/00/0000');
  }
}

function preencheResposavel () {
  $('#li-saude').html('<a data-toggle="pill" href="#info-saude">Informações Responsável</a>');
  var inforesp = '';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputCEP" name="inputCEP" value="" placeholder="Preencha o CEP"></div></div>';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><select class="form-control" id="inputEstado" name="inputEstado" placeholder="Estado">';
  inforesp += '<option value="" disabled selected>Estado</option>';
  inforesp += '<option value="AC">AC</option>';
  inforesp += '<option value="AL">AL</option>';
  inforesp += '<option value="AM">AM</option>';
  inforesp += '<option value="AP">AP</option>';
  inforesp += '<option value="BA">BA</option>';
  inforesp += '<option value="CE">CE</option>';
  inforesp += '<option value="DF">DF</option>';
  inforesp += '<option value="ES">ES</option>';
  inforesp += '<option value="GO">GO</option>';
  inforesp += '<option value="MA">MA</option>';
  inforesp += '<option value="MG">MG</option>';
  inforesp += '<option value="MS">MS</option>';
  inforesp += '<option value="MT">MT</option>';
  inforesp += '<option value="PA">PA</option>';
  inforesp += '<option value="PB">PB</option>';
  inforesp += '<option value="PE">PE</option>';
  inforesp += '<option value="PI">PI</option>';
  inforesp += '<option value="PR">PR</option>';
  inforesp += '<option value="RJ">RJ</option>';
  inforesp += '<option value="RN">RN</option>';
  inforesp += '<option value="RS">RS</option>';
  inforesp += '<option value="RO">RO</option>';
  inforesp += '<option value="RR">RR</option>';
  inforesp += '<option value="SC">SC</option>';
  inforesp += '<option value="SE">SE</option>';
  inforesp += '<option value="SP">SP</option>';
  inforesp += '<option value="TO">TO</option>';
  inforesp += '</select></div></div>';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputCidade" name="inputCidade" value="" placeholder="Cidade"></div></div>';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputBairro" name="inputBairro" value="" placeholder="Bairro"></div></div>';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputNumero" name="inputNumero" value="" placeholder="Número"></div></div>';
  inforesp += '<div class="form-group"><div class="input-group"><span class="input-group-addon"><i class="fa fa-location-arrow fa-fw" aria-hidden="true"></i></span><input type="text" class="form-control" id="inputRua" name="inputRua" value="" placeholder="Rua"></div></div>';
  $('#info-saude').html(inforesp);

  user_data['rua'] != '' ? $('#inputRua').val(user_data['rua']) : 0;
  user_data['numero'] != '0' ? $('#inputNumero').val(user_data['numero']) : 0;
  user_data['bairro'] != '' ? $('#inputBairro').val(user_data['bairro']) : 0;
  user_data['cidade'] != '' ? $('#inputCidade').val(user_data['cidade']) : 0;
  user_data['estado'] != '' ? $('#inputEstado').val(user_data['estado']) : 0;
  user_data['cep'] != '' ? $('#inputCEP').val(user_data['cep']) : 0;

  $('#inputCEP').mask('00000-000', {
    onKeyPress: function (value, event) {
      if ($('#inputCEP').val().length > 8) {
        var cep = $('#inputCEP').val().replace("-", "");
        $.getJSON("http://viacep.com.br/ws/"+ cep +"/json/", function(dados) {
          if (!("erro" in dados)) {
            $('#inputRua').val(dados.logradouro);
            $('#inputBairro').val(dados.bairro);
            $('#inputCidade').val(dados.localidade);
            $('#inputEstado').val(dados.uf);
          } else {
            console.log(dados);
            $('#modalErrosContent').html("O CEP informado é inválido!");
            $('#modalErros').modal('show');
          }
        });
      }
    }
  });
  $('#inputNumero').mask('0000');
  $('#li-resp').remove();
  $('#info-resp').remove();
  $('#perfil-pills li').css('width', '50%');
}

function mascaraCampos() {
  $('#inputRG').mask('00.000.000-Z', {
    translation: {
      'Z': {
        pattern: /[0-9Xx]/
      }
    }
    , onKeyPress: function (value, event) {
      event.currentTarget.value = value.toUpperCase();
    }
  });
  $('#inputCPF').mask('000.000.000-00');
  $('#inputTel').mask('(00) 0000-0000Z', {
    translation: {
      'Z': {
        pattern: /[0-9]/, optional: true
      }
    }
  });
  $('#inputNasc').mask('00/00/0000');
  $('#inputNumeroSUS').mask('000 0000 0000 0000');
}

$(window).on('load', function () {
  console.log("Arquivo perfil.js carregado!");
  preencheGeral();
  if (user_data.tipo == 1) {
    preencheDependente();
  } else if (user_data.tipo == 2) {
    preencheResposavel();
  }
  mascaraCampos();

  $('#li-geral').on('click', function () {
    $('#btnSalvar').fadeIn(200);
  });

  $('#li-saude').on('click', function () {
    $('#btnSalvar').fadeIn(200);
  });

  $('#li-resp').on('click', function () {
    $('#btnSalvar').fadeOut(200);
  });

  $('#addMedicamento').on('click', function () {
    countMedicamento++;
    $('#showMedicamentos').prepend(
      $('<div class="input-group groupMedicamento"><input type="text" class="form-control inputMedicamento" placeholder="Medicamento" maxlength="30" id="medicamento-' + countMedicamento + '" name="medicamento-' + countMedicamento + '" value =""><span class="input-group-addon btn-input-group remove-campo"><i class="fa fa-times fa-fw" aria-hidden="true"></i></span></div>').hide().fadeIn('slow')
    );
  });

  $('#addEnfermidade').on('click', function () {
    countEnfermidade++;
    $('#showEnfermidades').prepend(
      $('<div class="input-group groupEnfermidade"><input type="text" class="form-control inputEnfermidade" placeholder="Enfermidade" maxlength="30" id="enfermidade-' + countEnfermidade + '" name="enfermidade-' + countEnfermidade + '" value =""><span class="input-group-addon btn-input-group remove-campo"><i class="fa fa-times fa-fw" aria-hidden="true"></i></span></div>').hide().fadeIn('slow')
    );
  });

  $(document).on('click', '.remove-campo', function(e) {
    e.preventDefault();
    $(this).closest('.input-group').remove();
    return false;
 });

  $('#addResponsavel').on('click', function () {
    var salt = "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj"; // = S@LuS-$-2O17-!-m@D3us-%-Th13Go-&-B1g0d3-*-Br#Br#
    var resp = $('#infoResponsavel').val();
    if (validaEmail(resp)) {
      $.ajax({
        type: "POST",
        url: "http://salus.ml/app/scripts/vincula-responsavel.php",
        crossDomain: true,
        data: 'emailResponsavel=' + resp + '&codUsuario=' + user_data['cod'] + '&hash=' + salt,
        success: function (data) {
            var data = JSON.parse(data);
            if (data.status == "success") {
                $('#modalSuccessContent').html("O responsável " + data.nomeResp + " foi vinculado com sucesso! Atualizaremos a página em 5 segundos!");
                $('#modalSuccess').modal('show');
                delete data.status;
                user_data['resp'] = data;
                localStorage.setItem('salus_user_info', JSON.stringify(user_data));
                setTimeout( function (){
                  location.reload();
                }, 5000);
            } else {
                $('#modalErrosContent').html(data.msg);
                $('#modalErros').modal('show');
            }
        }
      });
    }
  });

  $('#btnDesvincular').on('click', function () {
    var salt = "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj"; // = S@LuS-$-2O17-!-m@D3us-%-Th13Go-&-B1g0d3-*-Br#Br#
    $.ajax({
      type: "POST",
      url: "http://salus.ml/app/scripts/vincula-responsavel.php",
      crossDomain: true,
      data: 'codDependente=' + user_data.cod + '&codResponsavel=' + user_data.resp.codResp + '&desvincula=1' +  '&hash=' + salt,
      success: function (data) {
          var data = JSON.parse(data);
          if (data.status == "success") {
              $('#modalSuccessContent').html(data.msg + ' Atualizaremos a página em 5 segundos!');
              $('#modalSuccess').modal('show');
              user_data.resp = [];
              localStorage.setItem('salus_user_info', JSON.stringify(user_data));
              $('#info-resp').html('<div class="form-group"><div class="input-group"><input type="email" class="form-control" id="infoResponsavel" placeholder="Digite o email do responsável" maxlength="50"><span class="input-group-btn"><a class="btn" id="addResponsavel">Vincular</a></span></div></div>');
              setTimeout( function (){
                location.reload();
              }, 5000);
          } else {
              $('#modalErrosContent').html(data.msg);
              $('#modalErros').modal('show');
          }
      }
    });
  });

  $('#btnSalvar').on('click', function () {
    var erro = [];
    erro = validaCampo(erro, $('#inputNome').val(), 'Nome Completo', 6, 50, 'texto', true);
    erro = validaCampo(erro, $('#inputEmail').val(), 'Email', 4, 50, 'email', true);
    erro = validaCampo(erro, $('#inputSexo').val(), 'Sexo', 1, 1, 'checkbox', true);
    erro = validaCampo(erro, $('#inputRG').val(), 'RG', 12, 13, 'texto', true);
    erro = validaCampo(erro, $('#inputCPF').val(), 'CPF', 14, 14, 'texto', true);
    erro = validaCampo(erro, $('#inputTel').val(), 'Telefone', 14, 15, 'texto', true);
    erro = validaCampo(erro, $('#inputNasc').val(), 'Data de Nascimento', 10, 10, 'texto', true);
    erro = validaCampo(erro, $('#inputSenhaAntiga').val(), 'Senha Antiga', 6, 24, 'texto', false);
    erro = validaCampo(erro, $('#inputSenhaNova').val(), 'Senha Nova', 6, 24, 'texto', false);
    erro = validaCampo(erro, $('#inputSenhaNovaConfirm').val(), 'Senha Nova Confirmação', 6, 24, 'texto', false);
    if (user_data['tipo'] == 1) {
      erro = validaCampo(erro, $('#inputPlanoSaude').val(), 'Plano de Saúde', 2, 20, 'checkbox', true);
      erro = validaCampo(erro, $('#inputNumeroPlano').val(), 'Número do plano de Saúde', 6, 30, 'texto', false);
      erro = validaCampo(erro, $('#inputNumeroSUS').val(), 'Número do SUS', 18, 18, 'checkbox', true);
      erro = validaCampo(erro, $('#inputBlood').val(), 'Tipo sanguíneo', 2, 5, 'checkbox', true);
    } else if (user_data['tipo'] == 2) {
      erro = validaCampo(erro, $('#inputRua').val(), 'Rua', 4, 50, 'texto', true);
      erro = validaCampo(erro, $('#inputNumero').val(), 'Número', 1, 4, 'texto', true);
      erro = validaCampo(erro, $('#inputBairro').val(), 'Bairro', 4, 50, 'texto', true);
      erro = validaCampo(erro, $('#inputCidade').val(), 'Cidade', 4, 50, 'texto', true);
      erro = validaCampo(erro, $('#inputEstado').val(), 'Estado', 2, 2, 'texto', true);
      erro = validaCampo(erro, $('#inputCEP').val(), 'CEP', 9, 9, 'texto', true);
    }
    if (!validaCpf($('#inputCPF').val())) {
      erro.push('<li>CPF inválido!</li>');
    }
    if (!validaTel($('#inputTel').val())) {
      erro.push('<li>Telefone inválido!</li>');
    }
    if (jQuery.isEmptyObject(erro)) {
      var salt = "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj"; // = S@LuS-$-2O17-!-m@D3us-%-Th13Go-&-B1g0d3-*-Br#Br#
      var resp = $('#formPefil :input').filter(function(index, element) {
        return $(element).val() != '';
      }).serialize();
      $.ajax({
        type: "POST",
        url: "http://salus.ml/app/scripts/edt-perfil.php",
        crossDomain: true,
        data: 'inputNome=' + $('#inputNome').val() + '&inputEmail=' + $('#inputEmail').val() + '&' + resp + '&hash=' + salt + '&id=' + user_data['cod'] + '&tipo=' + user_data['tipo'],
        success: function (ret) {
            var data = JSON.parse(ret);
            if (data.status == "success") {
                user_data['cpf'] = data.cpf;
                user_data['rg'] = data.rg;
                user_data['nasc'] = data.nasc;
                user_data['sexo'] = data.sexo;
                user_data['tel'] = data.tel;
                if (user_data['tipo'] == 1) {
                  user_data['planoSaude'] = data.planoSaude;
                  user_data['numPlanoSaude'] = data.numPlanoSaude;
                  user_data['numSus'] = data.numSus;
                  user_data['tipoSanguineo'] = data.tipoSanguineo;
                  user_data['enfermidades'] = data.enfermidades;
                  user_data['medicacoes'] = data.medicamentos;
                } else if (user_data['tipo'] == 2) {
                  user_data['rua'] = data.rua;
                  user_data['numero'] = data.numero;
                  user_data['bairro'] = data.bairro;
                  user_data['cidade'] = data.cidade;
                  user_data['estado'] = data.estado;
                  user_data['cep'] = data.cep;
                }
                localStorage.setItem('salus_user_info', JSON.stringify(user_data));
                $('#modalSuccessContent').html("Dados alterados com sucesso!");
                $('#modalSuccess').modal('show');
            } else {
                $('#modalErrosContent').html(data.msg);
                $('#modalErros').modal('show');
            }
        }
      });
    } else {
      $('#modalErrosContent').html(erro);
      $('#modalErros').modal('show');
      console.log(erro);
    }
  });
});