$(function () {
	console.log("Arquivo index.js carregado!");
	$('#myCarousel').carousel({
		interval: 0
	});
	// $('#myCarousel').bind('mousewheel DOMMouseScroll', function (e) {
	// 	if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0 && slideAtual == 1) {
	// 		$(this).carousel('prev');
	// 	} else {
	// 		$(this).carousel('next');
	// 	}
	// });

	// $("#myCarousel").on("touchstart", function (event) {
	// 	var yClick = event.originalEvent.touches[0].pageY;
	// 	$(this).one("touchmove", function (event) {
	// 		var yMove = event.originalEvent.touches[0].pageY;
	// 		if (Math.floor(yClick - yMove) > 1) {
	// 			$(".carousel").carousel('next');
	// 		} else if (Math.floor(yClick - yMove) < -1) {
	// 			$(".carousel").carousel('prev');
	// 		}
	// 	});
	// 	$(".carousel").on("touchend", function () {
	// 		$(this).off("touchmove");
	// 	});
	// });
	$(function () {
		$.fn.extend({
			animateCss: function (animationName) {
				var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				this.addClass('animated ' + animationName).one(animationEnd, function () {
					$(this).removeClass(animationName);
				});
			}
		});
		$('.item1.active img').animateCss('slideInDown');
		$('.item1.active h2').animateCss('zoomIn');
		$('.item1.active p').animateCss('fadeIn');
	});
	$("#myCarousel").on('slide.bs.carousel', function () {
		$.fn.extend({
			animateCss: function (animationName) {
				var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				this.addClass('animated ' + animationName).one(animationEnd, function () {
					$(this).removeClass(animationName);
				});
			}
		});
		$('.item1 img').animateCss('slideInDown');
		$('.item1 h2').animateCss('zoomIn');
		$('.item1 p').animateCss('fadeIn');
		$('.item2 img').animateCss('pulse');
		$('.item2 h2').animateCss('flash');
		$('.item2 p').animateCss('fadeIn');
		$('.item3 img').animateCss('fadeInLeft');
		$('.item3 h2').animateCss('fadeInDown');
		$('.item3 p').animateCss('fadeIn');
	});
});