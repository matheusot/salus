function geraChave(length, chars) {
    var mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    var result = '';
    for (var i = length; i > 0; --i) result += mask[Math.round(Math.random() * (mask.length - 1))];
    console.log("Chave gerada: " + result);
    return result;
}

function formCadValidate() {
    var nome = $('#inputNome').val();
    var email = $('#inputEmail').val();
    var senha = $('#inputSenha').val();
    var senhaConfirm = $('#inputSenhaConfirm').val();
    var sexo = $('#inputSexo').val();
    var erro = [];
    erro = validaCampo(erro, nome, 'Nome', 4, 50, 'texto');
    erro = validaCampo(erro, email, 'Email', 6, 50, 'email');
    erro = validaCampo(erro, senha, 'Senha', 6, 24, 'texto');
    erro = validaCampo(erro, senhaConfirm, 'Confirmação de Senha', 6, 24, 'texto');
    erro = validaCampo(erro, sexo, 'Sexo', 0, 0, 'checkbox');
    if (senhaConfirm !== senha) {
        erro.push("<li>A senha e a sua confirmação devem ser iguais!</li>");
    }
    if (jQuery.isEmptyObject(erro)) {
        return true;
    } else {
        $('#modalErrosContent').html(erro);
        $('#modalErros').modal('show');
        return false;
    }
}

$(window).on('load', function () {
    console.log("Arquivo cadastro.js carregado!");
    $("#cadastroBtnSubmit").on("click", function () {
        event.preventDefault();
        if (formCadValidate()) {
            var salt = "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj"; // = S@LuS-$-2O17-!-m@D3us-%-Th13Go-&-B1g0d3-*-Br#Br#
            var chave = geraChave(32, '#aA') + salt;
            chaveCript = btoa(chave);
            $.ajax({
                type: "POST",
                url: "http://app.salus.ml/scripts/cad-user.php",
                crossDomain: true,
                data: $("#formCadastro").serialize() + '&ChaveCriptografada=' + chaveCript + '&hash=' + salt,
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.status == "success") {
                        $('#modalSuccessContent').html("Conta criada com sucesso!");
                        $('#modalSuccess').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#modalSuccess').modal('show');
                        setTimeout(function () {
                            document.location.href = 'login.html';
                        }, 5000);
                    } else {
                        $('#modalErrosContent').html(data.msg);
                        $('#modalErros').modal('show');
                    }
                }
            });
        }
    });
});