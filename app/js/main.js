var nome, tipo, status, cod, avatar;

function startNav() {
  if ($("body").hasClass("active")) {
    closeNav();
  } else {
    openNav();
  }
}

function openNav() {
  var param = "calc(100% - 80px)";
  document.getElementById("mySidenav") !== null ? document.getElementById("mySidenav").style.width = param : 0;
  document.getElementById("slide-nav") !== null ? document.getElementById("slide-nav").style.marginLeft = param : 0;
  document.getElementById("navbar-btn") !== null ? document.getElementById("navbar-btn").style.marginLeft = param : 0;
  document.getElementById("main") !== null ? document.getElementById("main").style.marginLeft = param : 0;
  document.getElementById("btn-medicacoes") !== null ? document.getElementById("btn-medicacoes").style.right = "-150px" : 0;
  document.getElementById("h2-header") !== null ? document.getElementById("h2-header").style.marginLeft = "200px" : 0;
  document.getElementById("nav-bg") !== null ? document.getElementById("nav-bg").classList.add("active") : 0;
  document.body.classList.add("active");
}

function closeNav() {
  var param = "0";
  document.getElementById("mySidenav") !== null ? document.getElementById("mySidenav").style.width = param : 0;
  document.getElementById("slide-nav") !== null ? document.getElementById("slide-nav").style.marginLeft = param : 0;
  document.getElementById("navbar-btn") !== null ? document.getElementById("navbar-btn").style.marginLeft = param : 0;
  document.getElementById("main") !== null ? document.getElementById("main").style.marginLeft = param : 0;
  document.getElementById("h2-header") !== null ? document.getElementById("h2-header").style.marginLeft = param : 0;
  document.getElementById("nav-bg") !== null ? document.getElementById("nav-bg").classList.remove("active") : 0;
  document.body.classList.remove("active");
}

function validaLogin() {
  var url_atual = window.location.href;
  var user_data = JSON.parse(localStorage.getItem('salus_user_info'));
  if (user_data == null) {
    url_atual.split("/").slice(-1)[0] == 'inicial.html' ||
    url_atual.split("/").slice(-1)[0] == 'config.html' ||
    url_atual.split("/").slice(-1)[0] == 'perfil.html'
    ? document.location.href = 'login.html' : 0;
  } else {
    url_atual.split("/").slice(-1)[0] == 'login.html' ||
    url_atual.split("/").slice(-1)[0] == 'cadastro.html' ||
    url_atual.split("/").slice(-1)[0] == 'index.html'
    ? document.location.href = 'inicial.html' : 0;
  }
  if (document.getElementById('sidenav-name') !== null) {
    $('#sidenav-name').html("Olá " + reduzirNome(user_data['nome'], 15));
  }
  console.log(user_data);
}

function validaCampo(erro, campo, nome_campo, min, max, tipo, required) {
  if (tipo == 'texto') {
    if (campo !== null && campo !== undefined) {
      if (required) {
        if (campo.length === 0) {
          erro.push('<li>O campo ' + nome_campo + ' deve ser preenchido!</li>');
        } else if (campo.length < min) {
          erro.push('<li>O campo ' + nome_campo + ' deve ser maior que ' + min + ' caracteres!</li>');
        } else if (campo.length > max) {
          erro.push('<li>O campo ' + nome_campo + ' pode ter no máximo ' + max + ' caracteres!</li>');
        }
      }
    } else {
      erro.push('<li>O campo ' + nome_campo + ' deve ser preenchido!</li>');
    }
  }
  if (tipo == 'email') {
    if (campo !== null && campo !== undefined) {
      if (campo.length === 0) {
        erro.push('<li>O campo ' + nome_campo + ' deve ser preenchido!</li>');
      } else if (!validaEmail(campo)) {
        erro.push('<li>Email inválido!</li>');
      }
    } else {
      erro.push('<li>O campo ' + nome_campo + ' deve ser preenchido!</li>');
    }
  }
  if (tipo == 'checkbox') {
    if (campo === null || campo === undefined) {
      erro.push('<li>O campo ' + nome_campo + ' deve ser preenchido!</li>');
    }
  }
  return erro;
}

function validaEmail(email) {
  var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
  return pattern.test(email);
}

function validaCpf(cpf) {
  var i;
  var s = cpf;
  s = s.replace(".", "");
  s = s.replace(".", "");
  s = s.replace("-","");
  var c = s.substr(0,9);
  var dv = s.substr(9,2);
  var d1 = 0;
  for (i = 0; i < 9; i++) {
    d1 += c.charAt(i)*(10-i);
  }
  if (d1 == 0) {
    return false;
  }
  d1 = 11 - (d1 % 11);
  if (d1 > 9) {
    d1 = 0;
  }
  if (dv.charAt(0) != d1) {
    return false;
  }
  d1 *= 2;
  for (i = 0; i < 9; i++) {
    d1 += c.charAt(i)*(11-i);
  }
  d1 = 11 - (d1 % 11);
  if (d1 > 9) {
    d1 = 0;
  }
  if (dv.charAt(1) != d1) {
    return false;
  }
  return true;
}

function validaTel(telefone){
  telefone = telefone.replace(/\D/g,'');
  if(!(telefone.length >= 10 && telefone.length <= 11)) {
    return false;
  }
  if (telefone.length == 11 && parseInt(telefone.substring(2, 3)) != 9) {
    return false;
  }
  for(var n = 0; n < 10; n++) {
    if(telefone == new Array(11).join(n) || telefone == new Array(12).join(n)) {
      return false;
    } 
  }
  var codigosDDD = [11, 12, 13, 14, 15, 16, 17, 18, 19,
                    21, 22, 24, 27, 28, 31, 32, 33, 34,
                    35, 37, 38, 41, 42, 43, 44, 45, 46,
                    47, 48, 49, 51, 53, 54, 55, 61, 62,
                    64, 63, 65, 66, 67, 68, 69, 71, 73,
                    74, 75, 77, 79, 81, 82, 83, 84, 85,
                    86, 87, 88, 89, 91, 92, 93, 94, 95,
                    96, 97, 98, 99];
  if(codigosDDD.indexOf(parseInt(telefone.substring(0, 2))) == -1) {
    return false;
  }
  if (telefone.length == 10 && [2, 3, 4, 5, 7].indexOf(parseInt(telefone.substring(2, 3))) == -1){
    return false;
  }
  return true;
}

function validaConexao() {
  console.log("Conectado: " + navigator.onLine);
  return navigator.onLine;
}

function reduzirNome(texto, tamanho) {
  if (texto.length > (tamanho - 2)) {
    palavras = texto.split(' ');
    nome = palavras[0];
    palavras = texto.split(' ');
    sobrenome = palavras[palavras.length - 1].trim();
    var ult_posicao = palavras.length - 1;
    var meio = '';
    var a = 0;
    for (a = 1; a < ult_posicao; a++) {
      var concatenada = nome + ' ' + meio + ' ' + sobrenome;
      if (concatenada.length < tamanho) {
        meio += ' ' + palavras[a].substr(0, 1).toUpperCase() + '.';
      }
    }
  } else {
    nome = texto;
    meio = '';
    sobrenome = '';
  }
  if ($(document).width() < 365) {
    return nome.trim();
  }
  return (nome + meio + ' ' + sobrenome).trim();
}

function checkFirstLogin() {
  var first = localStorage.getItem('salus_primeiro_login');
  if (first == 'true') {
    localStorage.removeItem('salus_primeiro_login');
    localStorage.setItem('salus_primeiro_login', false);
    $("#modalSuccess").modal('show');
  }
}

function logout() {
  localStorage.removeItem('salus_user_info');
  document.location.href = 'login.html';
}

function redirectPage() {
  window.location = linkLocation;
}

function cordovaAdjusts() {
  //ajustes para o cordova
}

$(window).on('load', function () {
  console.log("Arquivo main.js carregado!");
  validaConexao();
  validaLogin();
  cordovaAdjusts();

  $(".transition").click(function(event){
    event.preventDefault();
    linkLocation = this.href;
    $("body").fadeOut(200, redirectPage);      
  });

  $(".transition-no-link").click(function(event){
    $("body").fadeOut(200);      
  });

  $('.screenloader-img').fadeIn();
	setTimeout(function() {
		$('.screenloader-img').fadeOut();
		$('.screenloader').fadeOut();
	}, 1000);

  $("#navbar-btn").on('click', function () {
    startNav();
  });

  $("#logoutBtn").on('click', function () {
    logout();
  });
});