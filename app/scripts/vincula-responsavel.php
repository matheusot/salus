<?php
$erro = "";
$msg = "";

require_once("include/valida.inc");

function vinculaResp($id, $email) {
    require_once("include/connect.inc");
    $sql = "SELECT * FROM usuarios INNER JOIN responsaveis ON `usuarios`.cod_usuario = `responsaveis`.cod_usuario WHERE `usuarios`.cod_usuario = (SELECT cod_usuario FROM usuarios WHERE email_usuario = ?)";
    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $email);
    $stmt->execute();
    $rows = $stmt->rowCount();
    $registro = $stmt->fetch(PDO::FETCH_OBJ);
    if ($rows == 0) {
        $msg = "Usuário não encontrado!";
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    } elseif ($registro->tipo_usuario == '1') {
        $msg = "O usuário a ser vinculado deve ser do tipo Responsável!";
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    } elseif ($registro->cod_usuario == $id) {
        $msg = "Você não pode se vincular como responsável!";
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    } else {
        $idResp = $registro->cod_usuario;
        $sql = "INSERT INTO dependencias VALUES (?, ?)";
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->bindParam(2, $idResp);
        $stmt->execute();
        $e = $stmt->errorCode();
        if ($e != "00000") {
            if (($e==23000) || ($e==1062)) {
                $msg = "Você já possui um responsável vinculado!";
                echo json_encode(array('status' => 'error', 'msg' => $msg));
            } else {
              $msg = "Erro desconhecido!";
              echo json_encode(array('status' => 'error', 'msg' => $msg));
            }
        } else {
            echo json_encode(
                array('status' => 'success',
                      'codResp' => $idResp,
                      'nomeResp' => $registro->nome_completo_usuario,
                      'emailResp' => $registro->email_usuario,
                      'sexoResp' => $registro->sexo_usuario,
                      'rgResp' => $registro->rg_usuario,
                      'cpfResp' => $registro->cpf_usuario,
                      'telResp' => $registro->tel_usuario,
                      'nascResp' => $registro->nasc_usuario,
                      'ruaResp' => $registro->rua,
                      'numeroResp' => $registro->numero,
                      'bairroResp' => $registro->bairro,
                      'cidadeResp' => $registro->cidade,
                      'estadoResp' => $registro->estado,
                      'cepResp' => $registro->cep
                    ));
        }
    }
}

function desvinculaResp($cod_res, $cod_dp) {
    require_once("include/connect.inc");
    $sql = "DELETE FROM dependencias WHERE cod_dependente = ? AND cod_responsavel = ?";
    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $cod_dp);
    $stmt->bindParam(2, $cod_res);
    $stmt->execute();
    $rows = $stmt->rowCount();
    $e = $stmt->errorCode();
    if ($rows == 0) {
        $msg = "Usuário não encontrado!";
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    } else {
        if ($e != "00000") {
              $msg = "Erro desconhecido!";
              echo json_encode(array('status' => 'error', 'msg' => $msg));
        } else {
            $msg = "Responsável desvinculado com sucesso!";
            echo json_encode(
                array('status' => 'success',
                      'msg' => $msg));
        }
    }
}

if (!isset($_POST['desvincula'])) {
    if (!isset($_POST['hash']) ||
    !isset($_POST['emailResponsavel']) ||
    !isset($_POST['codUsuario']) ||
    empty($_POST['hash']) ||
    empty($_POST['emailResponsavel']) ||
    empty($_POST['codUsuario'])) {
        header("Location: http://salus.ml");
        exit();
    }

    if ($_POST['hash'] != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
        $msg = "Erro desconhecido! Tente novamente!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }

    if (valida($_POST['emailResponsavel'], 6, 64, true, 3, "Email")) {
        $email = $_POST['emailResponsavel'];
    } else {
        $msg = "Email inválido!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }

    if (valida($_POST['codUsuario'], 0, 10, true, 1, "Cod")) {
        $id = $_POST['codUsuario'];
    } else {
        $msg = "Código de usuário inválido! Tente novamente ou relogue!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }
} else {
    if (!isset($_POST['hash']) ||
    !isset($_POST['codDependente']) ||
    !isset($_POST['codResponsavel']) ||
    empty($_POST['hash']) ||
    empty($_POST['codDependente']) ||
    empty($_POST['codResponsavel'])) {
        header("Location: http://salus.ml");
        exit();
    }

    if ($_POST['hash'] != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
        $msg = "Erro desconhecido! Tente novamente!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }

    if (valida($_POST['codResponsavel'], 0, 10, true, 1, "Cod Responsável")) {
        $cod_res = $_POST['codResponsavel'];
    } else {
        $msg = "Código de usuário inválido! Tente novamente ou relogue!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }

    if (valida($_POST['codDependente'], 0, 10, true, 1, "Cod Dependente")) {
        $cod_dp = $_POST['codDependente'];
    } else {
        $msg = "Código de usuário inválido! Tente novamente ou relogue!";
        $erro = true;
        echo json_encode(array('status' => 'error', 'msg' => $msg));
    }
}

if (!$erro) {
    if (isset($_POST['desvincula'])) {
        desvinculaResp($cod_res, $cod_dp);
    } else {
        vinculaResp($id, $email);
    }
}

?>
