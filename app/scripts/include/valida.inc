<?php
$hash = "S@LuS-$-2O17-!-m@D3us-%-Th13Go-&-B1g0d3-*-Br#Br#";

function valida($campo, $min, $max, $req, $tipo, $nome) { //1 = text / combobox | 2 = number | 3 = email | 4 = url
  $erro = '';
  if ($req) {
    if ((!isset($campo)) || (empty($campo))) {
      $erro = true;
      $msg = "O campo $nome deve ser preenchido!";
      //echo "$nome = 1";
      return false;
    }
    if ($max>0) {
      if (strlen(utf8_decode(trim(strip_tags($campo))))>$max) {
        $erro = true;
        $msg = "O campo $nome deve ter no máximo $max caracteres!";
        //echo "$nome = 2";
        return false;
      }
    }
    if (strlen(utf8_decode(trim(strip_tags($campo))))<$min && $req) {
      $erro = true;
      $msg = "O campo $nome deve ter no mínimo $min caracteres!";
      //echo "$nome = 3";
      return false;
    }
  }
  if ($tipo==2) {
    if (!is_numeric($campo)) {
      $erro = true;
      $msg = "O campo $nome deve ser numérico!";
      //echo "$nome = 4";
      return false;
    }
  }
  if ($tipo==3) {
    if (!filter_var($campo, FILTER_VALIDATE_EMAIL)) {
      $erro = true;
      $msg = "O campo $nome deve ser um email válido!";
      //echo "$nome = 5";
      return false;
    }
  }
  if ($tipo==4) {
    if (!filter_var($campo, FILTER_VALIDATE_URL)) {
      $erro = true;
      $msg = "O campo $nome deve ser uma URL válida!";
      //echo "$nome = 6";
      return false;
    }
  }
  if ($erro != true){
    //echo "<br>$nome = ok<br>$campo<br>";
    return true;
  }
}
?>
