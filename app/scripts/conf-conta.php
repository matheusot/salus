<?php
session_start();
$count = 0;
if (isset($_GET['c'])) {
  if (!empty($_GET['c'])) {
    $uuid64 = $_GET['c'];
    $uuid = base64_decode($uuid64);

    require_once("include/connect.inc");
    $stmt = $conn->prepare("UPDATE usuarios SET status_usuario = ? WHERE uuid = ?");
    if (isset($_GET['adm'])) {
        $status = '3';
    } else {
        $status = '1';
    }

    $stmt->bindParam(1, $status);
    $stmt->bindParam(2, $uuid);
    $stmt->execute();
    $count = $stmt->rowCount()+1;
    if ($stmt->errorCode() != "00000") {
      $uuid = null;
    }
  } else {
    $uuid = null;
  }
} else {
  Header("Location: http://www.salus.ml");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Salus - Confirmação de Conta</title>
		<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen,projection">
		<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen,projection">
		<link href="../css/main.css" rel="stylesheet" type="text/css" media="screen,projection">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <style>
    body {
      overflow-x: hidden;
    }
    </style>
	</head>
	<body>
		<!-- Header -->
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
					<center>
						<h2 class="h2-header" id="h2-header">CONFIRMAÇÃO DE CONTA</h2>
					</center>
			</div>
		</nav>
		<!-- End-header -->
    <div class="container">
      <center>
      <?php
        if (($count == 0) || ($uuid == null)){ ?>
          <div class="modal-dialog modal-erros" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h4 class="modal-title">Erro ao confirmar conta!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="modal-text">Tente novamente mais tarde!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <?php
        } else { ?>
          <div class="modal-dialog modal-success" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h4 class="modal-title">Conta confirmada com sucesso!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="modal-text">Faça login no aplicativo!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <?php
        }
      ?>
      </center>
    </div>
  </body>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/app.js"></script>
  </html>
