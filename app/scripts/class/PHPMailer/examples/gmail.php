<?php
require '../PHPMailerAutoload.php';

$mail = new PHPMailer;

$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "projetonfccotil@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "projetonfc123";

//Set who the message is to be sent from
$mail->setFrom('projetonfccotil@gmail.com', 'Administração - DinfoApp');

//Set an alternative reply-to address
$mail->addReplyTo('projetonfccotil@gmail.com', 'Administração - DinfoApp');

//Set who the message is to be sent to
$mail->addAddress('projetonfccotil@gmail.com', 'Projeto NFC COTIL');

//Set the subject line
$mail->Subject = 'Assunto - Email de teste';

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$msg = "<h1>Bem vindo ao DinfoApp!</h1>";
$msg .= "<p>Clique <a href='teste.php'>aqui</a> para confirmar sua conta!</p>";
$mail->msgHTML($msg);

//Replace the plain text body with one created manually
$mail->AltBody = 'Bem vindo ao DinfoApp.';

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
