<?php
$erro = "";
$msg = "";

require_once("include/valida.inc");

function edita($id, $tipo, $nome, $email, $sexo, $rg, $cpf, $tel, $nasc, $senhaAntiga, $senhaNova, $senhaNovaConfirm, $planoSaude, $numPlanoSaude, $numSus, $tipoSanguineo, $enfermidades, $medicamentos, $rua, $numero, $bairro, $cidade, $estado, $cep) {
    require_once("include/connect.inc");

    $rgF = str_replace('.', '', $rg);
    $rgF = str_replace('-', '', $rgF);

    $cpfF = str_replace('.', '', $cpf);
    $cpfF = str_replace('-', '', $cpfF);

    $telF = str_replace('(', '', $tel);
    $telF = str_replace(')', '', $telF);
    $telF = str_replace(' ', '', $telF);    
    $telF = str_replace('-', '', $telF);
    $telF = '550' . $telF;

    $nascF = substr($nasc, 6, 10) . '-' . substr($nasc, 3, 2) . '-' . substr($nasc, 0, 2);

    $numSusF = str_replace(' ', '', $numSus);

    $cepF = str_replace('-', '', $cep);

    if ($senhaAntiga != null && $senhaNova != null && $senhaNovaConfirm != null) {
        $sql = "UPDATE usuarios SET 
        nome_completo_usuario = ?,
        email_usuario = ?,
        sexo_usuario = ?,
        rg_usuario = ?,
        cpf_usuario = ?,
        tel_usuario = ?,
        nasc_usuario = ?,
        senha_usuario = ?
        WHERE cod_usuario = $id;";
    } else {
        $sql = "UPDATE usuarios SET 
        nome_completo_usuario = ?,
        email_usuario = ?,
        sexo_usuario = ?,
        rg_usuario = ?,
        cpf_usuario = ?,
        tel_usuario = ?,
        nasc_usuario = ?
        WHERE cod_usuario = $id;";
    }

    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $nome);
    $stmt->bindParam(2, $email);
    $stmt->bindParam(3, $sexo);
    $stmt->bindParam(4, $rgF);
    $stmt->bindParam(5, $cpfF);
    $stmt->bindParam(6, $telF);
    $stmt->bindParam(7, $nascF);
    if ($senhaAntiga != null && $senhaNova != null && $senhaNovaConfirm != null) {
        $stmt->bindParam(8, $senhaNova);
    }

    $stmt->execute();

    if($stmt->errorCode() != "00000"){
        $msg = "Erro desconhecido! Tente novamente!";
        echo json_encode(array('status' => 'error', 'msg' => $msg, 'erro' => $stmt->errorInfo()));
    }

    if ($tipo == 1) {
        $sqlDel = "DELETE FROM enfermidades WHERE cod_usuario = $id;
        DELETE FROM medicacoes WHERE cod_usuario = $id;";
        $conn->exec($sqlDel);

        $sqlMed = '';
        if (isset($medicamentos)) {
            foreach ($medicamentos as $key => $value) {
                $sqlMed .= "INSERT INTO medicacoes (cod, cod_usuario, nome_medicacao) VALUES (null, $id, '" . $value . "');";
            }
            $conn->exec($sqlMed);
        }

        $sqlEnf = '';
        if (isset($enfermidades)) {
            foreach ($enfermidades as $key => $value) {
                $sqlEnf .= "INSERT INTO enfermidades (cod, cod_usuario, nome_enfermidade) VALUES (null, $id, '" . $value . "');";
            }
            $conn->exec($sqlEnf);
        }

        $sql = "SELECT cod_usuario FROM dependentes WHERE cod_usuario = ?;";
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $rows = $stmt->rowCount();
    
        if ($rows==0) {
            $sql = "INSERT INTO dependentes (cod_usuario) VALUES (?);";
            $conn->exec($sql);
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(1, $id);
            $stmt->execute();
    
            if($stmt->errorCode() != "00000"){
                $msg = "Erro desconhecido! Tente novamente!";
                echo json_encode(array('status' => 'error', 'msg' => $msg, 'erro' => $stmt->errorInfo()));
            }
        }
    
        $sql = "UPDATE dependentes SET 
                plano_de_saude_dependente = ?,
                numero_plano_saude_dependente = ?,
                numero_sus_dependente = ?,
                tipo_sanguineo_dependente = ?
                WHERE cod_usuario = ?;";
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $planoSaude);
        $stmt->bindParam(2, $numPlanoSaude);
        $stmt->bindParam(3, $numSusF);
        $stmt->bindParam(4, $tipoSanguineo);
        $stmt->bindParam(5, $id);
        $stmt->execute();
        $rows = $stmt->rowCount();
    
        echo json_encode(
            array('status' => 'success',
                    'cpf' => $cpfF,
                    'rg' => $rgF,
                    'nasc' => $nascF,
                    'sexo' => $sexo,
                    'tel' => $telF,
                    'planoSaude' => $planoSaude,
                    'numPlanoSaude' => $numPlanoSaude,
                    'numSus' => $numSus,
                    'tipoSanguineo' => $tipoSanguineo,
                    'enfermidades' => $enfermidades,
                    'medicamentos' => $medicamentos
                    ));

    } else if ($tipo == 2) {
        $sql = "UPDATE responsaveis SET 
        rua = ?,
        numero = ?,
        bairro = ?,
        cidade = ?,
        estado = ?,
        cep = ?
        WHERE cod_usuario = ?;";
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $rua);
        $stmt->bindParam(2, $numero);
        $stmt->bindParam(3, $bairro);
        $stmt->bindParam(4, $cidade);
        $stmt->bindParam(5, $estado);
        $stmt->bindParam(6, $cepF);
        $stmt->bindParam(7, $id);
        $stmt->execute();
        $rows = $stmt->rowCount();

        echo json_encode(
            array('status' => 'success',
                    'cpf' => $cpfF,
                    'rg' => $rgF,
                    'nasc' => $nascF,
                    'sexo' => $sexo,
                    'tel' => $telF,
                    'rua' => $rua,
                    'numero' => $numero,
                    'bairro' => $bairro,
                    'cidade' => $cidade,
                    'estado' => $estado,
                    'cep' => $cepF
                ));
    }
}

if (!isset($_POST['hash']) || empty($_POST['hash'])) {
    Header("Location: http://www.salus.ml");
    exit();
}

if (!isset($_POST['inputNome']) ||
    !isset($_POST['inputEmail']) ||
    !isset($_POST['inputSexo']) ||
    !isset($_POST['inputRG']) ||
    !isset($_POST['inputCPF']) ||
    !isset($_POST['inputTel']) ||
    !isset($_POST['inputNasc']) ||
    !isset($_POST['tipo']) ||
    empty($_POST['inputNome']) ||
    empty($_POST['inputEmail']) ||
    empty($_POST['inputSexo']) ||
    empty($_POST['inputRG']) ||
    empty($_POST['inputCPF']) ||
    empty($_POST['inputTel']) ||
    empty($_POST['inputNasc']) ||
    empty($_POST['tipo'])) {
    $msg = "Campos faltam!";
    echo json_encode(array('status' => 'error', 'msg' => $msg));
    exit();
}

if ($_POST['hash'] != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
    $msg = "Erro desconhecido! Tente novamente!";
    echo json_encode(array('status' => 'error', 'msg' => $msg));
}

$id = null;
$nome = null;
$email = null;
$sexo = null;
$rg = null;
$cpf = null;
$tel = null;
$nasc = null;
$senhaAntiga = null;
$senhaNova = null;
$senhaNovaConfirm = null;
$planoSaude = null;
$numPlanoSaude = null;
$numSus = null;
$tipoSanguineo = null;
$enfermidades = null;
$medicamentos = null;
$rua = null;
$numero = null;
$bairro = null;
$cidade = null;
$estado = null;
$cep = null;

    $id = $_POST['id'];

if (valida($_POST['inputNome'], 6, 50, true, 1, "Nome Completo")) {
    $nome = $_POST['inputNome'];
}

if (valida($_POST['inputEmail'], 4, 50, true, 3, "Email")) {
    $email = $_POST['inputEmail'];
}

if (valida($_POST['inputSexo'], 1, 1, true, 1, "Sexo")) {
    $sexo = $_POST['inputSexo'];
}

if (valida($_POST['inputRG'], 12, 13, true, 1, "RG")) {
    $rg = $_POST['inputRG'];
}

if (valida($_POST['inputCPF'], 14, 14, true, 1, "CPF")) {
    $cpf = $_POST['inputCPF'];
}

if (valida($_POST['inputTel'], 14, 15, true, 1, "Telefone")) {
    $tel = $_POST['inputTel'];
}

if (valida($_POST['inputNasc'], 10, 10, true, 1, "Data de Nascimento")) {
    $nasc = $_POST['inputNasc'];
}

if (isset($_POST['inputSenhaAntiga'])) {
    if (valida($_POST['inputSenhaAntiga'], 6, 24, false, 1, "Senha Antiga")) {
        $senhaAntiga = $_POST['inputSenhaAntiga'];
    }
}

if (isset($_POST['inputSenhaNova'])) {
    if (valida($_POST['inputSenhaNova'], 6, 24, false, 1, "Senha Nova")) {
        $senhaNova = $_POST['inputSenhaNova'];
    }
}

if (isset($_POST['inputSenhaNovaConfirm'])) {
    if (valida($_POST['inputSenhaNovaConfirm'], 6, 24, false, 1, "Senha Nova Confirmação")) {
        $senhaNovaConfirm = $_POST['inputSenhaNovaConfirm'];
    }
}

if ($_POST['tipo'] == 1) {
    if (valida($_POST['inputPlanoSaude'], 2, 20, true, 1, "Plano de Saúde")) {
        $planoSaude = $_POST['inputPlanoSaude'];
    }
    
    if (isset($_POST['inputNumeroPlano'])) {
        if (valida($_POST['inputNumeroPlano'], 6, 30, false, 1, "Número do plano de Saúde")) {
            $numPlanoSaude = $_POST['inputNumeroPlano'];
        }
    }
    
    if (valida($_POST['inputNumeroSUS'], 18, 18, false, 1, "Número do SUS")) {
        $numSus = $_POST['inputNumeroSUS'];
    }
    
    if (valida($_POST['inputBlood'], 2, 5, true, 1, "Tipo sanguíneo")) {
        $tipoSanguineo = $_POST['inputBlood'];
    }
    
    foreach($_POST as $key => $value){
        $exp_key = explode('-', $key);
        if($exp_key[0] == 'enfermidade'){
             $enfermidades[] = $value;
        }
    }
      
    foreach($_POST as $key => $value){
        $exp_key = explode('-', $key);
        if($exp_key[0] == 'medicamento'){
             $medicamentos[] = $value;
        }
    }
} else if ($_POST['tipo'] == 2) {
    if (valida($_POST['inputRua'], 1, 50, true, 1, "Rua")) {
        $rua = $_POST['inputRua'];
    }

    if (valida($_POST['inputNumero'], 0, 0, true, 2, "Número")) {
        $numero = $_POST['inputNumero'];
    }

    if (valida($_POST['inputBairro'], 3, 50, true, 1, "Bairro")) {
        $bairro = $_POST['inputBairro'];
    }

    if (valida($_POST['inputCidade'], 3, 50, true, 1, "Cidade")) {
        $cidade = $_POST['inputCidade'];
    }

    if (valida($_POST['inputEstado'], 2, 2, true, 1, "Estado")) {
        $estado = $_POST['inputEstado'];
    }

    if (valida($_POST['inputCEP'], 9, 9, true, 1, "CEP")) {
        $cep = $_POST['inputCEP'];
    }
}

if ($erro != true) {
    edita($id, $_POST['tipo'], $nome, $email, $sexo, $rg, $cpf, $tel, $nasc, $senhaAntiga, $senhaNova, $senhaNovaConfirm, $planoSaude, $numPlanoSaude, $numSus, $tipoSanguineo, $enfermidades, $medicamentos, $rua, $numero, $bairro, $cidade, $estado, $cep);
} else {
    $msg = "Erro desconhecido!";
    echo json_encode(array('status' => 'error', 'msg' => $msg));
}
?>
