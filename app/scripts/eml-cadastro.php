<?php
require 'class/PHPMailer/PHPMailerAutoload.php';

$cod = base64_encode($key);

$mail = new PHPMailer;

$mail->isSMTP();

$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->CharSet = 'UTF-8';

$mail->Host = 'mx1.hostinger.com.br';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;

$mail->Username = "naoresponda@salus.ml";
$mail->Password = "salusproj";

$mail->setFrom('naoresponda@salus.ml', 'Salus');
$mail->addReplyTo('naoresponda@salus.ml', 'Salus');
$mail->addAddress($email, $nome);

$mail->Subject = 'Salus - Confirmação de Conta';
$msg = file_get_contents('include/email-template.html');
$placeholders = array("#TITULO#", "#MENSAGEM#", "#BOTAO-LINK#", "#BOTAO-TEXTO#", "#MENSAGEM2#", "#MENSAGEM3#");
$contents = array(
              "Confirmação de Conta",
              "Obrigado por se registrar na Salus! Clique no botão abaixo para confirmar sua conta!",
              "http://www.salus.ml/app/scripts/conf-conta.php?c=$cod",
              "Confirmar conta!",
              "Caso o botão acima não funcione, copie o link abaixo no seu navegador:",
              "http://www.salus.ml/app/scripts/conf-conta.php?c=$cod"
            );
$msg = str_replace($placeholders, $contents, $msg);
$mail->msgHTML($msg);
$mail->AltBody = "Obrigado por se registrar na Salus! Clique no link para confirmar sua conta! http://www.salus.ml/app/scripts/conf-conta.php?c=$cod";

if (!$mail->send()) {
  $msg = "Erro ao enviar email de cadastro!";
  echo json_encode(array('status' => 'error', 'msg' => $msg));
} else {
  echo json_encode(array('status' => 'success', 'msg' => ''));
}

?>
