<?php
$erro = "";
$msg = "";

require_once("include/valida.inc");

function registra($nome, $email, $sexo, $senha, $tipo, $key) {
    $cod = 0;
    require_once("include/connect.inc");

    $sql = "INSERT INTO usuarios (nome_completo_usuario, email_usuario, sexo_usuario, senha_usuario, tipo_usuario, uuid)
            VALUES (?, ?, ?, ?, ?, ?)";
    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $nome);
    $stmt->bindParam(2, $email);
    $stmt->bindParam(3, $sexo);
    $stmt->bindParam(4, $senha);
    $stmt->bindParam(5, $tipo);
    $stmt->bindParam(6, $key);
    $stmt->execute();
    $e = $stmt->errorCode();
    if($e != "00000") {
        if (($e==23000) || ($e==1062)) {
            $msg = "Não podem existir dois usuários com mesmo email!";
            echo json_encode(array('status' => 'error', 'msg' => $msg));
        } else {
          $msg = "Erro desconhecido! 01";
          echo json_encode(array('status' => 'error', 'msg' => $msg));
        }
    } else {
        if ($tipo == 1) {
          $sql = "INSERT INTO dependentes (cod_usuario) SELECT cod_usuario FROM usuarios WHERE email_usuario = ?";
        } else if ($tipo == 2) {
          $sql = "INSERT INTO responsaveis (cod_usuario) SELECT cod_usuario FROM usuarios WHERE email_usuario = ?";
        }
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $email);
        $stmt->execute();
        $e = $stmt->errorCode();
        if($e != "00000") {
          echo 'Cod: '.$cod;
          $msg = "Erro desconhecido! 03";
          echo json_encode(array('status' => 'error', 'msg' => $msg));
        } else {
          require_once ("eml-cadastro.php");
        }
    }
}

if ((!isset($_POST['inputNome'])) ||
    (!isset($_POST['inputSenhaConfirm'])) ||
    (!isset($_POST['inputSenha'])) ||
    (!isset($_POST['inputSexo'])) ||
    (!isset($_POST['inputEmail'])) ||
    (!isset($_POST['inputTipo'])) ||
    (!isset($_POST['ChaveCriptografada'])) ||
    (!isset($_POST['hash']))) {
  Header("Location: http://www.salus.ml");
}

if ($_POST['hash'] != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
  $msg = "Erro desconhecido! Tente novamente!";
  echo json_encode(array('status' => 'error', 'msg' => $msg));
}

if (valida($_POST['inputNome'], 5, 64, true, 1, "Nome")) {
  $nome = $_POST['inputNome'];
}

if (valida($_POST['inputSenhaConfirm'], 6, 64, true, 1, "Confirmar senha")) {
  $senha_conf = $_POST['inputSenhaConfirm'];
}

if (valida($_POST['inputSenha'], 6, 64, true, 1, "Senha")) {
  $senha = $_POST['inputSenha'];
}

if (valida($_POST['inputSexo'], 0, 2, true, 1, "Sexo")) {
  $sexo = $_POST['inputSexo'];
}

if (valida($_POST['inputTipo'], 0, 2, true, 1, "Tipo")) {
  $tipo = $_POST['inputTipo'];
}

if (valida($_POST['inputEmail'], 6, 64, true, 3, "Email")) {
  $email = $_POST['inputEmail'];
}

if (isset($_POST['ChaveCriptografada'])) {
  $keySalted = base64_decode($_POST['ChaveCriptografada']);
  $key = substr($keySalted, 0, 32);
  $salted = substr($keySalted, 32, 2000);
  if ($salted != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
    $erro = true;
  }
} else {
  $erro = true;
}

if ($erro != true) {
  if ($senha==$senha_conf) {
    $senha = md5($senha.$hash);
    registra($nome, $email, $sexo, $senha, $tipo, $key);
  } else {
    echo json_encode(array('status' => 'error', 'msg' => $msg));
  }
} else {
  json_encode(array('status' => 'error', 'msg' => $msg));
}

?>
