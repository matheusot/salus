<?php
$erro = "";
$msg = "";

require_once("include/valida.inc");

function socorro($cod_res, $cod_dp, $leitura) {
    $cod = 0;
    require_once("include/connect.inc");

    date_default_timezone_set('America/Sao_Paulo');

    $time = time();
    $now = new DateTime();
    $now->setTimestamp($time);
    $nowF = $now->format('Y-m-d H:i:s');

    $before = new DateTime();
    $before->setTimestamp($time-600);
    $beforeF = $before->format('Y-m-d H:i:s');

    $sql = "INSERT INTO emergencias (cod_leitura, cod_responsavel, data_hora_emergencia) VALUES (
      (SELECT cod_leitura FROM leituras_dependentes WHERE cod_dependente = ? ORDER BY data LIMIT 1), ?, '" . $nowF . "'
    );";

    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $cod_dp);
    $stmt->bindParam(2, $cod_res);
    $stmt->execute();
    $e = $stmt->errorCode();
    if($e != "00000") {
      $msg = "Erro desconhecido!";
      echo json_encode(array('status' => 'error', 'msg' => $msg));
    } else {
      $msg = "O chamado de socorro foi enviado ao seu responsável!";
      echo json_encode(array('status' => 'success', 'msg' => $msg));
    }
}

if ((!isset($_POST['codResp'])) ||
    (!isset($_POST['codDep'])) ||
    (!isset($_POST['hash']))) {
  Header("Location: http://www.salus.ml");
}

if ($_POST['hash'] != "=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj") {
  $msg = "Erro desconhecido! Tente novamente!";
  echo json_encode(array('status' => 'error', 'msg' => $msg));
}

if (valida($_POST['codResp'], 0, 0, true, 2, "Código responsável")) {
  $cod_res = $_POST['codResp'];
}

if (valida($_POST['codDep'], 0, 0, true, 2, "Código dependente")) {
  $cod_dp = $_POST['codDep'];
}

if (valida($_POST['leitura'], 5, 64, true, 1, "Leitura")) {
  $leitura = $_POST['leitura'];
}

if ($erro != true) {
  socorro($cod_res, $cod_dp, $leitura);
} else {
  json_encode(array('status' => 'error', 'msg' => $msg));
}

?>
