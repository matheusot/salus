<?php
$erro = '';
$msg = '';

require_once('include/valida.inc');

function login($user, $senha) {
    require_once('include/connect.inc');

    $sql = 'SELECT cod_usuario, tipo_usuario FROM `usuarios` WHERE email_usuario = ? AND senha_usuario = ?;';
    $conn->exec($sql);
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(1, $user);
    $stmt->bindParam(2, $senha);
    $stmt->execute();
    $rows = $stmt->rowCount();
    $usuario = $stmt->fetch(PDO::FETCH_OBJ);

    if ($rows==0) {
      $msg = 'Email ou senha não encontrados!';
      echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '1'));
    } else {
      $id = $usuario->cod_usuario;
      $tipo = $usuario->tipo_usuario;
      if ($tipo == 1) {
        $sql = 'SELECT 
          u.cod_usuario,
          u.nome_completo_usuario,
          u.email_usuario,
          u.sexo_usuario,
          u.rg_usuario,
          u.cpf_usuario,
          u.tel_usuario,
          u.nasc_usuario,
          u.senha_usuario,
          u.tipo_usuario,
          u.status_usuario,
          u.uuid,
          d.plano_de_saude_dependente,
          d.numero_plano_saude_dependente,
          d.numero_sus_dependente,
          d.tipo_sanguineo_dependente
        FROM `usuarios` AS `u` INNER JOIN `dependentes` AS `d` ON u.cod_usuario = d.cod_usuario WHERE u.cod_usuario = ?;';
        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $rows = $stmt->rowCount();
        $registro = $stmt->fetch(PDO::FETCH_OBJ);

        if ($rows==0) {
          $msg = 'Email ou senha não encontrados!';
          echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '2'));
        } else {
          if ($registro->status_usuario == '0') {
                $msg = '<p>Você precisa confirmar sua conta!</p><p>Veja o email que enviamos para você no registro!</p>';
                $msg += '<a id="btnEnviarNovamente">Não recebi o email</a>';
                echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '3'));
            } else {
                if($stmt->errorCode() != '00000'){
                  $msg = 'Erro desconhecido! Tente novamente!';
                  echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '4'));
                } else {

                  $sql = 'SELECT * FROM `usuarios` INNER JOIN `responsaveis` ON `usuarios`.cod_usuario = `responsaveis`.cod_usuario WHERE `usuarios`.cod_usuario =      (SELECT cod_responsavel FROM `dependencias` WHERE cod_dependente = ?);';
                  $conn->exec($sql);
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam(1, $id);
                  $stmt->execute();
                  $rowsResp = $stmt->rowCount();
                  $responsavel = $stmt->fetch(PDO::FETCH_OBJ);                  

                  $sql = 'SELECT nome_enfermidade FROM `enfermidades` WHERE cod_usuario = ?;';
                  $conn->exec($sql);
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam(1, $id);
                  $stmt->execute();
                  $rows = $stmt->rowCount();
                  $enfermidades = [];
                  while ($enfermidade = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $enfermidades[] = $enfermidade['nome_enfermidade'];
                  }

                  $sql = 'SELECT nome_medicacao FROM `medicacoes` WHERE cod_usuario = ?;';
                  $conn->exec($sql);
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam(1, $id);
                  $stmt->execute();
                  $rows = $stmt->rowCount();
                  $medicacoes = [];
                  while ($medicacao = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $medicacoes[] = $medicacao['nome_medicacao'];
                  }

                  $img = 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($user))) . '?s=256&d=mm&r=g';
                  if ($rowsResp == 0) {
                    $arrayResp = array();
                  } else {
                    $arrayResp = array(
                      'codResp' => $responsavel->cod_usuario,
                      'nomeResp' => $responsavel->nome_completo_usuario,
                      'emailResp' => $responsavel->email_usuario,
                      'sexoResp' => $responsavel->sexo_usuario,
                      'rgResp' => $responsavel->rg_usuario,
                      'cpfResp' => $responsavel->cpf_usuario,
                      'telResp' => $responsavel->tel_usuario,
                      'nascResp' => $responsavel->nasc_usuario,
                      'ruaResp' => $responsavel->rua,
                      'numeroResp' => $responsavel->numero,
                      'bairroResp' => $responsavel->bairro,
                      'cidadeResp' => $responsavel->cidade,
                      'estadoResp' => $responsavel->estado,
                      'cepResp' => $responsavel->cep
                    );
                  }
                  echo json_encode(
                    array('status' => 'success',
                          'cod' => $registro->cod_usuario,
                          'cpf' => $registro->cpf_usuario,
                          'rg' => $registro->rg_usuario,
                          'nasc' => $registro->nasc_usuario,
                          'sexo' => $registro->sexo_usuario,
                          'tel' => $registro->tel_usuario,
                          'nome' => $registro->nome_completo_usuario,
                          'email' => $registro->email_usuario,
                          'tipo' => $registro->tipo_usuario,
                          'avatar' => $img,
                          'stats' => $registro->status_usuario,
                          'uuid' => $registro->uuid,
                          'planoSaude' => $registro->plano_de_saude_dependente,
                          'numPlanoSaude' => $registro->numero_plano_saude_dependente,
                          'numSus' => $registro->numero_sus_dependente,
                          'tipoSanguineo' => $registro->tipo_sanguineo_dependente,
                          'enfermidades' => $enfermidades,
                          'medicacoes' => $medicacoes,
                          'resp' => $arrayResp                      
                        ));
            }
          }
        }
      } else if ($tipo == 2) {
        $sql = 'SELECT 
          u.cod_usuario,
          u.nome_completo_usuario,
          u.email_usuario,
          u.sexo_usuario,
          u.rg_usuario,
          u.cpf_usuario,
          u.tel_usuario,
          u.nasc_usuario,
          u.senha_usuario,
          u.tipo_usuario,
          u.status_usuario,
          u.uuid,
          r.rua,
          r.numero,
          r.bairro,
          r.cidade,
          r.estado,
          r.cep
        FROM `usuarios` AS `u` INNER JOIN `responsaveis` AS `r` ON u.cod_usuario = r.cod_usuario WHERE u.cod_usuario = ?;';

        $conn->exec($sql);
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $rows = $stmt->rowCount();
        $registro = $stmt->fetch(PDO::FETCH_OBJ);
        
        if ($rows==0) {
          $msg = 'Email ou senha não encontrados!';
          echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '5'));
        } else {
          if ($registro->status_usuario == '0') {
                $msg = '<p>Você precisa confirmar sua conta!</p><p>Veja o email que enviamos para você no registro!</p><a id="btnEnviarNovamente">Não recebi o email</a>';
                echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '6'));
            } else {
              if($stmt->errorCode() != '00000'){
                  $msg = 'Erro desconhecido! Tente novamente!';
                  echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '7'));
              } else {
                  $sql = 'SELECT * FROM `usuarios` INNER JOIN `dependentes` ON `usuarios`.cod_usuario = `dependentes`.cod_usuario WHERE `usuarios`.cod_usuario =      (SELECT cod_dependente FROM `dependencias` WHERE cod_responsavel = ?);';
                  $conn->exec($sql);
                  $stmt = $conn->prepare($sql);
                  $stmt->bindParam(1, $registro->cod_usuario);
                  $stmt->execute();
                  $rowsDep = $stmt->rowCount();
                  $dependente = $stmt->fetch(PDO::FETCH_OBJ);
                  
                  if ($rowsDep > 0) {
                    $sql = 'SELECT nome_enfermidade FROM `enfermidades` WHERE cod_usuario = ?;';
                    $conn->exec($sql);
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(1, $dependente->cod_usuario);
                    $stmt->execute();
                    $rows = $stmt->rowCount();
                    $enfermidades = [];
                    while ($enfermidade = $stmt->fetch(PDO::FETCH_ASSOC)) {
                      $enfermidades[] = $enfermidade['nome_enfermidade'];
                    }
  
                    $sql = 'SELECT nome_medicacao FROM `medicacoes` WHERE cod_usuario = ?;';
                    $conn->exec($sql);
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(1, $dependente->cod_usuario);
                    $stmt->execute();
                    $rows = $stmt->rowCount();
                    $medicacoes = [];
                    while ($medicacao = $stmt->fetch(PDO::FETCH_ASSOC)) {
                      $medicacoes[] = $medicacao['nome_medicacao'];
                    }

                    $img = 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($user))) . '?s=256&d=mm&r=g';
                    echo json_encode(
                      array('status' => 'success',
                            'cod' => $registro->cod_usuario,
                            'cpf' => $registro->cpf_usuario,
                            'rg' => $registro->rg_usuario,
                            'nasc' => $registro->nasc_usuario,
                            'sexo' => $registro->sexo_usuario,
                            'tel' => $registro->tel_usuario,
                            'nome' => $registro->nome_completo_usuario,
                            'email' => $registro->email_usuario,
                            'tipo' => $registro->tipo_usuario,
                            'avatar' => $img,
                            'stats' => $registro->status_usuario,
                            'uuid' => $registro->uuid,
                            'rua' => $registro->rua,
                            'numero' => $registro->numero,
                            'bairro' => $registro->bairro,
                            'cidade' => $registro->cidade,
                            'estado' => $registro->estado,
                            'cep' => $registro->cep,
                            'dep' => array(
                                        'codDep' => $dependente->cod_usuario,
                                        'nomeDep' => $dependente->nome_completo_usuario,
                                        'emailDep' => $dependente->email_usuario,
                                        'sexoDep' => $dependente->sexo_usuario,
                                        'rgDep' => $dependente->rg_usuario,
                                        'cpfDep' => $dependente->cpf_usuario,
                                        'telDep' => $dependente->tel_usuario,
                                        'nascDep' => $dependente->nasc_usuario,
                                        'planoSaudeDep' => $dependente->plano_de_saude_dependente,
                                        'numeroPlanoSaudeDep' => $dependente->numero_plano_saude_dependente,
                                        'numeroSusDep' => $dependente->numero_sus_dependente,
                                        'tipoSanguineoDep' => $dependente->tipo_sanguineo_dependente,
                                        'enfermidadesDep' => $enfermidades,
                                        'medicacoesDep' => $medicacoes
                                    )
                          ));
                  } else {
                    $img = 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($user))) . '?s=256&d=mm&r=g';
                    echo json_encode(
                      array('status' => 'success',
                            'cod' => $registro->cod_usuario,
                            'cpf' => $registro->cpf_usuario,
                            'rg' => $registro->rg_usuario,
                            'nasc' => $registro->nasc_usuario,
                            'sexo' => $registro->sexo_usuario,
                            'tel' => $registro->tel_usuario,
                            'nome' => $registro->nome_completo_usuario,
                            'email' => $registro->email_usuario,
                            'tipo' => $registro->tipo_usuario,
                            'avatar' => $img,
                            'stats' => $registro->status_usuario,
                            'uuid' => $registro->uuid,
                            'rua' => $registro->rua,
                            'numero' => $registro->numero,
                            'bairro' => $registro->bairro,
                            'cidade' => $registro->cidade,
                            'estado' => $registro->estado,
                            'cep' => $registro->cep,
                            'dep' => array()
                          ));
                  }
            }
          }
        }
      }
  }
}

if (!isset($_POST['inputSenha']) ||
    !isset($_POST['inputUsuario']) ||
    !isset($_POST['hash']) ||
    empty($_POST['inputSenha']) ||
    empty($_POST['inputUsuario']) ||
    empty($_POST['hash'])) {
  Header('Location: http://www.salus.ml');
}

if ($_POST['hash'] != '=U0BMdVMtJC0yTzE3LSEtbUBEM3VzLSUtVGgxM0dvLSYtQjFnMGQzLSotQnIjQnIj') {
  $msg = 'Erro desconhecido! Tente novamente!';
  echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '8'));
}

if (valida($_POST['inputUsuario'], 6, 64, true, 1, 'Email')) {
  $user = $_POST['inputUsuario'];
}

if (valida($_POST['inputSenha'], 6, 64, true, 1, 'Senha')) {
  $senha = $_POST['inputSenha'];
}

if ($erro != true) {
  login($user, md5($senha.$hash));
} else {
  $msg = 'Erro desconhecido!';
  echo json_encode(array('status' => 'error', 'msg' => $msg, 'n' => '9'));
}
?>
