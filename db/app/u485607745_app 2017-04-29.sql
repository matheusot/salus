-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: u485607745_app
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dependencias`
--

DROP TABLE IF EXISTS `dependencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencias` (
  `cod_dependente` int(11) NOT NULL,
  `cod_responsavel` int(11) NOT NULL,
  PRIMARY KEY (`cod_dependente`),
  UNIQUE KEY `cod_dependente` (`cod_dependente`),
  UNIQUE KEY `cod_responsavel` (`cod_responsavel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencias`
--

/*!40000 ALTER TABLE `dependencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `dependencias` ENABLE KEYS */;

--
-- Table structure for table `dependentes`
--

DROP TABLE IF EXISTS `dependentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependentes` (
  `cod_usuario` int(11) NOT NULL,
  `plano_de_saude_dependente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numero_plano_saude_dependente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `numero_sus_dependente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_sanguineo_dependente` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependentes`
--

/*!40000 ALTER TABLE `dependentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dependentes` ENABLE KEYS */;

--
-- Table structure for table `emergencias`
--

DROP TABLE IF EXISTS `emergencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emergencias` (
  `cod_responsavel` int(11) NOT NULL,
  `cod_dependente` int(11) NOT NULL,
  `data_hora_emergencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `leitura_emergencia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_dependente`),
  UNIQUE KEY `cod_responsavel` (`cod_responsavel`),
  UNIQUE KEY `cod_dependente` (`cod_dependente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emergencias`
--

/*!40000 ALTER TABLE `emergencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `emergencias` ENABLE KEYS */;

--
-- Table structure for table `enfermidades`
--

DROP TABLE IF EXISTS `enfermidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enfermidades` (
  `cod_usuario` int(11) NOT NULL,
  `nome_enfermidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sintomas_enfermidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `nome_enfermidade` (`nome_enfermidade`),
  UNIQUE KEY `cod_usuario` (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enfermidades`
--

/*!40000 ALTER TABLE `enfermidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `enfermidades` ENABLE KEYS */;

--
-- Table structure for table `medicacoes`
--

DROP TABLE IF EXISTS `medicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicacoes` (
  `cod_usuario` int(11) NOT NULL,
  `nome_medicacao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `periodo_medicacao` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dosagem_medicacao` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `nome_medicacao` (`nome_medicacao`),
  UNIQUE KEY `cod_usuario` (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicacoes`
--

/*!40000 ALTER TABLE `medicacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicacoes` ENABLE KEYS */;

--
-- Table structure for table `medicos`
--

DROP TABLE IF EXISTS `medicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicos` (
  `hospital_medico` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `area_medico` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `numero_registro_medico` int(15) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `numero_registro_medico` (`numero_registro_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicos`
--

/*!40000 ALTER TABLE `medicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicos` ENABLE KEYS */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `cod_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome_completo_usuario` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email_usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sexo_usuario` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `rg_usuario` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_usuario` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tel_usuario` int(15) NOT NULL,
  `nasc_usuario` date NOT NULL,
  `senha_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(1) NOT NULL DEFAULT '0' COMMENT '0 = sem tipo | 1 = dependente | 2 = responsável | 3 = médico',
  `status_usuario` int(1) NOT NULL DEFAULT '0' COMMENT '0 = a confirmar | 1 = cadastrado | 2 = administrador',
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `email_usuario` (`email_usuario`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (29,'Matheus Otávio Rodrigues','maot.rodrigues@gmail.com','M','','',0,'0000-00-00','be3c50e6c1fb3a3c6973ccd30afeff74',0,1,'XMlDBUd8t8qE3LjXBp8GoXeC7Xnh8ln9'),(30,'Breno de Oliveira Veroneze','breno.veroneze@outlook.com','M','','',0,'0000-00-00','8b7b1d111a58fe8cc6092163d038de03',1,1,'6Pt6uPiBpwqUWShsQL2fJ2pRZfkc2kuk'),(31,'Matheus Rodrigues','math.ota.rod@gmail.com','M','','',0,'0000-00-00','be3c50e6c1fb3a3c6973ccd30afeff74',0,1,'gJvUT7WI19C6sxjotmPlYvQ5DJibUnbu');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

--
-- Dumping routines for database 'u485607745_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-29 19:59:14
