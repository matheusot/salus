-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: u485607745_app
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app`
--

DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `versao` varchar(4) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app`
--

/*!40000 ALTER TABLE `app` DISABLE KEYS */;
INSERT INTO `app` VALUES ('1.8');
/*!40000 ALTER TABLE `app` ENABLE KEYS */;

--
-- Table structure for table `dependencias`
--

DROP TABLE IF EXISTS `dependencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencias` (
  `cod_dependente` int(11) NOT NULL,
  `cod_responsavel` int(11) NOT NULL,
  PRIMARY KEY (`cod_dependente`),
  UNIQUE KEY `cod_dependente` (`cod_dependente`),
  UNIQUE KEY `cod_responsavel` (`cod_responsavel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencias`
--

/*!40000 ALTER TABLE `dependencias` DISABLE KEYS */;
INSERT INTO `dependencias` VALUES (61,62);
/*!40000 ALTER TABLE `dependencias` ENABLE KEYS */;

--
-- Table structure for table `dependentes`
--

DROP TABLE IF EXISTS `dependentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependentes` (
  `cod_usuario` int(11) NOT NULL,
  `plano_de_saude_dependente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `numero_plano_saude_dependente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `numero_sus_dependente` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_sanguineo_dependente` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependentes`
--

/*!40000 ALTER TABLE `dependentes` DISABLE KEYS */;
INSERT INTO `dependentes` VALUES (61,'unihosp','1232131','231321321321312','ab+'),(62,'','','',''),(63,'unimed','1123123','123231213123122','o+');
/*!40000 ALTER TABLE `dependentes` ENABLE KEYS */;

--
-- Table structure for table `emergencias`
--

DROP TABLE IF EXISTS `emergencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emergencias` (
  `cod_emergencia` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_leitura` int(11) unsigned NOT NULL,
  `cod_responsavel` int(11) unsigned NOT NULL,
  `data_hora_emergencia` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cod_emergencia`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emergencias`
--

/*!40000 ALTER TABLE `emergencias` DISABLE KEYS */;
INSERT INTO `emergencias` VALUES (15,2,62,'2017-09-17 20:28:13'),(16,3,62,'2017-09-17 21:00:02'),(17,3,62,'2017-09-17 21:00:17');
/*!40000 ALTER TABLE `emergencias` ENABLE KEYS */;

--
-- Table structure for table `enfermidades`
--

DROP TABLE IF EXISTS `enfermidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enfermidades` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) NOT NULL,
  `nome_enfermidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enfermidades`
--

/*!40000 ALTER TABLE `enfermidades` DISABLE KEYS */;
INSERT INTO `enfermidades` VALUES (111,61,'Alzheimer'),(112,61,'Diarreia');
/*!40000 ALTER TABLE `enfermidades` ENABLE KEYS */;

--
-- Table structure for table `leituras_dependentes`
--

DROP TABLE IF EXISTS `leituras_dependentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leituras_dependentes` (
  `cod_leitura` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cod_dependente` int(11) unsigned NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bpm` int(11) unsigned NOT NULL,
  `temp` int(11) NOT NULL,
  `oxigenacao` int(11) unsigned NOT NULL,
  PRIMARY KEY (`cod_leitura`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leituras_dependentes`
--

/*!40000 ALTER TABLE `leituras_dependentes` DISABLE KEYS */;
INSERT INTO `leituras_dependentes` VALUES (2,61,'2017-09-17 20:14:00',23,232,32),(3,63,'2017-09-17 20:59:12',127,39,95);
/*!40000 ALTER TABLE `leituras_dependentes` ENABLE KEYS */;

--
-- Table structure for table `medicacoes`
--

DROP TABLE IF EXISTS `medicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicacoes` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `cod_usuario` int(11) NOT NULL,
  `nome_medicacao` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicacoes`
--

/*!40000 ALTER TABLE `medicacoes` DISABLE KEYS */;
INSERT INTO `medicacoes` VALUES (177,61,'Ué'),(178,61,'Tarja preta'),(179,61,'Remédio para dormir'),(180,61,'Gardenal');
/*!40000 ALTER TABLE `medicacoes` ENABLE KEYS */;

--
-- Table structure for table `medicos`
--

DROP TABLE IF EXISTS `medicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicos` (
  `hospital_medico` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `area_medico` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `numero_registro_medico` int(15) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `numero_registro_medico` (`numero_registro_medico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicos`
--

/*!40000 ALTER TABLE `medicos` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicos` ENABLE KEYS */;

--
-- Table structure for table `responsaveis`
--

DROP TABLE IF EXISTS `responsaveis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsaveis` (
  `cod_usuario` int(11) NOT NULL,
  `rua` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `bairro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsaveis`
--

/*!40000 ALTER TABLE `responsaveis` DISABLE KEYS */;
INSERT INTO `responsaveis` VALUES (62,'Timbiras',230,'JD São Francisco','Santa Bárbara D\'oeste','SP','13456086');
/*!40000 ALTER TABLE `responsaveis` ENABLE KEYS */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `cod_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome_completo_usuario` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email_usuario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sexo_usuario` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `rg_usuario` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_usuario` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `tel_usuario` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `nasc_usuario` date NOT NULL,
  `senha_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usuario` int(1) NOT NULL DEFAULT '0' COMMENT '0 = sem tipo | 1 = dependente | 2 = responsável | 3 = médico',
  `status_usuario` int(1) NOT NULL DEFAULT '0' COMMENT '0 = a confirmar | 1 = cadastrado | 2 = administrador',
  `uuid` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_usuario`),
  UNIQUE KEY `email_usuario` (`email_usuario`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (30,'Breno de Oliveira Veroneze','breno.veroneze@outlook.com','M','','','0','0000-00-00','8b7b1d111a58fe8cc6092163d038de03',1,1,'6Pt6uPiBpwqUWShsQL2fJ2pRZfkc2kuk'),(34,'Brenooo','oliveirabreno.veroneze@gmail.com','M','','','0','0000-00-00','dc37231a0fe530a113f71fcb66228465',0,1,'TPCXXschYAlFqOJI6UNboxZt9Syf8FNP'),(61,'Matheus Doente','maot.rodrigues@gmail.com','M','548278787','48016610846','55019996876858','1999-11-04','f8c8351e33498ecbaa77f2dc26322d2e',1,1,'m5oB6tTUNiXGIj0nGkzyhtOidvY5FmtK'),(62,'Matheus Responsável','math.ota.rod@gmail.com','M','548287849','48016610846','5501936262404','1999-11-04','f8c8351e33498ecbaa77f2dc26322d2e',2,1,'7CLOLU0IZhYmvVxXeXINtEk4ynabm0xV'),(63,'Diego Augusto Rodrigues','di_rodrigues@aedu.com','M','469253629','11111111111','5501936293629','1989-12-11','d65ad8e224e77e5490c4a761b482f22e',1,1,'CWDOMkhgEzEdd1xHJKOuDcjhhixyp1td');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

--
-- Dumping routines for database 'u485607745_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-18  2:47:52
