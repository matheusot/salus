-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: 07-Abr-2017 �s 19:14
-- Vers�o do servidor: 8.0.0-dmr
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `salus`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamado`
--

CREATE TABLE `chamado` (
  `chmd_cod` int(11) NOT NULL COMMENT 'C�digo do chamado',
  `chmd_data_abert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de abertura do chamado',
  `chmd_data_fecha` timestamp NULL DEFAULT NULL COMMENT 'Data de fechamento do chamado',
  `chmd_titulo` varchar(80) COLLATE utf8_bin NOT NULL COMMENT 'T�tulo do chamado',
  `chmd_descricao` text COLLATE utf8_bin NOT NULL COMMENT 'Descri��o do chamado',
  `chmd_prioridade` tinyint(1) NOT NULL COMMENT '0 = Prioridade baixa | 1 = Prioridade m�dia | 2 = Prioridade alta',
  `chmd_status` tinyint(1) NOT NULL COMMENT '0 = N�o vinculado | 1 = Em resolu��o | 2 = Finalizado | 3 = Cancelado'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `chamado`
--

INSERT INTO `chamado` (`chmd_cod`, `chmd_data_abert`, `chmd_data_fecha`, `chmd_titulo`, `chmd_descricao`, `chmd_prioridade`, `chmd_status`) VALUES
(11, '2017-01-04 16:57:14', '2017-03-08 03:00:00', 'Teste', 'Teste', 0, 3),
(8, '2017-02-15 23:00:00', NULL, 'Teste', 'Teste', 1, 0),
(9, '2017-02-26 00:00:00', NULL, 'Teste', 'Teste', 1, 1),
(10, '2017-01-24 23:00:00', NULL, 'Teste', 'Teste', 1, 0),
(12, '2017-02-16 16:57:14', NULL, 'Teste', 'Teste', 0, 0),
(13, '2017-01-22 16:57:14', NULL, 'Teste', 'Teste', 0, 0),
(14, '2017-01-03 17:00:20', NULL, 'Aloo', 'Teste', 2, 2),
(15, '2017-02-16 17:00:20', NULL, 'Teste', 'Teste', 2, 1),
(16, '2017-01-16 17:00:20', NULL, 'Teste', 'Teste', 2, 1),
(17, '2017-02-27 18:01:50', NULL, 'Teste', 'Teste', 2, 2),
(18, '2017-02-27 18:01:50', NULL, 'Teste', 'Teste', 2, 2),
(19, '2017-02-27 18:01:50', NULL, 'Teste', 'Teste', 2, 3),
(20, '2017-04-04 11:13:05', NULL, 'teste', 'teste', 1, 1),
(21, '2017-04-04 11:13:17', NULL, 'teste', 'teste', 1, 1),
(22, '2017-04-04 11:13:38', NULL, 'teste', 'teste', 1, 1),
(23, '2017-04-04 11:25:46', NULL, 'Teste1', 'Teste1', 1, 1),
(24, '2017-04-04 09:34:20', NULL, 'Teste1', 'Teste1', 1, 1),
(25, '2017-04-04 09:35:17', NULL, 'Teste1', 'Teste1', 1, 1),
(35, '2017-04-04 17:56:46', NULL, 'Thiago Gay', 'Gayzao', 2, 2),
(27, '2017-04-04 09:42:39', NULL, '312321321', '323312321', 2, 1),
(28, '2017-04-04 09:43:07', NULL, ' 43342432', '4324324', 1, 1),
(29, '2017-04-04 09:44:16', NULL, ' 43342432', '4324324', 1, 1),
(30, '2017-04-04 09:44:30', NULL, ' 43342432', '4324324', 1, 1),
(31, '2017-04-04 09:44:34', NULL, ' 43342432', '4324324', 1, 1),
(32, '2017-04-04 09:47:36', NULL, ' 43342432', '4324324', 1, 1),
(33, '2017-04-04 09:48:01', NULL, 'Teste cin Tidis', 'Teste com Todos', 2, 1),
(34, '2017-04-04 09:49:07', NULL, 'Teste com todos agr vai', 'teste com todos agr vai', 2, 1),
(36, '2017-04-06 01:29:37', NULL, 'Teste Classes', 'Teste Classes', 0, 0),
(37, '2017-04-06 14:34:02', NULL, 'Teste Definitivo', 'Teste Definitivo nessa porra', 2, 2),
(38, '2017-04-07 06:11:12', NULL, 'Teste', 'Teste', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `diario`
--

CREATE TABLE `diario` (
  `diar_usr_id` tinyint(1) NOT NULL COMMENT 'ID do usu�rio (Grupo = 0 | Breno = 1 | Gabriel = 2 | Thiago = 3 | Matheus = 4)',
  `diar_data` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Data de inser��o da a��o',
  `diar_desc` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Descri��o da a��o',
  `diar_data_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp da a��o',
  `diar_usr_cod_ins` tinyint(1) NOT NULL COMMENT 'C�digo do user que inseriu o registro'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `diario`
--

INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES
(4, '21/03/2017', 'Finaliza��o do Di�rio autom�tico e ajustes finais no Cronograma', '2017-03-21 05:40:03', 0),
(4, '21/03/2017', 'Finaliza��o do aplicativo de di�rio', '2017-03-21 04:25:15', 0),
(4, '21/03/2017', 'Melhorias no gerador de PDF de relat�rio', '2017-03-21 04:25:44', 0),
(4, '21/03/2017', 'Organiza��o dos diret�rios, altera��o das imagens do AutoExport e inser��o do novo logo', '2017-03-21 05:38:58', 0),
(4, '21/03/2017', 'Altera��o de �cones, cria��o de tela que mostra um dbgrid para consulta e ajustes gerais', '2017-03-21 05:40:47', 0),
(3, '22/03/2017', 'Conversado com o orientador Gabriel Maximo sobre o modulo Wi-Fi', '2017-03-22 23:32:24', 0),
(4, '22/03/2017', 'Finaliza��o do sistema de di�rio autom�tico', '2017-03-22 23:33:42', 0),
(2, '22/03/2017', 'Finaliza��o do sistema de di�rio autom�tico', '2017-03-22 23:34:10', 0),
(4, '09/03/2017', '	Corre��o de cabe�alho e rodap�, adi��o de marca d\'�gua, revis�es de formata��o	', '2017-03-08 23:00:01', 0),
(0, '07/03/2017', '	Revis�o da documenta��o e primeira entrega (Aula)	', '2017-03-06 23:00:01', 0),
(0, '05/03/2017', '	Altera��o da documenta��o, inser��o de mais se��es e estrutura visual (Whatsapp)	', '2017-03-04 23:00:01', 0),
(0, '04/03/2017', '	Defini��o de paleta de cores e treinamento de uso do GitKraken + GitLab (Skype)	', '2017-03-03 23:00:01', 0),
(0, '02/03/2017', '	Defini��o de nome, finaliza��o do or�amento e esbo�os de logotipo. (Whatsapp)	', '2017-03-01 23:00:01', 0),
(0, '28/02/2017', '	Organiza��o das ideias, in�cio do or�amento e documenta��o (Skype)	', '2017-02-27 23:00:01', 0),
(0, '14/02/2017', '	Desenvolvimento da ideia (Aula)	', '2017-02-13 23:00:01', 0),
(0, '19/02/2017', '	In�cio da documenta��o, ideias para nome e logotipo (Skype)	', '2017-02-18 23:00:01', 0),
(4, '14/03/2017', '	In�cio da documenta��o detalhada e discuss�es sobre imagens do projeto (Aula)	', '2017-03-13 23:00:01', 0),
(3, '14/03/2017', '	In�cio da documenta��o detalhada e discuss�es sobre imagens do projeto (Aula)	', '2017-03-13 23:00:02', 0),
(1, '14/03/2017', '	In�cio da prot�tipo do aplicativo e discuss�es sobre design das telas (Aula)	', '2017-03-13 23:00:03', 0),
(2, '14/03/2017', '	In�cio da prot�tipo do aplicativo e discuss�es sobre design das telas (Aula)	', '2017-03-13 23:00:04', 0),
(4, '16/03/2017', '	Desenvolvimento de aplicativo para c�pia de arquivos e cria��o autom�tica do Di�rio	', '2017-03-15 23:00:01', 0),
(4, '20/03/2017', '	Vetoriza��o e ajustes finais do logotipo	', '2017-03-19 23:00:01', 0),
(4, '20/03/2017', '	Desenvolvimento de software para gera��o de di�rio autom�tico 	', '2017-03-19 23:00:02', 0),
(1, '21/03/2017', '	Desenvolvimento do prot�tipo do aplicativo (Aula)	', '2017-03-20 23:00:01', 0),
(2, '21/03/2017', '	Desenvolvimento do prot�tipo do aplicativo (Aula)	', '2017-03-20 23:00:02', 0),
(4, '25/03/2017', 'Cria��o de telas do aplicativo e finaliza��o da descri��o detalhada da ideia', '2017-03-25 23:35:01', 2),
(1, '25/03/2017', 'Elabora��o das telas (prot�tipo) do aplicativo', '2017-03-25 23:41:29', 5),
(2, '25/03/2017', 'Elabora��o das telas (prot�tipo) do aplicativo', '2017-03-25 23:41:48', 3),
(3, '25/03/2017', 'Elabora��o de tela (prot�tipo) do aplicativo', '2017-03-25 23:43:35', 4),
(3, '25/03/2017', 'Elabora��o de telas (prot�tipos) do aplicativo', '2017-03-25 23:44:02', 4),
(4, '27/03/2017', 'Revis�o na documenta��o e altera��o dos logotipos para adequa��o do novo estilo', '2017-03-27 15:29:58', 2),
(1, '28/03/2017', 'Finaliza��o Home', '2017-03-28 23:37:26', 3),
(2, '28/03/2017', 'Finaliza��o Home', '2017-03-28 23:37:35', 3),
(3, '28/03/2017', 'Finaliza��o das imagens da documenta��o e revis�o final.', '2017-03-29 14:17:30', 2),
(3, '28/03/2017', 'Finaliza��o das imagens da documenta��o e revis�o final.', '2017-03-29 18:00:03', 2),
(4, '28/03/2017', 'Finaliza��o das imagens da documenta��o e revis�o final.', '2017-03-28 18:00:00', 2),
(0, '04/04/2017', 'Cria��o das telas do aplicativo', '2017-04-04 21:54:14', 2),
(4, '03/04/2017', 'Cria��o de �rea de login no sistema de chamados', '2017-03-04 18:40:51', 2),
(4, '04/04/2017', 'Novo estilo para o sistema de chamados (Superhero Bootswatch.com)', '2017-04-04 02:36:17', 2),
(4, '05/04/2017', 'Ajustes nas fun��es de v�nculos de chamados', '2017-04-05 15:10:52', 2),
(4, '06/04/2017', 'Adi��o da inser��o de evento pelo site.', '2017-04-06 14:09:42', 2),
(4, '06/04/2017', 'Finaliza��o do sistema de cadastro e v�nculos de chamados. Agora pode-se iniciar a utiliza��o deste sistema.', '2017-04-06 15:38:47', 2),
(4, '07/04/2017', 'Corre��o de incompatibilidade do Login do sistema de Chamados com a hospedagem da 000webhost.', '2017-04-06 23:35:46', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `user_cod` int(11) NOT NULL COMMENT 'C�digo do usu�rio',
  `user_login` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome de login (username) do usu�rio',
  `user_senha` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Senha do usu�rio criptografada *hash md5*',
  `user_nome` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome pessoal do usu�rio'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`user_cod`, `user_login`, `user_senha`, `user_nome`) VALUES
(2, 'matheusot', '409c2baac455afee82a6823769e965c9', 'Matheus'),
(3, 'gabriel11447', 'fbb4320ba5afdf700f42641ff93690e4', 'Gabriel'),
(4, 'thiago0003', 'c04716c3c68d22851304dc6e02f1c4f6', 'Thiago'),
(5, 'brenovero', '090245f477d118fde3e5fa4f7c150f1f', 'Breno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_chamado`
--

CREATE TABLE `usuario_chamado` (
  `usrchmd_user_cod` int(11) NOT NULL COMMENT 'C�digo do usu�rio vinculado ao chamado',
  `usrchmd_chmd_cod` int(11) NOT NULL COMMENT 'C�digo do chamado vinculado ao usu�rio',
  `usrchmd_chmd_vincini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data do in�cio do v�nculo',
  `usrchmd_chmd_vincfim` timestamp NULL DEFAULT NULL COMMENT 'Data do fim do v�nculo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario_chamado`
--

INSERT INTO `usuario_chamado` (`usrchmd_user_cod`, `usrchmd_chmd_cod`, `usrchmd_chmd_vincini`, `usrchmd_chmd_vincfim`) VALUES
(2, 14, '2017-02-26 03:00:00', '2017-03-05 03:00:00'),
(3, 14, '2017-03-12 03:00:00', '2017-03-19 03:00:00'),
(5, 34, '2017-04-04 09:49:07', NULL),
(3, 34, '2017-04-04 09:49:07', NULL),
(2, 34, '2017-04-04 09:49:07', NULL),
(4, 34, '2017-04-04 09:49:07', NULL),
(4, 35, '2017-04-04 17:56:46', NULL),
(4, 14, '2017-04-06 14:25:28', '2017-04-06 15:14:22'),
(5, 14, '2017-04-06 14:32:47', '2017-04-06 15:37:38'),
(4, 14, '2017-04-06 14:33:20', '2017-04-06 15:37:51'),
(4, 14, '2017-04-06 14:33:38', '2017-04-06 15:38:07'),
(2, 37, '2017-04-06 14:34:12', '2017-04-06 14:48:39'),
(5, 37, '2017-04-06 14:49:00', '2017-04-06 14:49:10'),
(2, 16, '2017-04-06 15:41:00', NULL),
(3, 38, '2017-04-07 06:11:12', NULL),
(3, 9, '2017-04-07 06:11:48', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chamado`
--
ALTER TABLE `chamado`
  ADD PRIMARY KEY (`chmd_cod`);

--
-- Indexes for table `diario`
--
ALTER TABLE `diario`
  ADD PRIMARY KEY (`diar_data_insert`),
  ADD UNIQUE KEY `diar_data_insert` (`diar_data_insert`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`user_cod`);

--
-- Indexes for table `usuario_chamado`
--
ALTER TABLE `usuario_chamado`
  ADD UNIQUE KEY `usrchmd_user_cod` (`usrchmd_user_cod`,`usrchmd_chmd_cod`,`usrchmd_chmd_vincini`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chamado`
--
ALTER TABLE `chamado`
  MODIFY `chmd_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C�digo do chamado', AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `user_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'C�digo do usu�rio', AUTO_INCREMENT=6;