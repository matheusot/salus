-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Mar-2017 às 17:56
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chamado`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamado`
--

CREATE TABLE `chamado` (
  `chmd_cod` int(11) NOT NULL COMMENT 'Código do chamado',
  `chmd_data_abert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de abertura do chamado',
  `chmd_data_fecha` timestamp NULL DEFAULT NULL COMMENT 'Data de fechamento do chamado',
  `chmd_titulo` varchar(80) COLLATE utf8_bin NOT NULL COMMENT 'Título do chamado',
  `chmd_descricao` text COLLATE utf8_bin NOT NULL COMMENT 'Descrição do chamado',
  `chmd_prioridade` tinyint(1) NOT NULL COMMENT '0 = Prioridade baixa | 1 = Prioridade média | 2 = Prioridade alta',
  `chmd_status` tinyint(1) NOT NULL COMMENT '0 = Não vinculado | 1 = Finalizado | 2 = Em resolução | 3 = Cancelado'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `chamado`
--

INSERT INTO `chamado` (`chmd_cod`, `chmd_data_abert`, `chmd_data_fecha`, `chmd_titulo`, `chmd_descricao`, `chmd_prioridade`, `chmd_status`) VALUES
(11, '2017-01-04 19:57:14', '2017-03-08 03:00:00', 'Teste', 'Teste', 0, 0),
(8, '2017-02-16 02:00:00', NULL, 'Teste', 'Teste', 1, 0),
(9, '2017-02-26 03:00:00', NULL, 'Teste', 'Teste', 1, 0),
(10, '2017-01-25 02:00:00', NULL, 'Teste', 'Teste', 1, 0),
(12, '2017-02-16 19:57:14', NULL, 'Teste', 'Teste', 0, 0),
(13, '2017-01-22 19:57:14', NULL, 'Teste', 'Teste', 0, 0),
(14, '2017-01-03 20:00:20', '2017-05-19 03:00:00', 'Teste', 'Teste', 2, 0),
(15, '2017-02-16 20:00:20', NULL, 'Teste', 'Teste', 2, 0),
(16, '2017-01-16 20:00:20', NULL, 'Teste', 'Teste', 2, 0),
(17, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 1),
(18, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 2),
(19, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `diario`
--

CREATE TABLE `diario` (
  `usr_id` int(11) NOT NULL,
  `dia_data` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `diario`
--

INSERT INTO `diario` (`usr_id`, `dia_data`) VALUES
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017'),
(1, '14/03/2017');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `user_cod` int(11) NOT NULL COMMENT 'Código do usuário',
  `user_login` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome de login (username) do usuário',
  `user_senha` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Senha do usuário criptografada *hash md5*',
  `user_nome` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome pessoal do usuário'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`user_cod`, `user_login`, `user_senha`, `user_nome`) VALUES
(2, 'matheusot', 'polopolo', 'Matheus'),
(3, 'gabriel11447', 'teste', 'Gabriel'),
(4, 'thiago', 'teste', 'Thiago');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_chamado`
--

CREATE TABLE `usuario_chamado` (
  `usrchmd_user_cod` int(11) NOT NULL COMMENT 'Código do usuário vinculado ao chamado',
  `usrchmd_chmd_cod` int(11) NOT NULL COMMENT 'Código do chamado vinculado ao usuário',
  `usrchmd_chmd_vincini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data do início do vínculo',
  `usrchmd_chmd_vincfim` timestamp NULL DEFAULT NULL COMMENT 'Data do fim do vínculo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario_chamado`
--

INSERT INTO `usuario_chamado` (`usrchmd_user_cod`, `usrchmd_chmd_cod`, `usrchmd_chmd_vincini`, `usrchmd_chmd_vincfim`) VALUES
(2, 14, '2017-02-26 03:00:00', '2017-03-05 03:00:00'),
(3, 14, '2017-03-12 03:00:00', '2017-03-19 03:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chamado`
--
ALTER TABLE `chamado`
  ADD PRIMARY KEY (`chmd_cod`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`user_cod`);

--
-- Indexes for table `usuario_chamado`
--
ALTER TABLE `usuario_chamado`
  ADD UNIQUE KEY `usrchmd_user_cod` (`usrchmd_user_cod`,`usrchmd_chmd_cod`,`usrchmd_chmd_vincini`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chamado`
--
ALTER TABLE `chamado`
  MODIFY `chmd_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do chamado', AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `user_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do usuário', AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
