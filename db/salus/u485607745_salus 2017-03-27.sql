﻿-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: 27-Mar-2017 às 17:41
-- Versão do servidor: 8.0.0-dmr
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `salus`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamado`
--

CREATE TABLE `chamado` (
  `chmd_cod` int(11) NOT NULL COMMENT 'Código do chamado',
  `chmd_data_abert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de abertura do chamado',
  `chmd_data_fecha` timestamp NULL DEFAULT NULL COMMENT 'Data de fechamento do chamado',
  `chmd_titulo` varchar(80) COLLATE utf8_bin NOT NULL COMMENT 'Título do chamado',
  `chmd_descricao` text COLLATE utf8_bin NOT NULL COMMENT 'Descrição do chamado',
  `chmd_prioridade` tinyint(1) NOT NULL COMMENT '0 = Prioridade baixa | 1 = Prioridade média | 2 = Prioridade alta',
  `chmd_status` tinyint(1) NOT NULL COMMENT '0 = Não vinculado | 1 = Finalizado | 2 = Em resolução | 3 = Cancelado'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `chamado`
--

INSERT INTO `chamado` (`chmd_cod`, `chmd_data_abert`, `chmd_data_fecha`, `chmd_titulo`, `chmd_descricao`, `chmd_prioridade`, `chmd_status`) VALUES
(11, '2017-01-04 19:57:14', '2017-03-08 03:00:00', 'Teste', 'Teste', 0, 0),
(8, '2017-02-16 02:00:00', NULL, 'Teste', 'Teste', 1, 0),
(9, '2017-02-26 03:00:00', NULL, 'Teste', 'Teste', 1, 0),
(10, '2017-01-25 02:00:00', NULL, 'Teste', 'Teste', 1, 0),
(12, '2017-02-16 19:57:14', NULL, 'Teste', 'Teste', 0, 0),
(13, '2017-01-22 19:57:14', NULL, 'Teste', 'Teste', 0, 0),
(14, '2017-01-03 20:00:20', '2017-05-19 03:00:00', 'Teste', 'Teste', 2, 0),
(15, '2017-02-16 20:00:20', NULL, 'Teste', 'Teste', 2, 0),
(16, '2017-01-16 20:00:20', NULL, 'Teste', 'Teste', 2, 0),
(17, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 1),
(18, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 2),
(19, '2017-02-27 21:01:50', NULL, 'Teste', 'Teste', 2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `diario`
--

CREATE TABLE `diario` (
  `diar_usr_id` tinyint(1) NOT NULL COMMENT 'ID do usuário (Grupo = 0 | Breno = 1 | Gabriel = 2 | Thiago = 3 | Matheus = 4)',
  `diar_data` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Data de inserção da ação',
  `diar_desc` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Descrição da ação',
  `diar_data_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp da ação',
  `diar_usr_cod_ins` tinyint(1) NOT NULL COMMENT 'Código do user que inseriu o registro'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `diario`
--

INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES
(4, '21/03/2017', 'Finalização do Diário automático e ajustes finais no Cronograma', '2017-03-21 05:40:03', 0),
(4, '21/03/2017', 'Finalização do aplicativo de diário', '2017-03-21 04:25:15', 0),
(4, '21/03/2017', 'Melhorias no gerador de PDF de relatório', '2017-03-21 04:25:44', 0),
(4, '21/03/2017', 'Organização dos diretórios, alteração das imagens do AutoExport e inserção do novo logo', '2017-03-21 05:38:58', 0),
(4, '21/03/2017', 'Alteração de ícones, criação de tela que mostra um dbgrid para consulta e ajustes gerais', '2017-03-21 05:40:47', 0),
(3, '22/03/2017', 'Conversado com o orientador Gabriel Maximo sobre o modulo Wi-Fi', '2017-03-22 23:32:24', 0),
(4, '22/03/2017', 'Finalização do sistema de diário automático', '2017-03-22 23:33:42', 0),
(2, '22/03/2017', 'Finalização do sistema de diário automático', '2017-03-22 23:34:10', 0),
(4, '09/03/2017', '	Correção de cabeçalho e rodapé, adição de marca d\'água, revisões de formatação	', '2017-03-08 23:00:01', 0),
(0, '07/03/2017', '	Revisão da documentação e primeira entrega (Aula)	', '2017-03-06 23:00:01', 0),
(0, '05/03/2017', '	Alteração da documentação, inserção de mais seções e estrutura visual (Whatsapp)	', '2017-03-04 23:00:01', 0),
(0, '04/03/2017', '	Definição de paleta de cores e treinamento de uso do GitKraken + GitLab (Skype)	', '2017-03-03 23:00:01', 0),
(0, '02/03/2017', '	Definição de nome, finalização do orçamento e esboços de logotipo. (Whatsapp)	', '2017-03-01 23:00:01', 0),
(0, '28/02/2017', '	Organização das ideias, início do orçamento e documentação (Skype)	', '2017-02-27 23:00:01', 0),
(0, '14/02/2017', '	Desenvolvimento da ideia (Aula)	', '2017-02-13 23:00:01', 0),
(0, '19/02/2017', '	Início da documentação, ideias para nome e logotipo (Skype)	', '2017-02-18 23:00:01', 0),
(4, '14/03/2017', '	Início da documentação detalhada e discussões sobre imagens do projeto (Aula)	', '2017-03-13 23:00:01', 0),
(3, '14/03/2017', '	Início da documentação detalhada e discussões sobre imagens do projeto (Aula)	', '2017-03-13 23:00:02', 0),
(1, '14/03/2017', '	Início da protótipo do aplicativo e discussões sobre design das telas (Aula)	', '2017-03-13 23:00:03', 0),
(2, '14/03/2017', '	Início da protótipo do aplicativo e discussões sobre design das telas (Aula)	', '2017-03-13 23:00:04', 0),
(4, '16/03/2017', '	Desenvolvimento de aplicativo para cópia de arquivos e criação automática do Diário	', '2017-03-15 23:00:01', 0),
(4, '20/03/2017', '	Vetorização e ajustes finais do logotipo	', '2017-03-19 23:00:01', 0),
(4, '20/03/2017', '	Desenvolvimento de software para geração de diário automático 	', '2017-03-19 23:00:02', 0),
(1, '21/03/2017', '	Desenvolvimento do protótipo do aplicativo (Aula)	', '2017-03-20 23:00:01', 0),
(2, '21/03/2017', '	Desenvolvimento do protótipo do aplicativo (Aula)	', '2017-03-20 23:00:02', 0),
(4, '25/03/2017', 'Criação de telas do aplicativo e finalização da descrição detalhada da ideia', '2017-03-25 23:35:01', 2),
(1, '25/03/2017', 'Elaboração das telas (protótipo) do aplicativo', '2017-03-25 23:41:29', 5),
(2, '25/03/2017', 'Elaboração das telas (protótipo) do aplicativo', '2017-03-25 23:41:48', 3),
(3, '25/03/2017', 'Elaboração de tela (protótipo) do aplicativo', '2017-03-25 23:43:35', 4),
(3, '25/03/2017', 'Elaboração de telas (protótipos) do aplicativo', '2017-03-25 23:44:02', 4),
(4, '27/03/2017', 'Revisão na documentação e alteração dos logotipos para adequação do novo estilo', '2017-03-27 15:29:58', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `user_cod` int(11) NOT NULL COMMENT 'Código do usuário',
  `user_login` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome de login (username) do usuário',
  `user_senha` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Senha do usuário criptografada *hash md5*',
  `user_nome` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome pessoal do usuário'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`user_cod`, `user_login`, `user_senha`, `user_nome`) VALUES
(2, 'matheusot', '409c2baac455afee82a6823769e965c9', 'Matheus'),
(3, 'gabriel11447', 'fbb4320ba5afdf700f42641ff93690e4', 'Gabriel'),
(4, 'thiago0003', 'c04716c3c68d22851304dc6e02f1c4f6', 'Thiago'),
(5, 'brenovero', '090245f477d118fde3e5fa4f7c150f1f', 'Breno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_chamado`
--

CREATE TABLE `usuario_chamado` (
  `usrchmd_user_cod` int(11) NOT NULL COMMENT 'Código do usuário vinculado ao chamado',
  `usrchmd_chmd_cod` int(11) NOT NULL COMMENT 'Código do chamado vinculado ao usuário',
  `usrchmd_chmd_vincini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data do início do vínculo',
  `usrchmd_chmd_vincfim` timestamp NULL DEFAULT NULL COMMENT 'Data do fim do vínculo'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `usuario_chamado`
--

INSERT INTO `usuario_chamado` (`usrchmd_user_cod`, `usrchmd_chmd_cod`, `usrchmd_chmd_vincini`, `usrchmd_chmd_vincfim`) VALUES
(2, 14, '2017-02-26 03:00:00', '2017-03-05 03:00:00'),
(3, 14, '2017-03-12 03:00:00', '2017-03-19 03:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chamado`
--
ALTER TABLE `chamado`
  ADD PRIMARY KEY (`chmd_cod`);

--
-- Indexes for table `diario`
--
ALTER TABLE `diario`
  ADD PRIMARY KEY (`diar_data_insert`),
  ADD UNIQUE KEY `diar_data_insert` (`diar_data_insert`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`user_cod`);

--
-- Indexes for table `usuario_chamado`
--
ALTER TABLE `usuario_chamado`
  ADD UNIQUE KEY `usrchmd_user_cod` (`usrchmd_user_cod`,`usrchmd_chmd_cod`,`usrchmd_chmd_vincini`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chamado`
--
ALTER TABLE `chamado`
  MODIFY `chmd_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do chamado', AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `user_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do usuário', AUTO_INCREMENT=6;