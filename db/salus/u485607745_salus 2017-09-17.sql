-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: u485607745_salus
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chamado`
--

DROP TABLE IF EXISTS `chamado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamado` (
  `chmd_cod` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Código do chamado',
  `chmd_data_abert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de abertura do chamado',
  `chmd_data_fecha` timestamp NULL DEFAULT NULL COMMENT 'Data de fechamento do chamado',
  `chmd_data_ini` timestamp NULL DEFAULT NULL COMMENT 'Data do primeiro vínculo do chamado',
  `chmd_titulo` varchar(80) COLLATE utf8_bin NOT NULL COMMENT 'Título do chamado',
  `chmd_descricao` text COLLATE utf8_bin NOT NULL COMMENT 'Descrição do chamado',
  `chmd_prioridade` tinyint(1) NOT NULL COMMENT '0 = Prioridade baixa | 1 = Prioridade média | 2 = Prioridade alta',
  `chmd_status` tinyint(1) NOT NULL COMMENT '0 = Não vinculado | 1 = Em resolução | 2 = Finalizado | 3 = Cancelado',
  PRIMARY KEY (`chmd_cod`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamado`
--

/*!40000 ALTER TABLE `chamado` DISABLE KEYS */;
INSERT INTO `chamado` VALUES (47,'2017-02-19 19:03:11','2017-03-09 17:33:17','2017-02-19 19:03:11','Documentação inicial','Desenvolvimento da ideia a nível inicial, definição de cores, logotipos, nome e etc',2,2),(51,'2017-03-16 14:06:12',NULL,'2017-03-16 14:06:10','Sistema de Cronograma e Diário automático','Sistema feito em Web que facilita a geração do Cronograma e do Diário, inclui um sistema em Delphi para facilitar a cópia de arquivos.',0,2),(50,'2017-03-14 13:52:55',NULL,'2017-03-29 12:52:57','Protótipo do aplicativo','Protótipo das telas e design em geral',2,1),(49,'2017-03-14 13:52:55',NULL,'2017-03-14 13:52:57','Documentação detalhada','Escrita da documentação detalhada com base no modelo visto em aula.',2,1),(39,'2017-02-14 18:18:06','2017-02-14 20:31:06','2017-02-14 18:22:06','Desenvolvimento da ideia','Discussões iniciais sobre o projeto (Brainstorm).',2,2),(53,'2017-04-18 19:08:55',NULL,'2017-04-18 19:08:55','Pulseira em Arduino','Desenvolvimento da pulseira com base em Arduíno.',1,1),(54,'2017-06-06 13:06:05',NULL,'2017-06-06 13:06:22','Parte funcional do aplicativo','Desenvolvimento da parte funcional do aplicativo.',2,1);
/*!40000 ALTER TABLE `chamado` ENABLE KEYS */;

--
-- Table structure for table `diario`
--

DROP TABLE IF EXISTS `diario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diario` (
  `diar_usr_id` tinyint(1) NOT NULL COMMENT 'ID do usuário (Grupo = 0 | Breno = 1 | Gabriel = 2 | Thiago = 3 | Matheus = 4)',
  `diar_data` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Data de inserção da ação',
  `diar_desc` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Descrição da ação',
  `diar_data_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp da ação',
  `diar_usr_cod_ins` tinyint(1) NOT NULL COMMENT 'Código do user que inseriu o registro',
  PRIMARY KEY (`diar_data_insert`),
  UNIQUE KEY `diar_data_insert` (`diar_data_insert`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diario`
--

/*!40000 ALTER TABLE `diario` DISABLE KEYS */;
INSERT INTO `diario` VALUES (2,'28/03/2017','Finalização Home','2017-03-28 23:37:35',3),(4,'25/03/2017','Criação de telas do aplicativo e finalização da descrição detalhada da ideia','2017-03-25 23:35:01',2),(1,'25/03/2017','Elaboração das telas (protótipo) do aplicativo','2017-03-25 23:41:29',5),(2,'25/03/2017','Elaboração das telas (protótipo) do aplicativo','2017-03-25 23:41:48',3),(3,'25/03/2017','Elaboração de tela (protótipo) do aplicativo','2017-03-25 23:43:35',4),(3,'25/03/2017','Elaboração de telas (protótipos) do aplicativo','2017-03-25 23:44:02',4),(4,'27/03/2017','Revisão na documentação e alteração dos logotipos para adequação do novo estilo','2017-03-27 15:29:58',2),(1,'28/03/2017','Finalização Home','2017-03-28 23:37:26',3),(4,'20/03/2017','Desenvolvimento de software para geração de diário automático 	','2017-03-19 23:00:02',0),(1,'21/03/2017','Desenvolvimento do protótipo do aplicativo (Aula)','2017-03-20 23:00:01',0),(2,'21/03/2017','Desenvolvimento do protótipo do aplicativo (Aula)','2017-03-20 23:00:02',0),(4,'20/03/2017','Vetorização e ajustes finais do logotipo','2017-03-19 23:00:01',0),(4,'16/03/2017','Desenvolvimento de aplicativo para cópia de arquivos e criação automática do Diário','2017-03-15 23:00:01',0),(2,'14/03/2017','Início da protótipo do aplicativo e discussões sobre design das telas (Aula)','2017-03-13 23:00:04',0),(4,'21/03/2017','Finalização do aplicativo de diário','2017-03-21 04:25:15',0),(4,'21/03/2017','Melhorias no gerador de PDF de relatório','2017-03-21 04:25:44',0),(4,'21/03/2017','Organização dos diretórios, alteração das imagens do AutoExport e inserção do novo logo','2017-03-21 05:38:58',0),(4,'21/03/2017','Alteração de ícones, criação de tela que mostra um dbgrid para consulta e ajustes gerais','2017-03-21 05:40:47',0),(3,'22/03/2017','Conversado com o orientador Gabriel Maximo sobre o modulo Wi-Fi','2017-03-22 23:32:24',0),(4,'22/03/2017','Finalização do sistema de diário automático','2017-03-22 23:33:42',0),(2,'22/03/2017','Finalização do sistema de diário automático','2017-03-22 23:34:10',0),(4,'09/03/2017','Correção de cabeçalho e rodapé, adição de marca d\'água, revisões de formatação','2017-03-08 23:00:01',0),(0,'07/03/2017','Revisão da documentação e primeira entrega (Aula)	','2017-03-06 23:00:01',0),(0,'05/03/2017','Alteração da documentação, inserção de mais seções e estrutura visual (Whatsapp)','2017-03-04 23:00:01',0),(0,'04/03/2017','Definição de paleta de cores e treinamento de uso do GitKraken + GitLab (Skype)','2017-03-03 23:00:01',0),(0,'02/03/2017','Definição de nome, finalização do orçamento e esboços de logotipo. (Whatsapp)','2017-03-01 23:00:01',0),(0,'28/02/2017','Organização das ideias, início do orçamento e documentação (Skype)','2017-02-27 23:00:01',0),(0,'14/02/2017','Desenvolvimento da ideia (Aula)','2017-02-13 23:00:01',0),(0,'19/02/2017','Início da documentação, ideias para nome e logotipo (Skype)','2017-02-18 23:00:01',0),(4,'14/03/2017','Início da documentação detalhada e discussões sobre imagens do projeto (Aula)','2017-03-13 23:00:01',0),(3,'14/03/2017','Início da documentação detalhada e discussões sobre imagens do projeto (Aula)','2017-03-13 23:00:02',0),(1,'14/03/2017','Início da protótipo do aplicativo e discussões sobre design das telas (Aula)','2017-03-13 23:00:03',0),(4,'21/03/2017','Finalização do Diário automático e ajustes finais no Cronograma','2017-03-21 05:40:03',0),(3,'28/03/2017','Finalização das imagens da documentação e revisão final.','2017-03-29 14:17:30',2),(3,'28/03/2017','Finalização das imagens da documentação e revisão final.','2017-03-29 18:00:03',2),(4,'28/03/2017','Finalização das imagens da documentação e revisão final.','2017-03-28 18:00:00',2),(0,'04/04/2017','Criação das telas do aplicativo','2017-04-04 21:54:14',2),(4,'03/04/2017','Criação de área de login no sistema de chamados','2017-03-04 18:40:51',2),(4,'04/04/2017','Novo estilo para o sistema de chamados (Superhero Bootswatch.com)','2017-04-04 02:36:17',2),(4,'05/04/2017','Ajustes nas funções de vínculos de chamados','2017-04-05 15:10:52',2),(4,'06/04/2017','Adição da inserção de evento pelo site.','2017-04-06 14:09:42',2),(4,'06/04/2017','Finalização do sistema de cadastro e vínculos de chamados. Agora pode-se iniciar a utilização deste sistema.','2017-04-06 15:38:47',2),(4,'07/04/2017','Correção de incompatibilidade do Login do sistema de Chamados com a hospedagem da 000webhost.','2017-04-06 23:35:46',2),(4,'08/04/2017','Alteração para domínio .ml do sistema de chamados.','2017-04-08 18:07:33',2),(4,'09/04/2017','Alteração do gráfico para somar a todos os usuários os vínculos de \"Grupo\"','2017-04-09 17:14:05',2),(4,'10/04/2017','Organização dos menus e ajustes na identação do index.php','2017-04-10 16:35:43',2),(1,'11/04/2017','Ajustes na tela inicial do aplicativo','2017-04-11 15:26:15',2),(4,'11/04/2017','Ajustes na tela inicial do aplicativo','2017-04-11 15:26:16',2),(4,'11/04/2017','Edição no software de Cópia de arquivos para remover a funcionalidade de enviar ao diário.','2017-04-11 16:17:05',2),(4,'11/04/2017','Atualização do GIT e utilização da hospedagem salus.000webhostapp.com para armazenar arquivos.','2017-04-11 16:18:12',2),(4,'13/04/2017','Alteração da estrutura das pastas do gerador de Cronograma e Diário, agora os arquivos .PDF estão em uma pasta separada.','2017-04-13 02:52:42',2),(4,'13/04/2017','Tentativa falha de criar uma rotina diária que apague os arquivos .PDF','2017-04-13 02:53:04',2),(4,'13/04/2017','Inserido script .PHP que remove a pasta PDF com todos os seus arquivos, a recria e salva num arquivo .txt de LOG.','2017-04-13 03:06:29',2),(0,'13/04/2017','Continuação do trabalho visual do aplicativo. Ajustes no formulário de cadastro e tela inicial.','2017-04-13 22:43:07',2),(4,'15/04/2017','Adição de select box e implantação de novo estilo minimalista.','2017-04-15 01:24:43',2),(1,'16/04/2017','Adição do menu slider e switch button, além de ajustes na tela Inicial e de Configurações.','2017-04-16 19:10:24',2),(2,'16/04/2017','Adição do menu slider e switch button, além de ajustes na tela Inicial e de Configurações.','2017-04-16 19:10:25',2),(4,'17/04/2017','Ajustes em todas as telas, remoção das travas de responsividade para exibição do botão menu em todas as resoluções, alterações no menu, mudanças no switch button e troca dos ícones utilizados para o Font Awesome.','2017-04-17 17:04:58',2),(3,'17/04/2017','Inicio do desenvolvimento da logica para o microcontrolador ','2017-04-17 18:38:17',4),(4,'18/04/2017','Mudança dos arquivos para hospedagem paga','2017-04-18 18:08:10',2),(3,'18/04/2017','Desenvolvimento da verificação do nível de bateria.','2017-04-18 19:10:32',4),(0,'18/04/2017','Continuação do aplicativo (tela inicial) em aula.','2017-04-18 19:48:30',2),(1,'22/04/2017','Desenvolvimento das telas do aplicativo','2017-04-22 15:33:51',3),(2,'22/04/2017','Desenvolvimento das telas do aplicativo','2017-04-22 15:33:52',3),(4,'21/04/2017','Adição de funcionalidade para reabrir chamados e ajustes adicionais no sistema de Chamados e Diário.','2017-04-21 23:22:00',2),(3,'20/04/2017','Terminado os códigos de verificação de bateria, conexão bluetooth, e botão on/off, e armazenamento na eeprom do arduino.','2017-04-20 01:30:21',4),(4,'26/04/2017','Uso de form groups, adição de ícones, correção dos alertas, redução da quantidade de campos, correção dos valores para o campo sexo e mudança na organização dos campos. Alteração da cor de fundo do Body.','2017-04-26 19:44:51',2),(4,'27/04/2017','Correção de bugs do aplicativo.','2017-04-27 23:34:39',2),(3,'24/04/2017','Desenvolvimento físico do botão de liga e desliga.','2017-04-24 08:06:35',4),(3,'25/04/2017','Montagem do botão liga e desliga.','2017-04-25 18:55:47',4),(3,'26/04/2017','Inicio da junção dos códigos do arduino.','2017-04-26 08:42:11',4),(3,'27/04/2017','Ajustando bugs do bluetooth na transmissão de dados do arduino para o celular.','2017-04-27 14:54:36',4),(1,'02/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-02 19:21:58',2),(2,'02/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-02 19:21:59',2),(4,'02/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-02 19:22:00',2),(3,'02/05/2017','Organização dos arquivos relacionados a pulseira.','2017-05-02 19:22:40',2),(3,'02/05/2017','Implementação do sensor de temperatura ao projeto. E consertado parcialmente os bugs do Bluetooth.','2017-05-02 10:43:11',4),(1,'07/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-07 01:07:08',3),(2,'07/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-07 01:07:09',3),(0,'09/05/2017','Continuação do desenvolvimento do aplicativo','2017-05-09 09:36:16',2),(3,'09/05/2017','Ajustes na pulseira, troca para o módulo Bluetooth comprado pela equipe.','2017-05-09 10:42:18',2),(4,'13/05/2017','Correção de bug que impedia o usuário de finalizar o apontamento de horário no sistema de chamados.','2017-05-13 01:34:01',2),(1,'14/05/2017','Atualização da tela perfil.html','2017-05-14 01:03:24',5),(1,'23/05/2017','Desenvolvimento das telas','2017-05-23 23:40:22',5),(2,'23/05/2017','Desenvolvimento das telas','2017-05-23 23:40:23',5),(0,'16/05/2017','Desenvolvimento das telas','2017-05-16 12:56:32',2),(4,'23/05/2017','Desenvolvimento das telas','2017-05-23 16:40:33',2),(3,'23/05/2017','Desenvolvimento das telas','2017-05-23 16:40:34',2),(4,'03/06/2017','Alterações na Home','2017-06-03 17:45:51',2),(4,'04/06/2017','Início da parte funcional do aplicativo. (Formulário de cadastro).','2017-06-04 11:00:31',2),(0,'30/05/2017','Desenvolvimento das telas e do banco de dados.','2017-05-30 16:29:03',2),(0,'06/06/2017','Inclusão de Pesquisa Bibliográfica, Palavras Chave, Agradecimentos e Métodos para a amostra do COTUCA.','2017-06-06 20:17:30',2),(4,'08/06/2017','Alteração de domínio para adm.salus.ml (possibilidade de adição de site para usuário ou site promocional). Correções de bugs para o porte.','2017-06-08 02:14:59',2),(1,'12/06/2017','Correções na documentação','2017-06-12 21:09:13',2),(2,'12/06/2017','Correções na documentação','2017-06-12 21:09:14',2),(4,'12/06/2017','Correções na documentação','2017-06-12 21:09:15',2),(1,'11/06/2017','Criação de Slides para apresentação','2017-06-11 13:09:35',2),(2,'11/06/2017','Criação de Slides para apresentação','2017-06-11 13:09:36',2),(3,'11/06/2017','Criação de Slides para apresentação','2017-06-11 13:09:37',2),(4,'13/06/2017','Criação da confirmação de senha e ajustes.','2017-06-13 21:53:03',2),(4,'16/06/2017','Correção de bugs e ajustes nas páginas index.html, inicial.html, login.html, cadastro.html e seus respectivos arquivos de chamada php e js.\r\nImplementação do sistema de login e localStorage.','2017-06-16 00:06:05',2),(4,'18/06/2017','Criação de página inicial para o site com Wordpress.','2017-06-18 01:11:59',2),(4,'20/06/2017','Edições na página de perfil, alterações no banco de dados e toques finais nos scripts PHP já existentes.','2017-06-20 03:58:42',2),(4,'27/06/2017','Criação de página de apresentação que incorpora os Slides e permite comentários (perguntas). Mudança do link da apresentação para essa página.','2017-06-27 01:51:38',2),(0,'05/08/2017','Reunião para nova divisão das tarefas','2017-08-05 15:15:47',2),(4,'05/08/2017','Remoção de todos os vínculos do aplicativo com os arquivos do XDK e Cordova. Organização dos scripts e mudança na estrutura dos arquivos.','2017-08-05 03:51:20',2),(4,'06/08/2017','Correção de problemas relacionados aos PDFs baixados da área de administração.','2017-08-06 13:05:05',2),(1,'07/08/2017','Pesquisas sobre utilização de Cordova com comunicação Bluetooth e Visual Studio','2017-08-07 08:41:48',2),(4,'07/08/2017','Pesquisas sobre utilização de Cordova com comunicação Bluetooth e Visual Studio','2017-08-07 08:41:49',2),(4,'20/08/2017','Adição dinâmica de campos na página de perfil (Medicamentos e Enfermidades). Verificação de adição de responsável (falta enviar o ID do usuário, verificar se o usuário retornado não é ele mesmo, e criar o relacionamento no banco).','2017-08-20 15:37:32',2),(1,'21/08/2017','Instalação cordova','2017-08-21 20:55:26',5),(2,'21/08/2017','Instalação cordova','2017-08-21 20:55:27',5),(2,'22/08/2017','Testes com cordova para conexão bluetooth','2017-08-22 04:14:37',5),(4,'29/08/2017','Commit das primeira compilações, atualização dos backups dos bancos de dados, alterações nas pastas para estrutura semelhante com a hospedagem.','2017-08-29 17:16:54',2),(4,'26/08/2017','Primeiras compilações no Cordova do aplicativo. Correções relacionadas a essas combinações.','2017-08-26 12:02:50',2),(1,'04/09/2017','tentando realizar conexão bluetooth entre interface app e arduino.','2017-09-04 14:13:06',5),(2,'04/09/2017','tentando realizar conexão bluetooth entre interface app e arduino.','2017-09-04 14:13:07',5),(1,'29/08/2017','tentando realizar conexão bluetooth entre interface app e arduino.','2017-08-29 06:01:23',5),(2,'29/08/2017','tentando realizar conexão bluetooth entre interface app e arduino.','2017-08-29 06:01:24',5),(4,'06/09/2017','- Atualização da função do menu para utilização do IF ternário (mais legível). Correção de bugs relacionados ao redirecionamento e adição do redirecionamento automático do usuário caso ele tenha um dado de login registrado (no caso seu código) para a pági','2017-09-06 23:12:13',2),(4,'04/09/2017','- Atualização da página de configurações para inclusão de informações como versão, site, sobre e desenvolvedores.\r\n- Atualização dos modais para conteúdo dinâmico com barra de rolagem.\r\n- Alteração dos nomes dos modais da página inicial, criação dos modai','2017-09-04 14:45:03',2),(1,'08/09/2017','Realização da conexão entre aplicativo e módulo hc-05(enviar e receber dados)','2017-09-08 19:27:07',5),(4,'12/09/2017','- main.css: desabilitado a barra inferior que sobe quando um campo disabled é selecionado\r\n- jquery.mask.js: adiciona mascaras de entrada\r\n- main.js: ajustes na função de validação de campos, validação de login e logout (eliminação de vetor em vez de dado','2017-09-12 17:28:03',2),(4,'17/09/2017','- cadastro.html: Inclusão do tipo de usuário no \r\n- main.css: alteração de estilos para integração com as alterações\r\n- inicial.html: remoção do conteúdo das tabelas, renomeios e acrescentação de ids.\r\n- main.js: adicionado função de validação de cpf, tel','2017-09-17 05:00:16',2),(4,'17/09/2017','- cadastro.html: renomeação do campo Tipo de conta;\r\n- config.html: correção do menu e adição de versão;\r\n- inicial.html: adição de id para o btnSocorro;\r\n- config.js: adição de variável de versão;\r\n- inicial.js: esconde o botão socorro se tipo responsáve','2017-09-17 23:45:37',2);
/*!40000 ALTER TABLE `diario` ENABLE KEYS */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `user_cod` int(11) NOT NULL COMMENT 'Código do usuário',
  `user_login` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome de login (username) do usuário',
  `user_senha` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'Senha do usuário criptografada *hash md5*',
  `user_nome` varchar(30) COLLATE utf8_bin NOT NULL COMMENT 'Nome pessoal do usuário',
  PRIMARY KEY (`user_cod`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'matheusot','409c2baac455afee82a6823769e965c9','Matheus'),(3,'gabriel11447','fbb4320ba5afdf700f42641ff93690e4','Gabriel'),(4,'thiago0003','c04716c3c68d22851304dc6e02f1c4f6','Thiago'),(5,'breno','e10adc3949ba59abbe56e057f20f883e','Breno'),(0,'group_user','c83bf60cc072ad36c2aa245c44370d06','Grupo');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

--
-- Table structure for table `usuario_chamado`
--

DROP TABLE IF EXISTS `usuario_chamado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_chamado` (
  `usrchmd_user_cod` int(11) NOT NULL COMMENT 'Código do usuário vinculado ao chamado',
  `usrchmd_chmd_cod` int(11) NOT NULL COMMENT 'Código do chamado vinculado ao usuário',
  `usrchmd_chmd_vincini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data do início do vínculo',
  `usrchmd_chmd_vincfim` timestamp NULL DEFAULT NULL COMMENT 'Data do fim do vínculo',
  UNIQUE KEY `usrchmd_user_cod` (`usrchmd_user_cod`,`usrchmd_chmd_cod`,`usrchmd_chmd_vincini`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_chamado`
--

/*!40000 ALTER TABLE `usuario_chamado` DISABLE KEYS */;
INSERT INTO `usuario_chamado` VALUES (5,50,'2017-03-14 13:52:57','2017-03-28 12:52:57'),(3,50,'2017-03-14 13:52:57','2017-03-28 12:52:57'),(0,50,'2017-03-29 12:52:57','2017-09-17 05:00:07'),(2,51,'2017-03-16 14:06:10','2017-04-13 02:51:00'),(0,39,'2017-02-14 18:22:06','2017-02-14 20:22:06'),(4,49,'2017-03-14 13:52:57','2017-03-28 17:28:14'),(2,49,'2017-03-14 13:52:57','2017-03-28 17:28:14'),(0,47,'2017-02-19 19:03:11','2017-03-09 14:19:06'),(4,53,'2017-04-18 19:08:55',NULL),(2,51,'2017-04-21 04:24:05','2017-06-06 13:06:38'),(0,54,'2017-06-06 13:06:05','2017-06-06 13:06:17'),(2,54,'2017-06-06 13:06:22','2017-08-06 12:56:17');
/*!40000 ALTER TABLE `usuario_chamado` ENABLE KEYS */;

--
-- Dumping routines for database 'u485607745_salus'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-18  2:48:57
