﻿# GitLab da Salus

### Documentos:
  * Documentação inicial: https://docs.google.com/document/d/1XG896gBl79CmtXfEjHt3O0ZyBnPbHDTbTMrXmldbQ7Q/edit
  * Cronograma, Diário e Orçamentos: https://docs.google.com/spreadsheets/d/10Ajb46JcKN7VIIFp3xKhLxB09K9hEp1QYh1EG_1xTIM/edit


### Configurar o GitKraken com o GitLab:
  1. Baixe ele aqui https://www.gitkraken.com/download
  2. Instale
  3. Quando abrir, pedirá para criar uma conta, crie a sua.
  4. Após a criação da conta e o login, clique no botão superior da direita (três linhas) e vá em Preferences.
  5. Na aba Authentication, desmarque a caixa de seleção "Use local SSH agent" e clique em Generate, salve o arquivo na área de trabalho.
  6. Abra esse arquivo com o bloco de notas.
  7. Abra o GitLab e mande seu usuário para o Matheus.
  8. Vá nas configurações da conta, vá em SSH Keys e cole o código do bloco de notas.
  9. Salve com o nome que preferir.
  10. Volte ao GitKraken. Vá em File -> Clone Repo.
  11. Clique em Browse para selecionar a pasta onde serão sincronizados os arquivos.
  12. Em URL coloque o SSH do repositório, no caso: git@gitlab.com:matheusot/salus.git
  13. Use o menu principal para enviar os arquivos.


### Usando o GitKraken:
  1. Os botões no menu superior servem para, da esquerda para a direita:
    * Desfazer o último Commit;
    * Refazer o último Commit (caso tenha sido defeito);
    * Fazer o Pull (download do GitLab para a pasta do projeto);
    * Fazer o Push (enviar a pasta do projeto para o GitLab);
    * Criar um Branch (subdivisão de desenvolvimento);
    * Fazer Stash;
    * Fazer Pop Stash.


  2. O menu ao lado esquerdo mostra, de cima para baixo:
    * Os Branches locais;
    * Os Branches remotos (GitLab);
    * Os Stashes;
    * As Tags;
    * Os Submódulos.
    Ao clicar com o botão direito nos Branches pode-se fazer Pull, Push, Reverter Commit e criar outros Branches.


  3. O menu ao lado direito mostra:
    * Os arquivos modificados (ao clicar duas vezes pode-se ver o que foi alterado com relação ao último Commit);
    * Área de Stage, onde os arquivos são salvos;
    * Mensagem de Commit e Descrição.

    