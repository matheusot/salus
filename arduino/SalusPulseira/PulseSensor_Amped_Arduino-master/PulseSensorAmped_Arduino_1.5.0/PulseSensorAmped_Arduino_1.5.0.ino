
/*  Pulse Sensor Amped 1.5    by Joel Murphy and Yury Gitman   http://www.pulsesensor.com

----------------------  Notes ----------------------  ----------------------
This code:
1) Blinks an LED to User's Live Heartbeat   PIN 13
2) Fades an LED to User's Live HeartBeat    PIN 5
3) Determines BPM
4) Prints All of the Above to Serial

Read Me:
https://github.com/WorldFamousElectronics/PulseSensor_Amped_Arduino/blob/master/README.md
 ----------------------       ----------------------  ----------------------
*/
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <EEPROM.h>

#define PROCESSING_VISUALIZER 1
#define SERIAL_PLOTTER  2

//  Variables
int pulsePin = 0;                 // Pulse Sensor purple wire connected to analog pin 0
int blinkPin = 13;                // pin to blink led at each beat
int fadePin = 5;                  // pin to do fancy classy fading blink at each beat
int fadeRate = 0;                 // used to fade LED on with PWM on fadePin
int pinosensor = A1;  
int pin = A2; 

// Volatile Variables, used in the interrupt service routine!
volatile int BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded!
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat".
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.
int tempc = 0; 
int i;
int endereco = 0;
int analogValue = 0;
int ledDelay = 100;
int contagem =0;
float leitura;
float voltage = 0;
float variavel= 0;
int samples[8];
int maxtemp = -100,mintemp = 100; 

// SET THE SERIAL OUTPUT TYPE TO YOUR NEEDS
// PROCESSING_VISUALIZER works with Pulse Sensor Processing Visualizer
//      https://github.com/WorldFamousElectronics/PulseSensor_Amped_Processing_Visualizer
// SERIAL_PLOTTER outputs sensor data for viewing with the Arduino Serial Plotter
//      run the Serial Plotter at 115200 baud: Tools/Serial Plotter or Command+L
static int outputType = SERIAL_PLOTTER;


void setup(){
  Serial.begin(9600);          
  pinMode(8, OUTPUT);
  pinMode(9,OUTPUT);
  
  pinMode(blinkPin,OUTPUT);         // pin that will blink to your heartbeat!
  pinMode(fadePin,OUTPUT);          // pin that will fade to your heartbeat!
  interruptSetup();                 // sets up to read Pulse Sensor signal every 2mS
   // IF YOU ARE POWERING The Pulse Sensor AT VOLTAGE LESS THAN THE BOARD VOLTAGE,
   // UN-COMMENT THE NEXT LINE AND APPLY THAT VOLTAGE TO THE A-REF PIN
//   analogReference(EXTERNAL);
}
 
 void Bateria(){
  analogValue = analogRead(A1);
    voltage = (0.0048*analogValue);
   if( voltage < 4 ){
    digitalWrite(8, HIGH);}
    else {
      digitalWrite(8, LOW);
    }
     Serial.print(voltage);
     Serial.print("|");
 }

void Temperatura(){
    for(i = 0;i<=7;i++){ // Loop que faz a leitura da temperatura 8 vezes
    samples[i] = ( 5.0 * analogRead(pin) * 100.0) / 1024.0;
    //A cada leitura, incrementa o valor da variavel tempc
    tempc = tempc + samples[i]; 
  delay(100);
}
// Divide a variavel tempc por 8, para obter precisão na medição
tempc = tempc/8.0; 

//Se a temperatura estiver acima de 37, acende o led vermelho
if(tempc > 37) 
  {
    digitalWrite(9, HIGH);
  }else {
    digitalWrite(9, LOW);
  }
Serial.print(tempc,DEC);
Serial.print("|");
Serial.print(0);
tempc = 0;
}




//  Where the Magic Happens
void loop(){
    if (QS == true){     // A Heartbeat Was Found
                       // BPM and IBI have been Determined
                       // Quantified Self "QS" true when arduino finds a heartbeat
        fadeRate = 255;         // Makes the LED Fade Effect Happen
                                // Set 'fadeRate' Variable to 255 to fade LED with pulse
        serialOutputWhenBeatHappens();   // A Beat Happened, Output that to serial.
        QS = false;                      // reset the Quantified Self flag for next time
  }
    serialOutput() ;
    Temperatura();
    Bateria();
  delay(30000);                             //  take a break
}

