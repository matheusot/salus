#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <EEPROM.h>

int pinosensor = A1;  
int pin = A2;          
int greenLed = 9;
int redLed = 8;
int tempc = 0; 
int samples[8];
int maxtemp = -100,mintemp = 100; 
int i;
int endereco = 0;
int analogValue = 0;
int ledDelay = 100;
int contagem =0;
float leitura;
float voltage = 0;
float variavel= 0;
 
void setup()
{       
  Serial.begin(9600);          
  pinMode(greenLed, OUTPUT);
  pinMode(redLed,OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  
}

 void Bluetooth(){
  leitura = analogRead(pinosensor);
    Serial.println(leitura);
    delay(100);
 }
 
 void Bateria(){
  analogValue = analogRead(A0);
    voltage = (0.0048*analogValue);
   if( voltage > 4 ){
    digitalWrite(greenLed, HIGH);}
  else {
     digitalWrite(redLed, HIGH);  }
     Serial.println(voltage);
 }

void Temperatura(){
    for(i = 0;i<=7;i++){ // Loop que faz a leitura da temperatura 8 vezes
    samples[i] = ( 5.0 * analogRead(pin) * 100.0) / 1024.0;
    //A cada leitura, incrementa o valor da variavel tempc
    tempc = tempc + samples[i]; 
  delay(100);
}
// Divide a variavel tempc por 8, para obter precisão na medição
tempc = tempc/8.0; 
//Se a temperatura estiver abaixo de 38, acende o led verde
if(tempc < 38) 
  {
    digitalWrite(13, HIGH);
    digitalWrite(12, LOW);
  }
//Se a temperatura estiver acima de 37, acende o led vermelho
if(tempc > 37) 
  {
    digitalWrite(12, HIGH);
    digitalWrite(13, LOW);
  }
Serial.print(tempc,DEC);
Serial.print(" Cels., ");
tempc = 0;
}

void loop()
{
  Bateria();
  Temperatura();
  Bluetooth();
     
     }
