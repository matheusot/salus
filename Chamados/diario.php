﻿<?php
  define('MPDF_PATH', 'class/mpdf/');
  include(MPDF_PATH.'mpdf.php');

  require_once("connect.php");

  session_start();
  if (empty($_SESSION['logado'])) {
  Header ("Location: index.php?e=3");
} else {

  $login = $_SESSION['logado'];

  $sql = "SELECT diar_usr_id,
							   diar_data,
							   diar_desc
					FROM diario
          ORDER BY diar_data_insert";

	$res = mysqli_query($link, $sql)	or die (mysql_error());

  if (@mysqli_num_rows($res)==0) {
    $html = "Erro"; //Escreve Erro
  } else {
    /* Carrega CSS da Tabela */
    $css =
      "<style>
            table {
              border-collapse: collapse;
              font-family: Arial, Sans Serif;
            }

            table, th, td {
              border: 1px solid black;
              text-align: center;
              vertical-align: middle;
              padding-left: 5px;
              padding-right: 5px;
            }

            th {
              background-color: #ddd;
              vertical-align: middle;
            }

            .square {
              width: 25px;
              background-color: black;
              height: 1px;
            }

            .line {
              background-color: black;
              height: 1px;
            }
      </style>";

    $corpo = '';

    $table_ini = "<table>
    <tr>
      <td rowspan='2'>
        <img src='img/logo_black.png' height='80px' style='padding-top:20px; padding-bottom:20px;'>
      </td>
      <th colspan='40' valign='middle'>
        <span style='font-size: 20px;'>Diário</span>
      </th>
    </tr>
    <tr height='80px'>
      <td colspan='20'>
        Gerado em ".date("d/m/Y")." - ".date("H:i")."
      </td>
      <td colspan='20'>
        Gerado por ".$login['nome']."
      </td>
    </tr>
    <tr>
      <th colspan='21'>Descrição</th>
      <th colspan='10'>Pessoa</th>
      <th colspan='10'>Data</th>
    </tr>
    "; // início table
    $i = 0;
    while($data = mysqli_fetch_array($res)){
      $i++;
      if ($data[0]==0) {
        $pessoa = "Grupo";
      }
      if ($data[0]==1) {
        $pessoa = "Breno";
      }
      if ($data[0]==2) {
        $pessoa = "Gabriel";
      }
      if ($data[0]==3) {
        $pessoa = "Thiago";
      }
      if ($data[0]==4) {
        $pessoa = "Matheus";
      }
      $descricao = str_replace("\n", '<br/>', $data[2]); 
      $corpo .=
      "<tr>
        <td colspan='21' style='text-align:left;padding-left:5px;padding-right:5px;'>$descricao</td>
        <td colspan='10' style='text-align:center;padding-left:5px;padding-right:5px;'>$pessoa</td>
        <td colspan='10' style='text-align:center;padding-left:5px;padding-right:5px;'>$data[1]</td>
      </tr>";
    }
    $corpo .=  "<tr class='line'>";
    $cont=0;
    do {
      $corpo .= "<td class='square'><td>";
      $cont++;
    } while($cont<20);

    $corpo .=  "</tr>";

  $table_fim = "</table>";

  $html = $css.$table_ini.$corpo.$table_fim;

  $footer = "
  <table width='100%' style='font-size: 8pt; border: none;'>
    <tr>
      <td width='30%' style='border: none; text-align:left;'>".date("d/m/Y")." - ".date("H:i")."</td>
      <td width='40%' style='border: none;'>Copyright &copy; Todos os direitos autorais reservados à Salus</td>
      <td width='30%' style='border: none; text-align:right;'>Página {PAGENO} de {nbpg}</td>
    </tr>
  </table>";

  //echo $html;
  $filename = 'Salus-Diario(' . date('d-m-Y-H-i-s') . ')-' . $login['nome'] .'.pdf';

  ob_clean();
  $mpdf=new mPDF('c', 'A4');
  $mpdf->SetHTMLFooter($footer);
  $mpdf->SetWatermarkImage('img/marca.png');
  $mpdf->showWatermarkImage = true;
  $mpdf->WriteHTML($html);
  $mpdf->Output($filename, 'D');
  exit;
  }
}
?>
