<?php
require_once("connect.php");
date_default_timezone_set('America/Sao_Paulo');

session_start();
if (empty($_SESSION['logado'])) {
Header ("Location: index.php?e=3");
} else {
  if (empty($_POST['breno']) && empty($_POST['gabriel']) && empty($_POST['matheus']) && empty($_POST['thiago'])) {
    Header ("Location: index.php?e=3");
  }

  $dados_login = $_SESSION['logado'];
  $user_cod = $dados_login['cod'];
  $desc = $_POST['chmd-desc'];
  if (empty($_POST['data'])) {
    $data = date("d/m/Y");
    $data_insert = date("Y-m-d H:i:s");
  } else {
    $dia = substr($_POST['data'], 8, 2);
    $mes = substr($_POST['data'], 5, 2);
    $ano = substr($_POST['data'], 0, 4);
    $data = $dia.'/'.$mes.'/'.$ano;
    $data_insert = date("Y-m-d H:i:s", mktime(rand(0,23), rand(0,59), rand(0,59), $mes, $dia, $ano));
  }
  $time_insert = strtotime($data_insert);

  if (!empty($_POST['breno']) && !empty($_POST['gabriel']) && !empty($_POST['matheus']) && !empty($_POST['thiago'])) {
    $data_insert = date("Y-m-d H:i:s", $time_insert);
    $sql = "INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES ('0', '" . $data . "', '" . $desc . "',  '" . $data_insert . "', '" . $user_cod . "');";
    $res = mysqli_query($link, $sql);
  } else {
    if (!empty($_POST['breno'])) {
      $time_insert += 1;
      $data_insert = date("Y-m-d H:i:s", $time_insert);
      $sql = "INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES ('1', '" . $data . "', '" . $desc . "',  '" . $data_insert . "', '" . $user_cod . "');";
      $res = mysqli_query($link, $sql);
    }
    if (!empty($_POST['gabriel'])) {
      $time_insert += 1;
      $data_insert = date("Y-m-d H:i:s", $time_insert);
      $sql = "INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES ('2', '" . $data . "', '" . $desc . "',  '" . $data_insert . "', '" . $user_cod . "');";
      $res = mysqli_query($link, $sql);
    }
    if (!empty($_POST['matheus'])) {
      $time_insert += 1;
      $data_insert = date("Y-m-d H:i:s", $time_insert);
      $sql = "INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES ('4', '" . $data . "', '" . $desc . "',  '" . $data_insert . "', '" . $user_cod . "');";
      $res = mysqli_query($link, $sql);
    }
    if (!empty($_POST['thiago'])) {
      $time_insert += 1;
      $data_insert = date("Y-m-d H:i:s", $time_insert);
      $sql = "INSERT INTO `diario` (`diar_usr_id`, `diar_data`, `diar_desc`, `diar_data_insert`, `diar_usr_cod_ins`) VALUES ('3', '" . $data . "', '" . $desc . "',  '" . $data_insert . "', '" . $user_cod . "');";
      $res = mysqli_query($link, $sql);
    }
  }
  if ($res) {
    header("Location: index.php?i");
  } else {
    header("Location: index.php?e=4");
  }
}
?>
