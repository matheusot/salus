<?php
	require_once("connect.php");

	$cod = $_REQUEST['data'];

	$sql = "SELECT chmd_cod,
							   chmd_data_abert,
							   chmd_titulo,
							   chmd_descricao,
							   chmd_prioridade,
							   chmd_status,
								 chmd_data_ini,
								 chmd_data_fecha
					FROM chamado
					WHERE chmd_cod = $cod";

	$res = mysqli_query($link, $sql)	or die (mysql_error());

	if (@mysqli_num_rows($res)==0) { ?>
		<div class="modal-header">
			<h5 class="modal-title">Chamado inválido!</h5>
		</div>
		<div class="modal-body">Não existem informações cadastradas sobre este chamado!</div>
		<div class="modal-footer">
			<a class="btn btn-secondary" data-dismiss="modal">Fechar</a>
		</div>
	<?php
	} else {
		while($data = mysqli_fetch_array($res)){
			$chmd_cod = $data[0]; 							//recebe cod
			$chmd_data = $data[1]; 							//recebe data
			$chmd_titulo = $data[2]; 						//recebe titulo
			$chmd_descricao = $data[3]; 				//recebe desc
			$chmd_prioridade = $data[4];				//recebe prioridade
			$chmd_status = $data[5]; 						//recebe status
			$chmd_data_ini = $data[6]; 					//recebe data inicial
			$chmd_data_fecha = $data[7]; 					//recebe data inicial

		/* Formatando Data */
		$chmd_data_ano = substr($chmd_data, 0, 4); //recorta ano
		$chmd_data_mes = substr($chmd_data, 5, 2); //recorta mes
		$chmd_data_dia = substr($chmd_data, 8, 2); //recorta dia
		$chmd_data_hora = substr($chmd_data, 11, 2); //recorta hora
		$chmd_data_min = substr($chmd_data, 14, 2); //recorta data
		$data_hoje = $chmd_data_dia.'/'.$chmd_data_mes.'/'.$chmd_data_ano; //formata data
		if ($data_hoje == date("d/m/Y")) { //verifica se a data é de hoje
			$chmd_data = $chmd_data_hora.':'.$chmd_data_min; //define data como hora (já que é de hoje)
		} else {
			$chmd_data = $chmd_data_dia.'/'.$chmd_data_mes.'/'.$chmd_data_ano.' '.$chmd_data_hora.':'.$chmd_data_min; //formata data-hora
		}

		//compara as datas para calcular diferença:
		if ($chmd_data_ini == '') {
			$data_hoje = new DateTime(date("Y-m-d"));
		} else {
			$chmd_data_ini_ano = substr($chmd_data_ini, 0, 4); //recorta ano
			$chmd_data_ini_mes = substr($chmd_data_ini, 5, 2); //recorta mes
			$chmd_data_ini_dia = substr($chmd_data_ini, 8, 2); //recorta dia
			$data_hoje = new DateTime($chmd_data_ini_ano.'-'.$chmd_data_ini_mes.'-'.$chmd_data_ini_dia);
		}
		$data_chamado = new DateTime($chmd_data_ano.'-'.$chmd_data_mes.'-'.$chmd_data_dia);

		$tempo = $data_hoje->diff($data_chamado);


		$class = "badge badge-pill data";
		$prior = "";
		$cprior = "";

		if ($chmd_prioridade=='0') { //se prioridade = 0: mostra amarelo (prioridade baixa)
			$cprior = $class . " prioridade-0";
			$prior = "Baixa";
		} else {
			if ($chmd_prioridade=='1') { //se prioridade = 0: mostra laranja (prioridade média)
				$cprior = $class . " prioridade-1";
				$prior = "Média";
			} else {
				if ($chmd_prioridade=='2') { //se prioridade = 0: mostra vermelho (prioridade alta)
					$cprior = $class . " prioridade-2";
					$prior = "Alta";
				}
			}
		}

		if ($tempo->d <= 15 && $tempo->m == 0 && $tempo->y == 0) {  //o tempo for < 15 dias deixa mais escuro (tempo-1)
			$cprior .= " tempo-1";
		} else {
			if ($tempo->d > 15 && $tempo->d <= 30 && $tempo->m == 0 && $tempo->y == 0) { //o tempo for > 15 e < 31 dias deixa mais escuro ainda (tempo-2)
				$cprior .= " tempo-2";
				$prior .= " (Atrasada)";
			} else {
				if ($tempo->d > 30 && $tempo->m == 0 && $tempo->y == 0) { //o tempo for > 30 dias deixa muito escuro (tempo-3)
					$cprior .= " tempo-3";
					$prior .= " (Muito atrasada)";
				} else {
					$cprior .= " tempo-3";
					$prior .= " (Muito atrasada)";
				}
			}
		}

		if ($chmd_status=='2') { //se status = 2: mostra verde (terminado)
			$cstat = $class . " status-2";
			$stat = "Terminado";
			$chmd_btn = null;
		} else {
			if ($chmd_status=='1') { //se status = 1: mostra azul (em processo)
				$cstat = $class . " status-1";
				$stat = "Em processo";
				$chmd_btn = "Editar vínculos";
			} else {
				if ($chmd_status=='3') { //se status = 3: mostra cinza (cancelado)
					$cstat = $class . " status-3";
					$stat = "Cancelado";
					$chmd_btn = "Razão";
				} else {
					if ($chmd_status=='0') { //se status = 0: mostra vermelho (não vinculado)
						$cstat = $class . " status-0";
						$stat = "Aberto";
						$chmd_btn = "Vincular";
					}
				}
			}
		}
		?>
    <div class="modal modal-vincs" id="modal-vincs">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Alteração de vínculos</h5>
          </div>
          <div class="modal-body">
            <form method="post" action="vincula.php" name="vincula">
							<input type="number" name="cod" id="cod" value="<?=$cod?>" style="display: none;">
              <div class="form-group">
                <label for="chmd-users">Vincular usuários</label></br>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="breno" id="breno" value="Breno">
                        Breno
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="gabriel" id="gabriel" value="Gabriel">
                        Gabriel
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="matheus" id="matheus" value="Matheus">
                        Matheus
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="thiago" id="thiago" value="Thiago">
                        Thiago
                      </label>
                    </div>
                  </div>
									<div class="col-md-6 col-sm-6 col-lg-6">
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="checkbox" name="grupo" id="grupo" value="Grupo">
												Grupo
											</label>
										</div>
									</div>
                </div>
              </div>
              <?php
							$sql = "SELECT usuario.user_nome,
														 usuario_chamado.usrchmd_chmd_vincini,
														 usuario_chamado.usrchmd_chmd_vincfim,
														 usuario_chamado.usrchmd_user_cod
											FROM usuario
											INNER JOIN usuario_chamado ON usuario.user_cod = usuario_chamado.usrchmd_user_cod
											WHERE usuario_chamado.usrchmd_chmd_cod = $cod";
							$res = mysqli_query($link, $sql)	or die (mysql_error());
                 ?>
              <div class="form-group">
                <label for="login-senha">Alteração dos vínculos</label>
                <table class="table table-sm table-responsive">
                  <tr>
                    <th>Pes.</th>
                    <th>Dt Hr Ini.</th>
                    <th>Dt Hr Fim.</th>
                  </tr>
                  <?php
                  while($data = mysqli_fetch_array($res)){
                    $user_nome = $data[0]; 					  	//recebe nome do user
                    $usrchmd_chmd_vincini = $data[1]; 	//recebe data inicio
                    $usrchmd_chmd_vincfim = $data[2]; 	//recebe data fim
										$usr_cod = $data[3];

                    /* Formatando Data */
                    $usrchmd_chmd_vincini_ano = substr($usrchmd_chmd_vincini, 0, 4); //recorta ano
                    $usrchmd_chmd_vincini_mes = substr($usrchmd_chmd_vincini, 5, 2); //recorta mes
                    $usrchmd_chmd_vincini_dia = substr($usrchmd_chmd_vincini, 8, 2); //recorta dia
                    $usrchmd_chmd_vincini_hora = substr($usrchmd_chmd_vincini, 11, 2); //recorta hora
                    $usrchmd_chmd_vincini_min = substr($usrchmd_chmd_vincini, 14, 2); //recorta data
                    $usrchmd_chmd_vincini = $usrchmd_chmd_vincini_dia.'/'.$usrchmd_chmd_vincini_mes.'/'.$usrchmd_chmd_vincini_ano.' '.$usrchmd_chmd_vincini_hora.':'.$usrchmd_chmd_vincini_min; //formata data

                    /* Formatando Data */
                    if ($usrchmd_chmd_vincfim != '') {
                    $usrchmd_chmd_vincfim_ano = substr($usrchmd_chmd_vincfim, 0, 4); //recorta ano
                    $usrchmd_chmd_vincfim_mes = substr($usrchmd_chmd_vincfim, 5, 2); //recorta mes
                    $usrchmd_chmd_vincfim_dia = substr($usrchmd_chmd_vincfim, 8, 2); //recorta dia
                    $usrchmd_chmd_vincfim_hora = substr($usrchmd_chmd_vincfim, 11, 2); //recorta hora
                    $usrchmd_chmd_vincfim_min = substr($usrchmd_chmd_vincfim, 14, 2); //recorta data
                    $usrchmd_chmd_vincfim = $usrchmd_chmd_vincfim_dia.'/'.$usrchmd_chmd_vincfim_mes.'/'.$usrchmd_chmd_vincfim_ano.' '.$usrchmd_chmd_vincfim_hora.':'.$usrchmd_chmd_vincfim_min; //formata data
                    }
                    echo "<tr>";
                    echo "<td>".$user_nome."</td>";
                    echo "<td>".$usrchmd_chmd_vincini."</td>";
                    if ($usrchmd_chmd_vincfim != '') {
                      echo "<td>".$usrchmd_chmd_vincfim."</td>";
                    } else {
											$url = "finaliza.php?cod=$cod&user=$usr_cod&ini=$data[1]";
                      echo "<td><a href='$url' class='btn btn-xs btn-primary'>Finalizar</a></td>";
										}
                    echo "</tr>";
                  }
                   ?>
                </table>
              </div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Vincular
								</button>
								<a href="#" onclick="$('.modal-vincs').modal('hide');" class="btn btn-secondary">Fechar</a>
							</div>
            </form>
          </div>
        </div>
      </div>
    </div>


		<div class="modal-header">
			<h5 class="modal-title"><?=$chmd_titulo?></h5>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<b>Codigo:</b></br>
					<?=$chmd_cod?>
				</div>
				<div class="col-md-6">
					<b>Data e Hora de abertura:</b></br>
					<?=$chmd_data?>
				</div>
			</div>
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-6">
					<b>Prioridade:</b></br>
					<span class="<?=$cprior?>"><?=$prior?></span>
				</div>
				<div class="col-md-6">
					<b>Status:</b></br>
					<span class="<?=$cstat?>"><?=$stat?></span>
				</div>
			</div>
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-12">
					<b>Descrição:</b></br>
					<?=$chmd_descricao?>
				</div>
			</div>
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-12">
					<b style="margin-bottom: 10px;">Vínculos:</b>
							<?php
							$sql = "SELECT usuario.user_nome,
													   usuario_chamado.usrchmd_chmd_vincini,
													   usuario_chamado.usrchmd_chmd_vincfim
											FROM usuario
											INNER JOIN usuario_chamado ON usuario.user_cod = usuario_chamado.usrchmd_user_cod
											WHERE usuario_chamado.usrchmd_chmd_cod = $cod";
							$res = mysqli_query($link, $sql)	or die (mysql_error());

							if (@mysqli_num_rows($res)==0) {
								echo "<br>Não há vinculos!";
							} else {
								?>
									<table class="table table-hover table-bordered table-condensed">
										<thead>
											<tr>
												<th>Usuário</th>
												<th>Início</th>
												<th>Fim</th>
											</tr>
										</thead>

								<?php
								$todos_com_finalizacao = true;
								while($data = mysqli_fetch_array($res)){
									$user_nome = $data[0]; 					  	//recebe nome do user
									$usrchmd_chmd_vincini = $data[1]; 	//recebe data inicio
									$usrchmd_chmd_vincfim = $data[2]; 	//recebe data fim

									/* Formatando Data */
									$usrchmd_chmd_vincini_ano = substr($usrchmd_chmd_vincini, 0, 4); //recorta ano
									$usrchmd_chmd_vincini_mes = substr($usrchmd_chmd_vincini, 5, 2); //recorta mes
									$usrchmd_chmd_vincini_dia = substr($usrchmd_chmd_vincini, 8, 2); //recorta dia
									$usrchmd_chmd_vincini_hora = substr($usrchmd_chmd_vincini, 11, 2); //recorta hora
									$usrchmd_chmd_vincini_min = substr($usrchmd_chmd_vincini, 14, 2); //recorta data
									$usrchmd_chmd_vincini = $usrchmd_chmd_vincini_dia.'/'.$usrchmd_chmd_vincini_mes.'/'.$usrchmd_chmd_vincini_ano.' '.$usrchmd_chmd_vincini_hora.':'.$usrchmd_chmd_vincini_min; //formata data

									/* Formatando Data */
									if ($usrchmd_chmd_vincfim != '') {
									$usrchmd_chmd_vincfim_ano = substr($usrchmd_chmd_vincfim, 0, 4); //recorta ano
									$usrchmd_chmd_vincfim_mes = substr($usrchmd_chmd_vincfim, 5, 2); //recorta mes
									$usrchmd_chmd_vincfim_dia = substr($usrchmd_chmd_vincfim, 8, 2); //recorta dia
									$usrchmd_chmd_vincfim_hora = substr($usrchmd_chmd_vincfim, 11, 2); //recorta hora
									$usrchmd_chmd_vincfim_min = substr($usrchmd_chmd_vincfim, 14, 2); //recorta data
									$usrchmd_chmd_vincfim = $usrchmd_chmd_vincfim_dia.'/'.$usrchmd_chmd_vincfim_mes.'/'.$usrchmd_chmd_vincfim_ano.' '.$usrchmd_chmd_vincfim_hora.':'.$usrchmd_chmd_vincfim_min; //formata data
								} else {
									$todos_com_finalizacao = false;
								}
									echo "<tr>";
									echo "<td>".$user_nome."</td>";
									echo "<td>".$usrchmd_chmd_vincini."</td>";
									if ($usrchmd_chmd_vincfim != '') {
										echo "<td>".$usrchmd_chmd_vincfim."</td>";
									} else {
										echo "<td>Atual</td>";
									}
									echo "</tr>";
								}
							}
							?>
						</table>
				</div>
			</div>
		</div>
	<div class="modal-footer">
    <?php if ($chmd_btn != null && $chmd_btn != "Razão") { ?>
						<a href="#" data-toggle="modal" data-target="#modal-vincs" data-backdrop="false" class="btn btn-primary"><?=$chmd_btn?></a>
    <?php }
		if (isset($todos_com_finalizacao)) {
					if ($todos_com_finalizacao && $chmd_status!='2') { ?>
						<a href="termina.php?cod=<?=$cod?>" class="btn btn-warning">Terminar</a>
		<?php }
		}
		if ($chmd_status!='2' && $chmd_status!='3') { ?>
			<a href="cancela.php?cod=<?=$cod?>" class="btn btn-danger">Cancelar</a>
		<?php	}
		if ($chmd_status=='2') { ?>
			<a href="reabre.php?cod=<?=$cod?>" class="btn btn-success">Reabrir</a>
			<?php	} ?>
		<a href="#" class="btn btn-secondary" data-dismiss="modal">Fechar</a>
	</div>
		<?php

	}}
?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
