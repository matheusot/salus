<?php
	require_once("connect.php");

  $sql = 'SELECT usuario.user_nome,
                 COUNT(usuario_chamado.usrchmd_user_cod)
          FROM usuario_chamado
          INNER JOIN usuario ON usuario.user_cod = usuario_chamado.usrchmd_user_cod
          GROUP BY usrchmd_user_cod';

  $res = mysqli_query($link, $sql)	or die (mysql_error());

  if (@mysqli_num_rows($res)==0) {
		echo "<div class='alert alert-common'>";
		echo "<h4 class='alert-heading'>Nenhum vínculo com usuário específico!</h4>";
		echo "</div>";
	} else {

		$grupo = false;
    $array_nome = array();
    $array_qtd = array();
		$cont = 0;
		$grupo_qtd = 0;

  while($data = mysqli_fetch_array($res)){
		if ($data[0]!="Grupo") {
			$array_nome[$cont] = "\"$data[0]\"";
			$array_qtd[$cont] = "$data[1]";
			$cont++;
  	} else {
			$grupo_qtd = $data[1];
		}
	}

	foreach ($array_qtd as $key => $value) {
		$array_qtd[$key] += $grupo_qtd;
	}

  $array_nome = implode(",", $array_nome);
  $array_qtd = implode(",", $array_qtd);
}
?>
<div class="alert alert-common">
	<h4 class="alert-heading">Chamados vinculados por usuário</h4>
		<canvas id="myChart" style="margin-top: 20px;"></canvas>
</div>


<script>
  var ctx = document.getElementById("myChart");
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: [<?=$array_nome?>],
          datasets: [{
              label: 'Chamados vinculados',
              data: [<?=$array_qtd?>],
              backgroundColor: ["rgba(129, 240, 193, .2)", "rgba(128, 234 , 239, .2)", "rgba(127, 239, 155, .2)", "rgba(136, 239, 127, .2)"],
              borderColor: ["rgb(129, 240, 193)", "rgb(128, 234 , 239)", "rgb(127, 239, 155)", "rgb(136, 239, 127)"],
              borderWidth: 1,
              hoverBackgroundColor: ["rgba(129, 240, 193, .4)", "rgba(128, 234 , 239, .4)", "rgba(127, 239, 155, .4)", "rgba(136, 239, 127, .4)"],
          }]
      },
      options: {
					legend:{
						display:false
 					},
          scales: {
							Axes: [{
        				display: true,
        				gridLines: {
									color: "rgba(255, 255, 255, .2)",
        				}
      				}],
              yAxes: [{
								display: true,
        				gridLines: {
									color: "rgba(255, 255, 255, .2)",
        				},
								ticks: {
										fixedStepSize: 1,
										beginAtZero: true
								}
              }]
          }
      }
  });
</script>
