<?php
session_start();

if (!empty($_SESSION['logado']))
    $login = $_SESSION['logado'];

if (isset($_GET["logout"])) {
    $_SESSION = array();
    session_destroy();
}
?>
<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Salus">
    <meta name="author" content="Breno Veroneze, Gabriel de Carvalho, Matheus Rodrigues e Thiago Martins">
    <title>Salus</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
</head>
<body>
<?php if (isset($_GET["logout"])) { ?>
    <div class="alert alert-success alert-dismissible msg" role="alert">
        Deslogado com <strong>sucesso!</strong>
    </div>
<?php } if (isset($_GET["s"])) { ?>
    <div class="alert alert-success alert-dismissible msg" role="alert">
        Bem vindo <strong><?php echo $login['nome']?></strong>
    </div>
<?php } if (isset($_GET["e"]) && $_GET["e"] == 1) { ?>
    <div class="alert alert-danger alert-dismissible msg" role="alert">
        Usuário <strong>não encontrado!</strong>
    </div>
<?php } if (isset($_GET["e"]) && $_GET["e"] == 2) { ?>
    <div class="alert alert-danger alert-dismissible msg" role="alert">
        Senha <strong>incorreta!</strong>
    </div>
<?php } if (isset($_GET["e"]) && $_GET["e"] == 3) { ?>
    <div class="alert alert-danger alert-dismissible msg" role="alert">
        <strong>É necessário estar logado para acessar esta função!</strong>
    </div>
<?php } ?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarText" aria-expanded="false">
                <span class="sr-only">Habilitar navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/logo_salus.png" height="25px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <?php if (!empty($_SESSION['logado'])) { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Cronograma <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" data-toggle="modal" data-target=".modal-chmd">Cadastrar chamado</a>
                            </li>
                            <li>
                                <a href="cronograma.php" target="_blank">Gerar Cronograma</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Diário <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" data-toggle="modal" data-target=".modal-diario">Cadastrar evento</a>
                            </li>
                            <li>
                                <a href="diario.php" target="_blank">Gerar Diário</a>
                            </li>
                            <li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php if (empty($_SESSION['logado'])) { ?>
                        <a href="#" data-toggle="modal" data-target="#modal-login"><i class="fa fa-user-circle" aria-hidden="true"></i> Login</a>
                    <?php } else {
                        $dados = $_SESSION['logado'];?>
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user-circle" aria-hidden="true"></i>
                            <?=$dados['nome']?> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index.php?logout">Sair</a>
                            </li>
                        </ul>
                    <?php } ?>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="modal fade" id="modal-informacoes">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="informacoes">
        </div>
    </div>
</div>
<div class="modal fade modal-chmd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inserir Chamado</h5>
            </div>
            <div class="modal-body">
                <form method="post" action="insere.php" name="insere">
                    <div class="form-group">
                        <label for="chmd-titulo">Título</label>
                        <input type="text" class="form-control" id="chmd-titulo" name="chmd-titulo" required>
                    </div>
                    <div class="form-group">
                        <label for="chmd-desc">Descrição</label>
                        <textarea class="form-control" id="chmd-desc" name="chmd-desc" rows="3" required></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="chmd-prior">Prioridade</label>
                                <select class="form-control" id="chmd-prior" name="chmd-prior">
                                    <option value="0">Baixa</option>
                                    <option value="1">Média</option>
                                    <option value="2">Alta</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-lg-6">
                            <div class="form-group">
                                <label for="chmd-users">Usuários vinculados</label></br>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="breno" id="breno" value="Breno"> Breno
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="gabriel" id="gabriel" value="Gabriel"> Gabriel
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="matheus" id="matheus" value="Matheus"> Matheus
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="thiago" id="thiago" value="Thiago"> Thiago
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="grupo" id="grupo" value="Grupo">
                                                Grupo
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-block">Inserir</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modal-diario" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inserir Evento</h5>
            </div>
            <div class="modal-body">
                <form method="post" action="evento.php" name="insere">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="chmd-users">Usuários vinculados</label></br>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="breno" id="breno" value="Breno"> Breno
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="gabriel" id="gabriel" value="Gabriel"> Gabriel
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="matheus" id="matheus" value="Matheus"> Matheus
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="thiago" id="thiago" value="Thiago"> Thiago
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="data">Data: (preencha apenas se for diferente de hoje)</label>
                                <input type="date" class="form-control" id="data" name="data">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="chmd-desc">Descrição</label>
                        <textarea class="form-control" id="chmd-desc" name="chmd-desc" rows="3" required></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-block">Inserir</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-login">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Formulário de Login</h5>
            </div>
            <form method="post" action="login.php" name="login">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="login-user">Usuário</label>
                        <input type="text" class="form-control" id="login-user" name="login-user">
                    </div>
                    <div class="form-group">
                        <label for="login-senha">Senha</label>
                        <input type="password" class="form-control" id="login-senha" name="login-senha">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6" id="grafico"></div>
        <div class="col-md-6" id="chamados"></div>
    </div>
</div>
<script src="js/tether.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="js/chart.min.js"></script></body></html>
