<?php
	session_start();
	if (!empty($_SESSION['logado']))
  	$login = $_SESSION['logado'];

	require_once("connect.php");

	$sql = 'SELECT  chmd_cod, 				/* 0 */
	 								chmd_data_abert, 	/* 1 */
									chmd_titulo, 			/* 2 */
									chmd_descricao,		/* 3 */
									chmd_prioridade, 	/* 4 */
									chmd_status 			/* 5 */
					FROM chamado ORDER BY chmd_status ASC, chmd_prioridade DESC, chmd_data_abert DESC'; //buscando registros

	$res = mysqli_query($link, $sql)	or die (mysql_error());

	if (@mysqli_num_rows($res)==0)
		echo "Não há nenhum chamado cadastrado!";

	while($data = mysqli_fetch_array($res)){
		$chmd_cod = $data[0]; 						//recebe cod
		$chmd_data = $data[1]; 						//recebe data
		$chmd_titulo = $data[2]; 					//recebe desc
		$chmd_descricao = $data[3]; 			//recebe desc
		$chmd_prioridade = $data[4];			//recebe prioridade
		$chmd_status = $data[5]; 					//recebe status

		/* Formatando Data */
		$chmd_data_ano = substr($chmd_data, 0, 4); //recorta ano
		$chmd_data_mes = substr($chmd_data, 5, 2); //recorta mes
		$chmd_data_dia = substr($chmd_data, 8, 2); //recorta dia
		$chmd_data_hora = substr($chmd_data, 11, 2); //recorta hora
		$chmd_data_min = substr($chmd_data, 14, 2); //recorta data
		$data_hoje = $chmd_data_dia.'/'.$chmd_data_mes.'/'.$chmd_data_ano; //formata data
		if ($data_hoje == date("d/m/Y")) { //verifica se a data é de hoje
			$chmd_data = $chmd_data_hora.':'.$chmd_data_min; //define data como hora (já que é de hoje)
		} else {
			$chmd_data = $chmd_data_dia.'/'.$chmd_data_mes.'/'.$chmd_data_ano.' '.$chmd_data_hora.':'.$chmd_data_min; //formata data-hora
		}

		//compara as datas para calcular diferença:
		$data_hoje = new DateTime(date("Y-m-d"));
		$data_chamado = new DateTime($chmd_data_ano.'-'.$chmd_data_mes.'-'.$chmd_data_dia);

		$tempo = $data_hoje->diff($data_chamado);

		$class = "badge badge-pill data";

		if ($chmd_prioridade=='0' && $chmd_status=='0') { //se prioridade = 0: mostra amarelo (prioridade baixa)
			$class .= " prioridade-0";
		} else {
			if ($chmd_prioridade=='1' && $chmd_status=='0') { //se prioridade = 0: mostra laranja (prioridade média)
				$class .= " prioridade-1";
			} else {
				if ($chmd_prioridade=='2' && $chmd_status=='0') { //se prioridade = 0: mostra vermelho (prioridade alta)
					$class .= " prioridade-2";
				}
			}
		}

		if ($tempo->d <= 15 && $tempo->m == 0 && $tempo->y == 0) {  //o tempo for < 15 dias deixa mais escuro (tempo-1)
			$class .= " tempo-1";
		} else {
			if ($tempo->d > 15 && $tempo->d <= 30 && $tempo->m == 0 && $tempo->y == 0) { //o tempo for > 15 e < 31 dias deixa mais escuro ainda (tempo-2)
				$class .= " tempo-2";
			} else {
				if ($tempo->d > 30 && $tempo->m == 0 && $tempo->y == 0) { //o tempo for > 30 dias deixa muito escuro (tempo-3)
					$class .= " tempo-3";
				} else {
					$class .= " tempo-3";
				}
			}
		}

		if ($chmd_status=='2') { //se status = 2: mostra verde (terminado)
			$class .= " status-2";
		} else {
			if ($chmd_status=='1') { //se status = 1: mostra azul (em processo)
				$class .= " status-1";
			} else {
				if ($chmd_status=='3') { //se status = 3: mostra cinza (cancelado)
					$class .= " status-3";
				}
			}
		}

	if (isset($login['logged'])) { ?>
		<!-- Ao clicar na div, a função carrega pega o cod da tarefa e define os parâmetros de acordo com o cod -->
		<div onclick="carrega(<?=$chmd_cod?>)" data-toggle="modal" data-target="#modal-informacoes" class="alert alert-common" role="alert">
			<button type="button" class="close" data-toggle="tooltip" data-placement="left" title="<?=$chmd_data?>">
				<span class="<?=$class?>">&nbsp;&nbsp;</span>
			</button>
			<h4 class="alert-heading"><?=$chmd_titulo?></h4>
			<p class="mb-0"><?=$chmd_descricao?></p>
		</div>
	<?php } else { ?>
		<!-- Ao clicar na div, a função carrega pega o cod da tarefa e define os parâmetros de acordo com o cod -->
		<div class="alert alert-common" role="alert">
			<button type="button" class="close" data-toggle="tooltip" data-placement="left" title="<?=$chmd_data?>">
				<span class="<?=$class?>">&nbsp;&nbsp;</span>
			</button>
			<h4 class="alert-heading"><?=$chmd_titulo?></h4>
			<p class="mb-0"><?=$chmd_descricao?></p>
		</div>
		<?php } ?>
	<?php } ?>
<script>
$(function() {
	$('[data-toggle="tooltip"]').tooltip()
});
</script>
