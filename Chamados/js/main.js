$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

$(document).on('hidden.bs.modal', '.modal', function () {
    $('.modal:visible').length && $(document.body).addClass('modal-open');
});

window.setTimeout(function() {
  $(".msg").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
  });
}, 4000);

$(document).ready(function(){
  $('[data-toggle="popover"]').popover(); 
  comeca();
})

var timerI = null;
var timerR = false;

function para(){
    if(timerR)
        clearTimeout(timerI)
    timerR = false;
}

function comeca(){
    para();
    lista();
    grafico();
}

function lista(){
  $.ajax({
    url:"lista.php",
        success: function (textStatus){
        $('#chamados').html(textStatus); //mostrando resultado
      }
    })
    timerI = setTimeout("lista()", 60*1000);//tempo de espera
    timerR = true;
}

function carrega(cod) {
  var carregaData = {
    'data': cod
  };
  $.ajax({
    type: "POST",
    data: carregaData,
    url:"carrega.php",
        success: function (textStatus){
        $('#informacoes').html(textStatus); //mostrando resultado
      }
    })
}

function grafico() {
  $.ajax({
    url:"grafico.php",
        success: function (textStatus){
        $('#grafico').html(textStatus); //mostrando resultado
      }
    })
    timerI = setTimeout("grafico()", 10*60*1000);//tempo de espera
    timerR = true;
}
