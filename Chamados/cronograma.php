﻿<?php
  require_once("connect.php");

  session_start();
  if (empty($_SESSION['logado'])) {
  Header ("Location: index.php?e=3");
  } else {

  $login = $_SESSION['logado'];

  $sql = "SELECT chmd_cod,
							   chmd_data_abert,
							   chmd_titulo,
                 chmd_data_fecha
					FROM chamado
          ORDER BY chmd_cod";

	$res = mysqli_query($link, $sql)	or die (mysql_error());

  if (@mysqli_num_rows($res)==0) {
    $html = "Erro"; //Escreve Erro
  } else {

    /* Inicialização de Vars */
    $corpo = "";
    $conts = 1;
    $contm = [0,0,0,0,0,0,0,0,0,0,0,0];
    $i = 0;
    $data_antes = "";

    /* Carrega CSS da Tabela */
    $css =
      "<style>
            table {
              border-collapse: collapse;
              font-family: Arial, Sans Serif;
            }

            table, th, td {
              border: 1px solid black;
              text-align: center;
              vertical-align: middle;
            }

            th {
              background-color: #ddd;
              vertical-align: middle;
            }

            .square {
              width: 25px;
            }

            .item {
              width: 300px;
              text-align: left;
              padding-left: 10px;
            }

            .gru {
              background-color: green;
            }

            .mat {
              background-color: red;
            }

            .mat .bre {
              background: -webkit-linear-gradient(red, yellow);
              background: -o-linear-gradient(red, yellow);
              background: -moz-linear-gradient(red, yellow);
              background: linear-gradient(red, yellow);
            }

            .mat .gab {
              background: -webkit-linear-gradient(red, blue);
              background: -o-linear-gradient(red, blue);
              background: -moz-linear-gradient(red, blue);
              background: linear-gradient(red, blue);
            }

            .mat .thi {
              background: -webkit-linear-gradient(red, purple);
              background: -o-linear-gradient(red, purple);
              background: -moz-linear-gradient(red, purple);
              background: linear-gradient(red, purple);
            }

            .bre {
              background-color: yellow;
            }

            .bre .gab {
              background: -webkit-linear-gradient(yellow, blue);
              background: -o-linear-gradient(yellow, blue);
              background: -moz-linear-gradient(yellow, blue);
              background: linear-gradient(yellow, blue);
            }

            .bre .thi {
              background: -webkit-linear-gradient(yellow, purple);
              background: -o-linear-gradient(yellow, purple);
              background: -moz-linear-gradient(yellow, purple);
              background: linear-gradient(yellow, purple);
            }

            .gab {
              background-color: blue;
            }

            .gab .thi {
              background: -webkit-linear-gradient(blue, purple);
              background: -o-linear-gradient(blue, purple);
              background: -moz-linear-gradient(blue, purple);
              background: linear-gradient(blue, purple);
            }

            .thi {
              background-color: purple;
            }

      </style>";

    $table_ini = "<table>
    <tr>
      <td rowspan='3'>
        <img src='img/logo_black.png' height='80px' style='padding-top:20px; padding-bottom:20px;'>
      </td>
      <th colspan='52'>
        <h1>Cronograma Anual<h1>
      </th>
    </tr>
    <tr>
      <td colspan='12' rowspan='2'>
        Gerado em ".date("d/m/Y")." - ".date("H:i")."
      </td>
        <td colspan='30'><b>Legenda</b></td>
        <td colspan='12' rowspan='2'>
          Gerado por ".$login['nome']."
        </td>
      </tr>
      <tr>
        <td class='square gru'></td>
        <td colspan='5' style='text-align: left; padding-left: 10px'>Grupo</td>

        <td class='square mat'></td>
        <td colspan='5' style='text-align: left; padding-left: 10px'>Matheus</td>

        <td class='square bre'></td>
        <td colspan='5' style='text-align: left; padding-left: 10px'>Breno</td>

        <td class='square gab'></td>
        <td colspan='5' style='text-align: left; padding-left: 10px'>Gabriel</td>

        <td class='square thi'></td>
        <td colspan='5' style='text-align: left; padding-left: 10px'>Thiago</td>
    </tr>
    <tr><th>Itens do Desenvolvimento</th>
    "; // início table
      /* Contador de semanas por mês */
      do {
        $dateTimeObject = new DateTime(); //cria dateTime
        $dateTimeObject->setISODate(2017, $conts); //retorna o dia do começo da semana número $conts
        $ret['week_start'] = $dateTimeObject->format('Y-m-d'); //cria array com o começo da semana (data)
        $mes = substr($ret['week_start'], 5, 2); //recorta mes

        /* Conta quantas semanas por mês */
        if ($mes=='01')
          $contm[0]++;
        if ($mes=='02')
          $contm[1]++;
        if ($mes=='03')
          $contm[2]++;
        if ($mes=='04')
          $contm[3]++;
        if ($mes=='05')
          $contm[4]++;
        if ($mes=='06')
          $contm[5]++;
        if ($mes=='07')
          $contm[6]++;
        if ($mes=='08')
          $contm[7]++;
        if ($mes=='09')
          $contm[8]++;
        if ($mes=='10')
          $contm[9]++;
        if ($mes=='11')
          $contm[10]++;
        if ($mes=='12')
          $contm[11]++;

        $conts++; //faz o acréscimo

      } while ($conts<=52); // 52 = numero de semanas no ano

      do {
        /* Define o nome de acordo com o count */
        if ($i==0)
          $mes_nome = "Janeiro";
        if ($i==1)
          $mes_nome = "Fevereiro";
        if ($i==2)
          $mes_nome = "Março";
        if ($i==3)
          $mes_nome = "Abril";
        if ($i==4)
          $mes_nome = "Maio";
        if ($i==5)
          $mes_nome = "Junho";
        if ($i==6)
          $mes_nome = "Julho";
        if ($i==7)
          $mes_nome = "Agosto";
        if ($i==8)
          $mes_nome = "Setembro";
        if ($i==9)
          $mes_nome = "Outubro";
        if ($i==10)
          $mes_nome = "Novembro";
        if ($i==11)
          $mes_nome = "Dezembro";

        /* Cria o cabeçalho da tabela */
        $table_ini .= "<th colspan='".$contm[$i]."'>".$mes_nome."</th>";

        $i++; // faz acréscimo

      } while($i<12); //12 meses no ano

    /* While para receber valores da query */
    while($data = mysqli_fetch_array($res)){
      $class = " gru"; //class padrão
      $chmd_cod = $data[0]; 							//recebe cod
      $chmd_data = $data[1]; 							//recebe data
      $chmd_titulo = $data[2]; 						//recebe titulo
      $chmd_data_fecha = $data[3]; 				//recebe titulo

      /* Formatando Data */
      $chmd_data_ano = substr($chmd_data, 0, 4); //recorta ano
      $chmd_data_mes = substr($chmd_data, 5, 2); //recorta mes
      $chmd_data_dia = substr($chmd_data, 8, 2); //recorta dia
      $semana = date("W", mktime(0,0,0,$chmd_data_mes, $chmd_data_dia, $chmd_data_ano)); //recebe numero da semana da data do chamado
      $semana_atual = date("W", strtotime(date("d-m-Y"))); //recebe o numero da semana atual

      /* Query que busca vínculos de usuários */
      $query = "SELECT usuario.user_cod,
                       usuario_chamado.usrchmd_chmd_vincini,
                       usuario_chamado.usrchmd_chmd_vincfim
              FROM usuario
              INNER JOIN usuario_chamado ON usuario.user_cod = usuario_chamado.usrchmd_user_cod
              WHERE usuario_chamado.usrchmd_chmd_cod = $chmd_cod";

      $result = mysqli_query($link, $query)	or die (mysql_error());

      if (@mysqli_num_rows($result)==0) {
        /* Inicialização da contagem de semanas */
        $cont = 1;

        /* Início do corpo - Insere o codigo do item seguido pelo nome */
        $corpo .= "<tr><td class='item'>". $chmd_cod . " - " . $chmd_titulo . "</td>";

        /* Se não possui data de fechamento */
        if ($chmd_data_fecha == '') {
          do {
            if ($cont <= $semana_atual && $cont >= $semana) { //verifica se a contagem de semanas está no intervalo entre semana de início e a semana atual
              $corpo .= "<td class='square gru'></td>";
            } else {
              $corpo .= "<td class='square'></td>";
            }
            $cont++;
          } while ($cont<=52); //quantidade de semanas no ano

        } else {

          $data_fecha = substr($chmd_data_fecha, 0, 10); //recebe data de fechamento
          $semana_fecha = date("W", strtotime($data_fecha)); //recebe semana de fechamento

          do {
            if ($cont <= $semana_fecha && $cont >= $semana) {  //verifica se a contagem de semanas está no intervalo entre semana de início e a semana atual
              $corpo .= "<td class='square".$class."'></td>";
            } else {
                $corpo .= "<td class='square'></td>";
              }
            $cont++;
            } while ($cont<=52);
          }
          $corpo .= "</tr>";

        } else {

          while ($dados= mysqli_fetch_array($result)) { //recebe valores da segunda query
          $user_cod = $dados[0];
          $vinc_ini = $dados[1];
          $vinc_fim = $dados[2];

          $corpo .=  "<tr> <td class='item'>" . $chmd_titulo . "</td>"; //escreve o código e o nome do chamado
          $cont = 1; //reinicializa o cont
          do {
            $class = "gru"; //classe padrão

            if ($user_cod == 2)
              $class = " mat"; //matheus

            if ($user_cod == 3)
              $class = " gab"; //gabriel

            if ($user_cod == 4)
              $class = " thi"; //thiago

            if ($user_cod == 5)
              $class = " bre"; //breno

            if ($user_cod == 0)
              $class = " gru"; //breno

            if ($chmd_data_fecha == '') { //se o chamado ainda não foi fechado

              if ($vinc_fim == null) { //se o vínculo não foi fechado
                $vinc_ini = substr($vinc_ini, 0, 10); //recebe data inicial
                $semana_vinc_ini = date("W", strtotime($vinc_ini)); //retorna semana

                if ($cont <= $semana_atual && $cont >= $semana_vinc_ini) {
                  $corpo .= "<td class='square".$class."'></td>";
                } else {
                  $corpo .= "<td class='square'></td>";
                }

              } else {
                $vinc_ini = substr($vinc_ini, 0, 10); //recebe data inicial
                $semana_vinc_ini = date("W", strtotime($vinc_ini)); //retorna semana

                $vinc_fim = substr($vinc_fim, 0, 10); //recebe data final
                $semana_vinc_fim = date("W", strtotime($vinc_fim)); //retorna semana

                if ($cont <= $semana_vinc_fim && $cont >= $semana_vinc_ini) {
                  $corpo .= "<td class='square".$class."'></td>";
                } else {
                  $corpo .= "<td class='square'></td>";
                }
              }

            } else {
              $vinc_ini = substr($vinc_ini, 0, 10); //recebe data inicial
              $semana_vinc_ini = date("W", strtotime($vinc_ini)); //retorna semana

              $vinc_fim = substr($vinc_fim, 0, 10); //recebe data final
              $semana_vinc_fim = date("W", strtotime($vinc_fim)); //retorna semana

              if ($cont <= $semana_vinc_fim && $cont >= $semana_vinc_ini) {
                $corpo .= "<td class='square".$class."'></td>";
              } else {
              $corpo .= "<td class='square'></td>";
              }
            }
            $cont++;
          } while ($cont<=52);

            $corpo .=  "</tr>";
          }
        }
      }

  $table_fim = "</table>";

  $html = $css.$table_ini.$corpo.$table_fim;

  $footer = "
  <table width='100%' style='font-size: 8pt; border: none;'>
    <tr>
      <td width='30%' style='border: none; text-align:left;'>".date("d/m/Y")." - ".date("H:i")."</td>
      <td width='40%' style='border: none;'>Copyright &copy; Todos os direitos autorais reservados à Salus</td>
      <td width='30%' style='border: none; text-align:right;'>Página {PAGENO} de {nbpg}</td>
    </tr>
  </table>";

  //echo $html;
  $filename = 'Salus - Cronograma (' . date('d-m-Y H:i:s'). ') - ' . $login['nome'] .'.pdf';

  define('MPDF_PATH', 'class/mpdf/');
  include(MPDF_PATH.'mpdf.php');
  $mpdf=new mPDF('c', 'A4-L');
  $mpdf->SetHTMLFooter($footer);
  $mpdf->SetWatermarkImage('img/marca.png');
  $mpdf->showWatermarkImage = true;
  $mpdf->WriteHTML($html);
  $mpdf->Output("pdf/".$filename);
  $mpdf->Output("pdf/".$filename, 'F');
  }
  header("Location: http://www.salus.ml/pdf/$filename");
}
?>
