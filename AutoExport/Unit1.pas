unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, ShellAPI, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.Imaging.pngimage, Vcl.ComCtrls, Vcl.Tabs, Data.DB, IniFiles,
  Data.Win.ADODB, MemDS, DBAccess, MyAccess, VCLTee.TeeFilters, Vcl.Imaging.jpeg,
  Vcl.Grids, Vcl.DBGrids;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    Edit1: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Edit2: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    FileOpenDialog1: TFileOpenDialog;
    CheckBox1: TCheckBox;
    FileOpenDialog2: TFileOpenDialog;
    BitBtn3: TBitBtn;
    TabControl1: TTabControl;
    Panel2: TPanel;
    CheckBox2: TCheckBox;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label4: TLabel;
    Edit3: TEdit;
    MyConnection1: TMyConnection;
    MyQuery1: TMyQuery;
    MyDataSource1: TMyDataSource;
    TImage1: TImage;
    MyQuery2: TMyQuery;
    Timer2: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure FileOpenDialog1FileOkClick(Sender: TObject;
      var CanClose: Boolean);
    procedure BitBtn2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure FileOpenDialog2FileOkClick(Sender: TObject;
      var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel2DblClick(Sender: TObject);
    procedure FileOpenDialog2Execute(Sender: TObject);
    procedure FileOpenDialog1Execute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Panel1DblClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    procedure LerIni;
    procedure GravarIni;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  ArqIni : TIniFile;
  Origem: string;
  Destino: string;

implementation

{$R *.dfm}

uses Unit2;

procedure TForm1.GravarIni;
var
  ArqIni : TIniFile;
begin
  ArqIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'config.ini');
  Try
    ArqIni.WriteInteger('Combobox1','ItemIndex',Combobox1.ItemIndex);
    ArqIni.WriteBool('Checkbox1','Checked',Checkbox1.Checked);
    ArqIni.WriteBool('Checkbox2','Checked',Checkbox2.Checked);
    ArqIni.WriteString('Edit1','Text',Edit1.Text);
    ArqIni.WriteString('Edit2','Text',Edit2.Text);
    ArqIni.WriteString('Edit3','Text',Edit3.Text);
  Finally
    FreeAndNil(ArqIni);
  End;
end;

procedure TForm1.LerIni;
var
  ArqIni : TIniFile;
begin
  ArqIni := TIniFile.Create(ExtractFilePath(Application.ExeName)+'config.ini');
  Try
    Combobox1.ItemIndex := ArqIni.ReadInteger('Combobox1','ItemIndex',Combobox1.ItemIndex);
    Checkbox1.Checked := ArqIni.ReadBool('Checkbox1','Checked',Checkbox1.Checked);
    Checkbox2.Checked := ArqIni.ReadBool('Checkbox2','Checked',Checkbox2.Checked);
    Edit1.Text := ArqIni.ReadString('Edit1','Text',Edit1.Text);
    Edit2.Text := ArqIni.ReadString('Edit2','Text',Edit2.Text);
    Edit3.Text := ArqIni.ReadString('Edit3','Text',Edit3.Text);
  Finally
    FreeAndNil(ArqIni);
  End;
end;



procedure TForm1.Panel1DblClick(Sender: TObject);
begin
Form1.Visible := False;
Form2.ShowModal;
end;

procedure TForm1.Panel2DblClick(Sender: TObject);
begin
Form1.Visible := False;
Form2.ShowModal;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
FileOpenDialog1.Execute;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
FileOpenDialog2.Execute;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
var
 Parametros: Pchar;
 data : TDateTime;
begin
  if (Length(Trim(Origem)) > 0) and (Length(Trim(Destino)) > 0) then
  begin
    Parametros := Pchar('"' + Origem + '"' + ' "' + Destino + '" /S /Y /H /E /K /F /D');
    ShellExecute(handle, '', 'xCopy', Parametros, '', SW_HIDE);
  end;
  if(Combobox1.ItemIndex = -1) then
    MyQuery1.SQL.Text := ''
  else
    data := date;
    MyQuery1.SQL.Text := 'SELECT * FROM diario WHERE `diar_usr_id` =' + QuotedStr(IntToStr(Combobox1.ItemIndex)) + ' AND `diar_data` = ' + QuotedStr(DateToStr(data)) + ' AND `diar_desc` = ' +   QuotedStr(Edit3.Text);
    MyQuery1.Active := true;
    MyQuery1.Recno := MyQuery1.RecordCount-1;
    if (MyQuery1.Fields[0].AsString = IntToStr(Combobox1.ItemIndex)) or (MyQuery1.Fields[1].AsString = DateToStr(data)) or (MyQuery1.Fields[2].AsString = Edit3.Text) then
    else
      begin
        MyQuery1.SQL.Text := 'insert into diario (`diar_usr_id`, `diar_data`, `diar_desc`) values (' + IntToStr(Combobox1.ItemIndex) + ',' + QuotedStr(DateToStr(data)) + ',' + QuotedStr(Edit3.Text)+');';
        MyQuery1.ExecSQL;
      end;
    if Panel2.Visible then
      Edit3.Clear;

end;

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
if Timer1.Enabled then
  Timer1.Enabled := False
else
  Timer1.Enabled := True;
end;

procedure TForm1.FileOpenDialog1Execute(Sender: TObject);
begin
      Form1.FormStyle := fsNormal;
end;

procedure TForm1.FileOpenDialog1FileOkClick(Sender: TObject;
  var CanClose: Boolean);
begin
      Form1.FormStyle := fsStayOnTop;
      Origem := FileOpenDialog1.FileName;
      Origem := stringreplace(Origem, '\', '/', [rfReplaceAll, rfIgnoreCase]);
      Edit1.Text := Origem;
end;

procedure TForm1.FileOpenDialog2Execute(Sender: TObject);
begin
      Form1.FormStyle := fsNormal;
end;

procedure TForm1.FileOpenDialog2FileOkClick(Sender: TObject;
  var CanClose: Boolean);
begin
      Form1.FormStyle := fsStayOnTop;
      Destino := FileOpenDialog2.FileName;
      Destino := stringreplace(Destino, '\', '/', [rfReplaceAll, rfIgnoreCase]);
      Edit2.Text := Destino;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
MyQuery2.Active := True;
Origem := Edit1.Text;
Destino := Edit2.Text;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
GravarIni;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
LerIni;
end;

procedure TForm1.FormDblClick(Sender: TObject);
begin
Form1.Visible := False;
Form2.ShowModal;
end;

procedure TForm1.FormHide(Sender: TObject);
begin
BitBtn3.Click;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
BitBtn3.Click;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
BitBtn3.Click;
end;

procedure TForm1.TabControl1Change(Sender: TObject);
begin
//if TabControl1.TabIndex=0 then
//begin
//  Panel2.Visible := false;
//  Panel1.Visible := true;
//end

//else
//begin
// Panel1.Visible := False;
// Panel2.Visible := true;
//end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
BitBtn3.Click;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
if Form2.Visible then
begin
  MyQuery2.Active := False;
  MyQuery2.Active := True;
end;

end;

end.
