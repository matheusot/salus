object Form2: TForm2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Dados do Di'#225'rio'
  ClientHeight = 309
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 296
    Width = 645
    Height = 13
    Align = alBottom
    Alignment = taCenter
    Caption = 
      'ID: (0 = Grupo | 1 = Breno | 2 = Gabriel | 3 = Thiago | 4 = Math' +
      'eus)'
    ExplicitWidth = 329
  end
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 0
    Top = 0
    Width = 645
    Height = 296
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alClient
    BorderStyle = bsNone
    DataSource = Form1.MyDataSource1
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'diar_usr_id'
        Title.Alignment = taCenter
        Title.Caption = 'ID'
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'diar_data'
        Title.Alignment = taCenter
        Title.Caption = 'DATA'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'diar_desc'
        Title.Alignment = taCenter
        Title.Caption = 'DESCRI'#199#195'O'
        Width = 350
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'diar_data_insert'
        Title.Alignment = taCenter
        Title.Caption = 'INSER'#199#195'O'
        Width = 120
        Visible = True
      end>
  end
end
